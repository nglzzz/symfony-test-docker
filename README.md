Project installation
========================

# 1. Composer

$ composer install

# 2. Install node modules
2.1 Install Yarn (you can see the instruction on documentation of yarn: https://yarnpkg.com/lang/en/docs/install/)

2.2 Set config:  
2.2.1 Copy frontend/src/config.js.dist to frontend/src/config.js  
2.2.2 Change parameter "servicesUrl" to your local environment url (localhost:8000 if you use symfony console command "php bin/console server:start") in the config.js  

2.3 Create ".env" file in frontend/
2.3.1 Write NODE_PATH in the file:  
NODE_PATH=src/

2.2 $ cd frontent  
2.2 $ yarn && yarn start  
