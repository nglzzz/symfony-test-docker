import React from 'react';

import { Route, Switch, withRouter } from 'react-router';
import ResultsSearchContainer from 'containers/ResultsSearchContainer';
import MainSearchContainer from 'containers/MainSearchContainer';
import TermsPage from 'components/TermsPage';
import OffersPageContainer from 'containers/OffersPageContainer';
import PropTypes from 'prop-types';

function App({ location }) {
  return (
    <div className={location.pathname === '/' ? 'home' : ''}>
      <Switch>
        <Route exact path="/" component={MainSearchContainer} />
        <Route path="/results" component={ResultsSearchContainer} />
        <Route path="/terms" component={TermsPage} />
        <Route path="/offers" component={OffersPageContainer} />
      </Switch>
    </div>
  );
}

App.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired
};

export default withRouter(App);
