import React from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as SearchActions from 'actions/SearchActions'
import { LICENSE_TYPE, ONLINE_SALE_TYPE } from 'constants/ProgramTypes'
import ResultsSearchContainer from 'containers/ResultsSearchContainer'

class OffersPageContainer extends React.Component {
  componentWillMount () {
    this.props.setGeneralSearchSettings(null)

    this.props.filterBy({
      programTypes: [LICENSE_TYPE, ONLINE_SALE_TYPE]
    })

    this.props.setPage(1)

    const urlParams = new URLSearchParams(window.location.search);

    if (urlParams.has('assignee')) {
      this.props.toggleAdvancedSearch(true);

      this.props.setAdvancedSearchSettings({
        currentAssignee: urlParams.get('assignee')
      });
    }

    this.props.loadPatents()
  }

  render () {
    return <ResultsSearchContainer/>
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(SearchActions, dispatch)
}

export default connect(null, mapDispatchToProps)(OffersPageContainer)
