import React from 'react'
import PropTypes from 'prop-types'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as SearchActions from 'actions/SearchActions'
import {withRouter} from 'react-router';
import Header from 'components/Header';
import Footer from 'components/Footer';
import SearchForm from 'components/SearchForm';

class MainSearchContainer extends React.Component {
  static propTypes = {
    loadPatents: PropTypes.func.isRequired,

    toggleAdvancedSearch: PropTypes.func.isRequired,
    setAdvancedSearchSettings: PropTypes.func.isRequired,
    setGeneralSearchSettings: PropTypes.func.isRequired,

    setPage: PropTypes.func.isRequired,
    clearResults: PropTypes.func.isRequired,
    filterBy: PropTypes.func.isRequired,

    query: PropTypes.shape({
      generalSearchQuery: PropTypes.string.isRequired,
      advancedSearchAttributes: PropTypes.shape({
        searchGrants: PropTypes.bool.isRequired,
        searchApplications: PropTypes.bool.isRequired,
        searchNPLs: PropTypes.bool.isRequired,
        title: PropTypes.string.isRequired,
        inventor: PropTypes.string.isRequired,
        abstract: PropTypes.string.isRequired,
        specification: PropTypes.string.isRequired,
        claims: PropTypes.string.isRequired,
        currentAssignee: PropTypes.string.isRequired
      }).isRequired,
      isAdvancedSearchEnabled: PropTypes.bool.isRequired,
      filteringAttributes: PropTypes.shape({
        programTypes: PropTypes.arrayOf(PropTypes.string).isRequired,
      }).isRequired,
    }).isRequired,

    history: PropTypes.shape({
      push: PropTypes.func.isRequired
    }).isRequired
  };

  static contextTypes = {
    router: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      query: {...props.query},
    };

    props.clearResults();

    this.toggleAdvancedSearch = this.toggleAdvancedSearch.bind(this);
    this.search = this.search.bind(this);
    this.handleGeneralSearchChange = this.handleGeneralSearchChange.bind(this);
    this.toggleProgramType = this.toggleProgramType.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      query: { ...nextProps.query },
    });
  }

  toggleAdvancedSearch = (value) => {
    this.props.toggleAdvancedSearch(value);
  };

  search = (settings, filters) => {
    if (this.props.query.isAdvancedSearchEnabled) {
      this.props.setAdvancedSearchSettings(settings);
    } else {
      this.props.setGeneralSearchSettings(this.state.query.generalSearchQuery);
    }

    this.props.filterBy(filters || {});

    this.props.setPage(1);

    this.props.loadPatents();

    this.props.history.push('/results', this.state.query)
  };

  addTerm = (type, term) => {
    if (!this.props.query.filteringAttributes[type].includes(term)) {
      this.props.filterBy({
        [type]: [...this.props.query.filteringAttributes[type], term]
      });
    }
  };

  removeTerm = (type, term) => {
    this.props.filterBy({
      [type]: [
        ...this.props.query.filteringAttributes[type].filter(
            item => item !== term
        )
      ]
    });
  };

  toggleProgramType = (type) => {
    this.props.setGeneralSearchSettings(this.state.query.generalSearchQuery);

    const currentState = this.props.query.filteringAttributes.programTypes.includes(type);

    if (currentState) {
      this.removeTerm('programTypes', type);
    } else {
      this.addTerm('programTypes', type);
    }
  };

  handleGeneralSearchChange = (newValue) => {
    this.setState((oldState) => ({
      query: {
        ...oldState.query,
        generalSearchQuery: newValue,
      }
    }));
  };

  render() {
    return (
      <div>
        <Header />
        <SearchForm
          isMain
          onSearch={this.search}
          toggleAdvancedSearch={this.toggleAdvancedSearch}

          handleGeneralSearchChange={this.handleGeneralSearchChange}

          isAdvancedSearchEnabled={this.props.query.isAdvancedSearchEnabled}
          generalSearchQuery={this.state.query.generalSearchQuery}
          advancedSearchAttributes={this.state.query.advancedSearchAttributes}
          selectedProgramTypes={this.props.query.filteringAttributes.programTypes}
          toggleProgramType={this.toggleProgramType}
        />
        <Footer isFixed />
      </div>
    )
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(SearchActions, dispatch)
}

function mapStateToProps (state) {
  return {
    query: state.query,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(MainSearchContainer))
