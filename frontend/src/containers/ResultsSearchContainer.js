import React from 'react';
import PropTypes from 'prop-types';

import SearchResults from 'components/results/SearchResults';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as SearchActions from 'actions/SearchActions';
import { withRouter } from 'react-router';
import Header from 'components/Header';
import Footer from 'components/Footer';
import SearchSettings from 'components/results/SearchSettings';
import SelectedTags from 'components/results/SelectedTags';
import SearchForm from 'components/SearchForm';
import { GENERIC_STATUS, STATUSES } from 'constants/VerificationStatuses';

class ResultsSearchContainer extends React.Component {
  static propTypes = {
    selectPatent: PropTypes.func.isRequired,
    loadPatents: PropTypes.func.isRequired,

    toggleAdvancedSearch: PropTypes.func.isRequired,
    setAdvancedSearchSettings: PropTypes.func.isRequired,
    setGeneralSearchSettings: PropTypes.func.isRequired,
    clear: PropTypes.func.isRequired,

    setPage: PropTypes.func.isRequired,
    setPerPage: PropTypes.func.isRequired,

    sortBy: PropTypes.func.isRequired,
    filterBy: PropTypes.func.isRequired,

    patents: PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.string,
        number: PropTypes.string,
        qScore: PropTypes.number,
        vScore: PropTypes.number,
        title: PropTypes.string,
        priorityDate: PropTypes.string,
        source: PropTypes.string,
        sourceDesc: PropTypes.string,
        currentAssignee: PropTypes.arrayOf(PropTypes.string),
        verificationStatus: PropTypes.string
      })
    ),
    currentAssignees: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        count: PropTypes.number.isRequired
      })
    ).isRequired,
    sources: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        count: PropTypes.number.isRequired
      })
    ).isRequired,

    paginationAttributes: PropTypes.shape({
      countAll: PropTypes.number.isRequired,
      onPage: PropTypes.number.isRequired,
      page: PropTypes.number.isRequired,
      pages: PropTypes.number.isRequired,
      perPage: PropTypes.number.isRequired
    }).isRequired,

    query: PropTypes.shape({
      generalSearchQuery: PropTypes.string.isRequired,
      advancedSearchAttributes: PropTypes.shape({
        searchGrants: PropTypes.bool.isRequired,
        searchApplications: PropTypes.bool.isRequired,
        searchNPLs: PropTypes.bool.isRequired,
        title: PropTypes.string.isRequired,
        inventor: PropTypes.string.isRequired,
        abstract: PropTypes.string.isRequired,
        specification: PropTypes.string.isRequired,
        claims: PropTypes.string.isRequired,
        currentAssignee: PropTypes.string.isRequired
      }).isRequired,
      isAdvancedSearchEnabled: PropTypes.bool.isRequired,
      sortingField: PropTypes.string,
      filteringAttributes: PropTypes.shape({
        verificationStatus: PropTypes.arrayOf(PropTypes.string).isRequired,
        currentAssignees: PropTypes.arrayOf(PropTypes.string).isRequired,
        priorityYears: PropTypes.shape({
          from: PropTypes.string,
          to: PropTypes.string
        }).isRequired,
        documentSource: PropTypes.arrayOf(PropTypes.string).isRequired
      }).isRequired,
      paginationAttributes: PropTypes.shape({
        perPage: PropTypes.number.isRequired,
        page: PropTypes.number.isRequired
      }).isRequired
    }).isRequired,

    history: PropTypes.shape({
      push: PropTypes.func.isRequired
    }).isRequired,

    isLoading: PropTypes.bool.isRequired
  };

  static defaultProps = {
    patents: null
  };

  constructor(props) {
    super(props);

    if (props.patents === null && !props.isLoading) {
      props.history.push('/');
    }

    this.state = {
      query: { ...props.query }
    };

    this.toggleProgramType = this.toggleProgramType.bind(this);
    this.setPage = this.setPage.bind(this);
    this.setPerPage = this.setPerPage.bind(this);
    this.toggleAdvancedSearch = this.toggleAdvancedSearch.bind(this);
    this.search = this.search.bind(this);
    this.handleGeneralSearchChange = this.handleGeneralSearchChange.bind(this);
    this.addTerm = this.addTerm.bind(this);
    this.removeTerm = this.removeTerm.bind(this);
    this.sort = this.sort.bind(this);
    this.changeDate = this.changeDate.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      query: { ...nextProps.query }
    });
  }

  setPage = page => {
    this.props.setPage(page);

    this.props.loadPatents();
  };

  setPerPage = event => {
    const rows = +event.target.value;

    this.props.setPerPage(rows);

    this.props.loadPatents();
  };

  toggleAdvancedSearch = value => {
    this.props.toggleAdvancedSearch(value);
  };

  search = settings => {
    if (this.props.query.isAdvancedSearchEnabled) {
      this.props.setAdvancedSearchSettings(settings);
    } else {
      this.props.setGeneralSearchSettings(this.state.query.generalSearchQuery);
    }

    if (this.props.paginationAttributes.page !== 1) {
      this.props.setPage(1);
    }

    this.props.loadPatents();

    this.props.history.push('/results', this.state.query);
  };

  handleGeneralSearchChange = newValue => {
    this.setState(oldState => ({
      query: {
        ...oldState.query,
        generalSearchQuery: newValue
      }
    }));
  };

  toggleType = (type, state) => {
    const statuses = this.props.query.filteringAttributes.verificationStatus;
    const validStatuses = statuses.filter(status => status !== GENERIC_STATUS);

    let verificationStatuses;

    if (state) {
      if (type === GENERIC_STATUS) {
        verificationStatuses = STATUSES;
      } else {
        if (statuses.includes(type)) {
          return;
        }

        verificationStatuses = [...validStatuses, type];
      }
    } else {
      if (!statuses.includes(type)) {
        return;
      }

      verificationStatuses = [...validStatuses.filter(item => item !== type)];
    }

    if (verificationStatuses) {
      this.props.filterBy({
        verificationStatus: verificationStatuses
      });
    }

    this.props.loadPatents();
  };

  addTerm = (type, term) => {
    if (!this.props.query.filteringAttributes[type].includes(term)) {
      this.props.filterBy({
        [type]: [...this.props.query.filteringAttributes[type], term]
      });

      this.props.loadPatents();
    }
  };

  removeTerm = (type, term) => {
    this.props.filterBy({
      [type]: [
        ...this.props.query.filteringAttributes[type].filter(
          item => item !== term
        )
      ]
    });

    this.props.loadPatents();
  };

  toggleProgramType = (type) => {
    const currentState = this.props.query.filteringAttributes.programTypes.includes(type);

    if (currentState) {
      this.removeTerm('programTypes', type);
    } else {
      this.addTerm('programTypes', type);
    }
  };

  changeDate = (type, value) => {
    if (this.props.query.filteringAttributes.priorityYears[type] !== value) {
      this.props.filterBy({
        priorityYears: {
          ...this.props.query.filteringAttributes.priorityYears,
          [type]: value
        }
      });

      this.props.loadPatents();
    }
  };

  sort = value => {
    this.props.sortBy(value);

    this.props.loadPatents();
  };

  clear = () => {
    this.props.clear();
  };

  render() {
    return (
      <div>
        <Header />
        <SearchForm
          onSearch={this.search}
          onClear={this.clear}
          toggleAdvancedSearch={this.toggleAdvancedSearch}
          handleGeneralSearchChange={this.handleGeneralSearchChange}
          isAdvancedSearchEnabled={this.props.query.isAdvancedSearchEnabled}
          generalSearchQuery={this.state.query.generalSearchQuery}
          advancedSearchAttributes={this.state.query.advancedSearchAttributes}
          paginationAttributes={this.props.paginationAttributes}
          selectedProgramTypes={this.props.query.filteringAttributes.programTypes}
          toggleProgramType={this.toggleProgramType}
        />
        <SelectedTags
          selectedCurrentAssignees={
            this.props.query.filteringAttributes.currentAssignees
          }
          selectedSources={this.props.query.filteringAttributes.documentSource}
          selectedVerificationTypes={this.props.query.filteringAttributes.verificationStatus}
          removeTerm={this.removeTerm}
          wrapperClass="d-none d-md-block"
        />
        <SearchSettings
          query={this.state.query}
          currentAssignees={this.props.currentAssignees}
          sources={this.props.sources}
          addTerm={this.addTerm}
          sort={this.sort}
          onDateChange={this.changeDate}
          selectedCurrentAssignees={
            this.props.query.filteringAttributes.currentAssignees
          }
          selectedSources={this.props.query.filteringAttributes.documentSource}
          selectedVerificationTypes={this.props.query.filteringAttributes.verificationStatus}
          removeTerm={this.removeTerm}
          toggleType={this.toggleType}
        />
        <SearchResults
          isLoading={this.props.isLoading}
          setPage={this.setPage}
          paginationAttributes={this.props.paginationAttributes}
          patents={this.props.patents}
          changeRowsPerPage={this.setPerPage}
          selectPatent={this.props.selectPatent}
          selectedPatent={this.props.selectedPatent}
        />
        <div className="results-veil">&nbsp;</div>
        <Footer />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(SearchActions, dispatch);
}

function mapStateToProps(state) {
  return {
    query: state.query,
    paginationAttributes: {
      ...state.results.paginationAttributes,
      perPage: state.query.paginationAttributes.perPage
    },
    patents: state.results.patents,
    currentAssignees: state.results.currentAssignees,
    sources: state.results.sources,
    isLoading: state.results.inProgress,
    selectedPatent: state.patent.selectedPatent
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withRouter(ResultsSearchContainer)
);
