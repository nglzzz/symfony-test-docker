const { css } = require('styled-components');

const sizes = {
  giant: 1170,
  desktop: 992,
  tablet: 768,
  phone: 376
};

const media = Object.keys(sizes).reduce((accumulator, label) => {
  const emSize = sizes[label] / 16;

  const localAccumulator = accumulator;

  localAccumulator[label] = (...args) => css`
    @media (max-width: ${emSize}em) {
      ${css(...args)}
    }
  `;

  return localAccumulator
}, {});

module.exports = media;
module.exports.sizes = sizes;
