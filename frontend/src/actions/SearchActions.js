import {
  VERIFICATIONS_REQUEST_SUCCESS,
  VERIFICATIONS_REQUEST_STARTED,
  VERIFICATIONS_REQUEST_ERROR,
  SEARCH_PATENTS_SUCCESS,
  SEARCH_PATENTS_STARTED,
  SEARCH_PATENTS_ERROR,
  CLEAR_RESULTS,
  SET_PAGE,
  SET_PER_PAGE,
  SORT_BY,
  FILTER_BY,
  TOGGLE_ADVANCED_SEARCH,
  SET_GENERAL_SEARCH_SETTINGS,
  SET_ADVANCED_SEARCH_SETTINGS,
  SET_QUERY,
  CLEAR,
  SELECT_PATENT
} from 'constants/ActionTypes';
import { createAction } from 'redux-actions';

import config from 'config';
import axios from 'axios/index';

export const searchPatentsStarted = createAction(SEARCH_PATENTS_STARTED);
export const searchPatentsSuccess = createAction(SEARCH_PATENTS_SUCCESS);
export const searchPatentsError = createAction(SEARCH_PATENTS_ERROR);

export const verificationsRequestStarted = createAction(
  VERIFICATIONS_REQUEST_STARTED
);
export const verificationsRequestSuccess = createAction(
  VERIFICATIONS_REQUEST_SUCCESS
);
export const verificationsRequestError = createAction(
  VERIFICATIONS_REQUEST_ERROR
);

export const clearResults = createAction(CLEAR_RESULTS);
export const selectPatent = createAction(SELECT_PATENT);

export const setPage = createAction(SET_PAGE);
export const setPerPage = createAction(SET_PER_PAGE);
export const toggleAdvancedSearch = createAction(TOGGLE_ADVANCED_SEARCH);
export const sortBy = createAction(SORT_BY);
export const filterBy = createAction(FILTER_BY);
export const setGeneralSearchSettings = createAction(
  SET_GENERAL_SEARCH_SETTINGS
);
export const setAdvancedSearchSettings = createAction(
  SET_ADVANCED_SEARCH_SETTINGS
);
export const setQuery = createAction(SET_QUERY);
export const clear = createAction(CLEAR);

export const loadPatents = () => (dispatch, getState) => {
  const state = getState();

  if (state.query.isAdvancedSearchEnabled) {
    const searchAttributes = state.query.advancedSearchAttributes;

    if (
      !searchAttributes.title && !searchAttributes.inventor && !searchAttributes.claims && !searchAttributes.abstract
      && !searchAttributes.specification && !searchAttributes.currentAssignee
    ) {
      return;
    }
  }

  dispatch(searchPatentsStarted(state.query));

  axios
    .get(`${config.servicesUrl}/search`, {
      params: {
        query: state.query
      }
    })
    .then(response => {
      dispatch(searchPatentsSuccess(response.data));
    })
    .catch(reason => {
      dispatch(searchPatentsError(reason));
    });
};

export const loadVerificationDetails = () => (dispatch, getState) => {
  const state = getState();

  dispatch(verificationsRequestStarted(state.patent.selectedPatent));

  axios
    .get(`${config.servicesUrl}/verifications`, {
      params: {
        patentNumber: state.patent.selectedPatent.number
      }
    })
    .then(response => {
      dispatch(verificationsRequestSuccess(response.data));
    })
    .catch(reason => {
      dispatch(verificationsRequestError(reason));
    });
};
