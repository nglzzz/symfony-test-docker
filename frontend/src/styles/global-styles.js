import { injectGlobal } from 'styled-components'
import 'assets/css/select.css'

/* eslint no-unused-expressions: 0 */
injectGlobal`
  body {
    margin: 0;
    padding: 0;
    min-height: 100vh;
    font-family: 'Roboto', sans-serif;
    background: #e9f1f0;
    font-size: 14px;
  }
  
  .root {
    overflow: hidden;
  }
  
  a, a:hover {
    color: #00a693;
    text-decoration: none;
  }
  
  .home {
    height: 100vh;
    background: url(../images/main-bg.jpg) no-repeat 50% 50%;
    background-size: cover;
  }
  
  .container {
    position: relative;
    overflow: hidden;
    max-width: 98%;

    @media all and (min-width: 768px) {
      max-width: 100%;
    }
    
    @media (min-width: 1280px) {
      width: 1200px;
      padding: 0;
    }
  }
  
  .results-veil {
    display: none;
    position: fixed;
    top: 0;
    left: 0;
    width:  100%;
    height: 100%;
    background-color: rgba(0,0,0,0.6);
    z-index: 700;
  
    @media all and (min-width: 580px) {
      visibility: hidden;
    }
  }
  
  .table {
    margin-bottom: 0;
    
    th {
      border-top: 0;
    }
  }
`;
