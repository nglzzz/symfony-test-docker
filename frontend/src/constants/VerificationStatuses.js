import React from 'react';
import styled from 'styled-components';
import tickGreen from 'assets/images/icons/tick-green.svg';
import tickBlue from 'assets/images/icons/tick-blue.svg';
import tickGold from 'assets/images/icons/tick-gold.svg';

export const GENERIC_STATUS = 'ALL';

const VerificationIconWrapper = styled.span`
  width: 18px;
  height: 18px;
  margin-right: 5px;
  display: inline-block;
  vertical-align: top;
  position: relative;
  border-radius: 50%;
  border-width: 2px;
  border-style: solid;
  background: 3px 6px no-repeat;
  background-size: 14px 10px;
  background-color: #e1eaf9;
  border-color: #cdd9ec;
  cursor: pointer;

  img {
    width: 10px;
    height: 10px;
    position: absolute;
    left: 2px;
    top: 3px;
  }
`;

const GreenIconWrapper = VerificationIconWrapper.extend`
  background-color: #d9eae8;
  border-color: #b0dfda;'
`;

const BlueIconWrapper = VerificationIconWrapper.extend`
  background-color: #e1eaf9;
  border-color: #cdd9ec;'
`;

const GoldIconWrapper = VerificationIconWrapper.extend`
  background-color: #eee9e0;
  border-color: #e5dac8;''
`;

export const OPTIONS = [
  {
    id: GENERIC_STATUS,
    name: 'All Types',
    component: 'All Types'
  },
  {
    id: 'OWNER_VERIFIED',
    name: 'Owner Verified',
    component: [
      <GreenIconWrapper key="wrapper">
        <img src={tickGreen} alt="Verification status icon" />
      </GreenIconWrapper>,
      <span key="type-name">Owner Verified</span>
    ]
  },
  {
    id: 'IPWE_VERIFIED',
    name: 'IPwe Verified',
    component: [
      <GoldIconWrapper key="wrapper">
        <img src={tickGold} alt="Verification status icon" />
      </GoldIconWrapper>,
      <span key="type-name">IPwe Verified</span>
    ]
  },
  {
    id: 'NATIONAL_OFFICE_VERIFIED',
    name: 'National Office Verified',
    component: [
      <BlueIconWrapper key="wrapper">
        <img src={tickBlue} alt="Verification status icon" />
      </BlueIconWrapper>,
      <span key="type-name">National Office Verified</span>
    ]
  },
  {
    id: 'NOT_REPORTED',
    name: 'Not reported',
    component: 'Not reported'
  }
];

export const getVerificationStatusName = verificationStatusId => {
  const filteredOptions = OPTIONS.filter(
    item => verificationStatusId === item.id
  );

  return filteredOptions.length === 0 ? 'Unknown' : filteredOptions[0].name;
};

export const STATUSES = [
  GENERIC_STATUS,
  'OWNER_VERIFIED',
  'IPWE_VERIFIED',
  'NATIONAL_OFFICE_VERIFIED',
  'NOT_REPORTED'
];
