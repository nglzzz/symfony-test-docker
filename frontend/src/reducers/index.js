import { combineReducers } from 'redux'
import query from './query'
import patent from './patent'
import results from './results'

const rootReducer = combineReducers({
  query,
  results,
  patent
});

export default rootReducer
