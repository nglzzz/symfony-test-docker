import {
  SEARCH_PATENTS_SUCCESS,
  SEARCH_PATENTS_ERROR,
  SEARCH_PATENTS_STARTED,
  CLEAR_RESULTS
} from 'constants/ActionTypes'

const initialState = {
  patents: null,
  error: null,
  inProgress: false,
  paginationAttributes: {
    pages: 1,
    page: 1,
    onPage: 0,
    countAll: 0
  },
  currentAssignees: [],
  sources: []
};

export default function results(state = initialState, action) {
  switch (action.type) {
    case SEARCH_PATENTS_STARTED:
      return {
        ...state,
        inProgress: true,
        error: null
      };
    case SEARCH_PATENTS_SUCCESS:
      return {
        ...state,
        inProgress: false,
        error: null,
        ...action.payload
      };
    case SEARCH_PATENTS_ERROR:
      return {
        ...state,
        error: action.payload,
        inProgress: false
      };
    case CLEAR_RESULTS:
      return initialState;
    default:
      return state
  }
}
