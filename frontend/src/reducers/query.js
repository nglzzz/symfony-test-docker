import {
  SET_PAGE,
  SET_PER_PAGE,
  SORT_BY,
  FILTER_BY,
  TOGGLE_ADVANCED_SEARCH,
  SET_GENERAL_SEARCH_SETTINGS,
  SET_ADVANCED_SEARCH_SETTINGS,
  SET_QUERY,
  CLEAR
} from 'constants/ActionTypes';

const initialState = {
  isAdvancedSearchEnabled: false,
  generalSearchQuery: '',
  advancedSearchAttributes: {
    searchNPLs: true,
    searchApplications: true,
    searchGrants: true,
    inventor: '',
    title: '',
    abstract: '',
    specification: '',
    claims: '',
    currentAssignee: ''
  },
  sortingField: null,
  filteringAttributes: {
    verificationStatus: [],
    currentAssignees: [],
    priorityYears: {
      from: null,
      to: null
    },
    documentSource: [],
    programTypes: [],
  },
  paginationAttributes: {
    perPage: 20,
    page: 1
  }
};

export default function query(state = initialState, action) {
  switch (action.type) {
    case SORT_BY:
      return {
        ...state,
        sortingField: action.payload
      };
    case FILTER_BY:
      return {
        ...state,
        filteringAttributes: {
          ...state.filteringAttributes,
          ...action.payload
        }
      };
    case TOGGLE_ADVANCED_SEARCH:
      return {
        ...state,
        isAdvancedSearchEnabled: action.payload
      };
    case SET_ADVANCED_SEARCH_SETTINGS:
      return {
        ...state,
        advancedSearchAttributes: action.payload
      };
    case SET_GENERAL_SEARCH_SETTINGS:
      return {
        ...state,
        generalSearchQuery: action.payload
      };
    case SET_PAGE:
      return {
        ...state,
        paginationAttributes: {
          ...state.paginationAttributes,
          page: action.payload
        }
      };
    case SET_PER_PAGE:
      return {
        ...state,
        paginationAttributes: {
          ...state.paginationAttributes,
          perPage: action.payload
        }
      };
    case SET_QUERY:
      return {
        ...action.payload
      };
    case CLEAR:
      return {
        ...initialState,
        isAdvancedSearchEnabled: state.isAdvancedSearchEnabled,
        generalSearchQuery: state.generalSearchQuery
      };
    default:
      return state;
  }
}
