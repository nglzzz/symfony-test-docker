import {
  VERIFICATIONS_REQUEST_SUCCESS,
  VERIFICATIONS_REQUEST_STARTED,
  VERIFICATIONS_REQUEST_ERROR,
  SELECT_PATENT
} from 'constants/ActionTypes';

const initialState = {
  selectedPatent: null,
  verifications: [],
  inProgress: false,
  error: null
};

export default function results(state = initialState, action) {
  switch (action.type) {
    case SELECT_PATENT:
      return {
        ...state,
        selectedPatent: action.payload
      };
    case VERIFICATIONS_REQUEST_STARTED:
      return {
        ...state,
        inProgress: true,
        error: null
      };
    case VERIFICATIONS_REQUEST_SUCCESS:
      return {
        ...state,
        inProgress: false,
        error: null,
        verifications: action.payload
      };
    case VERIFICATIONS_REQUEST_ERROR:
      return {
        ...state,
        error: action.payload,
        inProgress: false
      };
    default:
      return state;
  }
}
