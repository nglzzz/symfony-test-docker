import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Checkbox from 'components/common/Checkbox';
import SearchTips from 'components/common/SearchTips';
import ResultsQuantity from 'components/results/ResultsQuantity';
import {LICENSE_TYPE, ONLINE_SALE_TYPE} from "constants/ProgramTypes";

const AdvancedSearchWrapper = styled.div`
  padding-bottom: 15px;

  .main & {
    background: rgba(255, 255, 255, 0.8);

    @media all and (min-width: 768px) {
      background: transparent;
    }

    @media (min-width: 1200px) {
      width: 100%;
      margin: 10px 0 0;
    }
  }
  
  .types-wrap {
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    @media all and (min-width: 648px) {
      flex-direction: row;
    }
    .results-quantity {
      font-size: 12px;
      padding-left: 11px;
      padding-right: 17px;
      @media all and (min-width: 768px) {
        padding-right: 11px;
      }
    }
  }

  @media all and (min-width: 768px) {
    background: transparent;
    margin: 0 20px;
    padding: 0 0 15px;
  }

  @media (min-width: 1200px) {
    width: 1200px;
    margin: auto;
  }
`;

const TypesText = styled.div`
  margin-right: 10px;
  font-size: 13px;
  font-family: 'Roboto', sans-serif;
  font-weight: bold;
  line-height: 16px;
  color: #212529;
  @media all and (min-width: 768px) {
    margin-right: 20px;
  }
`;

const Checkboxes = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 0 15px 15px;
  
  div {
    font-size: 13px;
    font-family: 'Roboto', sans-serif;
    line-height: 18px;
    color: #212529;
  }
  
  .checkbox {
    @media all and (max-width: 468px) {
      padding-right: 10px;
    }
  }

  @media all and (min-width: 768px) {
    justify-content: flex-start;
    margin: 0 10px 10px 10px;
  }
`;

const InputField = styled.input`
  display: block !important;
  float: left;
  margin: 0 0 10px;
  width: 100%;
  height: 35px;
  padding: 0 13px;
  font-size: 12px;
  border: 1px solid #dbd7cd;

  .main & {
    @media (min-width: 1200px) {
      margin: 0 10px 10px 10px;
      width: 303px;
    }
  }

  @media all and (min-width: 768px) {
    display: block !important;
    float: left;
    margin: 0 1% 10px 1%;
    width: 48%;
    height: 35px;
    padding: 0 13px;
    font-size: 12px;
    border: 1px solid #dbd7cd;
  }

  @media (min-width: 1200px) {
    margin: 0 10px 10px 10px;
    width: calc((100% - 60px)/3);
  }
`;

const SearchButtonWrapper = styled.div`
  position: relative;
  padding-top: 30px;
  margin: 0 0;

  @media all and (min-width: 768px) {
    padding-top: 5px;
  }
  @media all and (max-width: 768px) {
    margin: 0 15px;
  }

  .button-flex-wrap {
    display: flex;
    justify-content: center;
  }
  .verification-filter-wrap {
    display: flex;
    flex-direction: row;
    border-top: 1px solid #c1c1c1;
    padding-top: 25px;
    margin-bottom: 39px;
    @media all and (max-width: 768px) {
      flex-direction: column;
    }
  }
  .gpr-block {
    display: flex;
    padding: 15px 31px 0 75px;
    height: 53px;
    background: url(../images/Global_Patent_Registry.svg) no-repeat 5px -2px;
    background-size: 58px;
    @media all and (max-width: 768px) {
      margin-bottom: 25px;
    }
  }
  .gpr-logo {
    font: bold 18px 'Raleway', sans-serif;
    margin-bottom: 0;
  }
  .checkboxes-wrap {
    padding-left: 32px;
    border-left: 1px solid #c1c1c1;
    @media all and (max-width: 768px) {
      padding-left: 0;
      border-left: none;
    }
  }
  .checkboxes-wrap > div {
    margin-left: 0;
    margin-bottom: 0;
    @media (max-width: 1030px) {
      flex-direction: column;
    }
  }
  .checkboxes-wrap > div > span {
    margin-right: 9px;
    @media (max-width: 1030px) {
      margin-bottom: 15px;
    }
  }
  .checkboxes-wrap > div > span > label {
    display: flex;
    margin-bottom: 0;
  }
  .checkboxes-wrap > div > span > label > div:last-of-type {
    display: flex;
  }
  .checkbox-title {
    font: bold 14px 'Roboto', sans-serif;
    margin-bottom: 16px;
  }
  .checkbox-label {
    margin-right: 13px;
  }
`;

const TooltipTrigger = styled.a`
  display: block;
  width: 21px;
  height: 21px;
  text-align: center;
  font-weight: bold;
  background-color: #ffffff;
  color: #a8a8ae;
  border: 1px solid #d7d7db;
  border-radius: 20px;
  margin: 2px 0 0 9px;
  cursor: pointer;
  &:hover {
    background-color: #f9d9de;
    border-color: #f9d9de;
  }
`;

const TickIcon = styled.span`
  display: inline-block;
  margin-top: -2px;
  width: 20px;
  height: 20px;
  border-width: 2px;
  border-style: solid;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: 12px;
  border-radius: 50%;
  &.golden {
    background-color: #eee9e0;
    border-color: #e5dac8;
    background-image: url(../images/icons/tick-gold.svg);
  }
  &.blue {
    background-color: #d9eae8;
    border-color: #b0dfda;
    background-image: url(../images/icons/tick-green.svg);
  }
  &.green {
    background-color: #e1eaf9;
    border-color: #cdd9ec;
    background-image: url(../images/icons/tick-blue.svg);
  }
`;

const Button = styled.button`
  display: block;
  height: 37px;
  width: 115px;
  margin: 0 10px;
  color: #ffffff;
  border: 0;
  font-family: 'Roboto Slab', sans-serif;
  font-size: 15px;
  font-weight: bold;

  &:hover {
    background-color: #0e8268;
    cursor: pointer;
  }

  @media all and (min-width: 768px) {
    &:hover {
      background-color: #0e8268;
    }
  }
`;

const SearchButton = Button.extend`
  width: 100%;
  height: auto;
  margin: 0 auto;
  padding: 15px 30px;
  background-color: #00a693;
  text-transform: uppercase;
  outline: none;

  &:hover {
    background-color: #0e8268;
  }

  @media all and (min-width: 768px) {
    width: auto;
    height: auto;
    padding: 15px 30px;
    background-color: #00a693;
    text-transform: uppercase;
    outline: none;
  }
`;

const ApplyButton = Button.extend`
  background: #00a693 url(../images/icons/tick.svg) 15px 50% no-repeat;
  background-size: 10px 10px;
  padding-left: 20px;

  @media all and (min-width: 768px) {
    background: #00a693 url(../images/icons/tick.svg) 15px 50% no-repeat;
    background-size: 10px 10px;
  }
`;

const ClearButton = Button.extend`
  background: #fff url(../images/icons/cross.svg) 15px 50% no-repeat;
  background-size: 10px 10px;
  color: #888;
  &:hover {
    background: #fff url(../images/icons/cross-green.svg) 15px 50% no-repeat;
    background-size: 10px 10px;
    color: #00a693;
  }

  @media all and (min-width: 768px) {
    background: #fff url(../images/icons/cross.svg) 15px 50% no-repeat;
    background-size: 10px 10px;
    color: #888;
    &:hover {
      background: #fff url(../images/icons/cross-green.svg) 15px 50% no-repeat;
      background-size: 10px 10px;
      color: #00a693;
    }
  }
`;

const InputGroup = styled.div`
  margin: 0 15px 15px;

  &::before,
  &::after {
    content: '';
    display: block;
    clear: both;
  }

  @media all and (min-width: 768px) {
    margin: 0 0 10px 0;

    &::before,
    &::after {
      content: '';
      display: block;
      clear: both;
    }
  }
`;

class AdvancedSearch extends React.Component {
  static propTypes = {
    onSearch: PropTypes.func.isRequired,
    onClear: PropTypes.func,
    toggleProgramType: PropTypes.func.isRequired,

    values: PropTypes.shape({
      searchGrants: PropTypes.bool.isRequired,
      searchApplications: PropTypes.bool.isRequired,
      searchNPLs: PropTypes.bool.isRequired,
      title: PropTypes.string.isRequired,
      inventor: PropTypes.string.isRequired,
      abstract: PropTypes.string.isRequired,
      specification: PropTypes.string.isRequired,
      claims: PropTypes.string.isRequired,
      currentAssignee: PropTypes.string.isRequired
    }).isRequired,
    paginationAttributes: PropTypes.shape({
      countAll: PropTypes.number.isRequired,
      onPage: PropTypes.number.isRequired,
      page: PropTypes.number.isRequired,
      pages: PropTypes.number.isRequired,
      perPage: PropTypes.number.isRequired
    }),
    selectedProgramTypes: PropTypes.arrayOf(PropTypes.string).isRequired,
    isMain: PropTypes.bool.isRequired
  };

  static defaultProps = {
    paginationAttributes: null,
    onClear: () => {}
  };

  constructor(props) {
    super(props);

    this.state = {
      values: {
        ...props.values
      },
      selectedVerificationTypes: []
    };

    this.toggleType = this.toggleType.bind(this);
    this.toggleCheckbox = this.toggleCheckbox.bind(this);
    this.changeInputField = this.changeInputField.bind(this);
    this.search = this.search.bind(this);
  }

  componentWillReceiveProps(props) {
    this.setState({
      values: {
        ...props.values
      }
    });
  }

  toggleType = (type, state) => {
    const statuses = this.state.selectedVerificationTypes;

    const currentStatus = statuses.includes(type);

    if (typeof state === 'undefined') {
      state = !currentStatus;
    }

    const verificationStatuses = [...statuses.filter(item => item !== type)];

    if (state) {
      verificationStatuses.push(type);
    }

    if (verificationStatuses) {
      this.setState({
        selectedVerificationTypes: verificationStatuses
      });
    }
  };

  search = () => {
    this.props.onSearch(this.state.values, {
      verificationStatus: this.state.selectedVerificationTypes,
      programTypes: this.props.selectedProgramTypes,
    });
  };

  clear = () => {
    this.props.onClear();
  };

  toggleCheckbox = event => {
    const field = event.target.id;

    this.setState(oldState => ({
      values: {
        ...oldState.values,
        [field]: !oldState.values[field]
      }
    }));
  };

  changeInputField = event => {
    const field = event.target.id;
    const value = event.target.value;

    this.setState(oldState => ({
      values: {
        ...oldState.values,
        [field]: value
      }
    }));
  };

  render() {
    return (
      <AdvancedSearchWrapper>
        <Checkboxes>
          <Checkbox
            clickHandler={this.toggleCheckbox}
            name="searchGrants"
            state={this.state.values.searchGrants}
          >
            Grant
          </Checkbox>
          <Checkbox
            clickHandler={this.toggleCheckbox}
            name="searchApplications"
            state={this.state.values.searchApplications}
          >
            Application
          </Checkbox>
          <Checkbox
            clickHandler={this.toggleCheckbox}
            name="searchNPLs"
            state={this.state.values.searchNPLs}
          >
            NPL
          </Checkbox>
        </Checkboxes>

        <InputGroup>
          <InputField
            onChange={this.changeInputField}
            value={this.state.values.title}
            id="title"
            type="text"
            placeholder="Title"
          />
          <InputField
            onChange={this.changeInputField}
            value={this.state.values.abstract}
            id="abstract"
            type="text"
            placeholder="Abstract"
          />
          <InputField
            onChange={this.changeInputField}
            value={this.state.values.claims}
            id="claims"
            type="text"
            placeholder="Claims"
          />
          <InputField
            onChange={this.changeInputField}
            value={this.state.values.inventor}
            id="inventor"
            type="text"
            placeholder="Inventor"
          />
          <InputField
            onChange={this.changeInputField}
            value={this.state.values.specification}
            id="specification"
            type="text"
            placeholder="Specification"
          />
          <InputField
            onChange={this.changeInputField}
            value={this.state.values.currentAssignee}
            id="currentAssignee"
            type="text"
            placeholder="Current Assignee"
          />
        </InputGroup>
        <div className="types-wrap">
          <Checkboxes>
            <TypesText>Show types:</TypesText>
            <Checkbox
              clickHandler={() => this.props.toggleProgramType(ONLINE_SALE_TYPE)}
              name="showForSale"
              state={this.props.selectedProgramTypes.includes(ONLINE_SALE_TYPE)}
            >
              For sale
            </Checkbox>
            <Checkbox
              clickHandler={() => this.props.toggleProgramType(LICENSE_TYPE)}
              name="showForLicense"
              state={this.props.selectedProgramTypes.includes(LICENSE_TYPE)}
            >
              For license
            </Checkbox>
          </Checkboxes>
          {this.props.paginationAttributes && (
            <ResultsQuantity
              paginationAttributes={this.props.paginationAttributes}
            />
          )}
        </div>

        {this.props.isMain ? (
          <SearchButtonWrapper>
            <div className="verification-filter-wrap">
              <div className="gpr-block">
                <p className="gpr-logo">Global Patent Registry</p>
                <TooltipTrigger
                  href="https://registry.ipwe.com/"
                  target="_blank"
                >
                  ?
                </TooltipTrigger>
              </div>
              <div className="checkboxes-wrap">
                <p className="checkbox-title">Select Verification Type:</p>
                <Checkboxes>
                  <Checkbox
                    clickHandler={() => this.toggleType('OWNER_VERIFIED')}
                    name="filterOwners"
                    state={this.state.selectedVerificationTypes.includes(
                      'OWNER_VERIFIED'
                    )}
                  >
                    <span className="checkbox-label">Owner Verified</span>
                    <TickIcon className="green">&nbsp;</TickIcon>
                  </Checkbox>
                  <Checkbox
                    clickHandler={() => this.toggleType('IPWE_VERIFIED')}
                    name="filterIPWEs"
                    state={this.state.selectedVerificationTypes.includes(
                      'IPWE_VERIFIED'
                    )}
                  >
                    <span className="checkbox-label">IPWe Verified</span>
                    <TickIcon className="blue">&nbsp;</TickIcon>
                  </Checkbox>
                  <Checkbox
                    clickHandler={() =>
                      this.toggleType('NATIONAL_OFFICE_VERIFIED')
                    }
                    name="filterNationalOffices"
                    state={this.state.selectedVerificationTypes.includes(
                      'NATIONAL_OFFICE_VERIFIED'
                    )}
                  >
                    <span className="checkbox-label">
                      National Office Verified
                    </span>
                    <TickIcon className="golden">&nbsp;</TickIcon>
                  </Checkbox>
                </Checkboxes>
              </div>
            </div>
            <SearchButton onClick={this.search}>
              <span className="icon-search" />
              &nbsp;
              <span>Search Patent</span>
            </SearchButton>
          </SearchButtonWrapper>
        ) : (
          <SearchButtonWrapper>
            <div className="button-flex-wrap">
              <ClearButton onClick={this.clear}>
                <span>Clear</span>
              </ClearButton>
              <ApplyButton onClick={this.search}>
                <span>Search</span>
              </ApplyButton>
            </div>
          </SearchButtonWrapper>
        )}
        <SearchTips shouldBeCentered />
      </AdvancedSearchWrapper>
    );
  }
}

export default AdvancedSearch;
