import React from 'react';

import StaticPage from 'components/common/StaticPage';

class TermsPage extends React.Component {
  render() {
    return (
      <StaticPage
        content={
          <div>
            <p>
              All use of Zuse Analytics and related websites, including
              www.zuseip.com and www.ipwe.com (collectively “Zuse”) is subject
              to the terms and conditions set forth below (the "Terms"). Any use
              of such web pages constitutes the user's agreement to abide by the
              Terms. These Terms may be amended from time to time by posting the
              amended terms on this site.
            </p>
            <h2>Zuse’s Golden Rule</h2>
            <p>
              <strong>
                We do not track. We do not advertise. You own your information.
                Use Zuse in full confidence that your secrets are safe with you.
              </strong>
            </p>

            <div className="row">
              <div className="col-md-6">
                <h4>What We Collect</h4>
                <p>
                  IPwe, Inc. (collectively “IPwe” or “we”) take the privacy of
                  Zuse users (the “Users” or “you”) seriously. Many of the users
                  of Zuse are engaged in innovation and involved in R&D—they are
                  exploring new ideas that may be patentable or trade secrets
                  that have incredible value—why would you ever use a website
                  that snoops on what you are researching on an aggregate or
                  individual level? We endeavor to collect as little information
                  as possible about Zuse Users. We feel strongly that the best
                  way to safeguard our Users’ privacy is to not collect their
                  personal information or track their interactions with Zuse in
                  the first place. Zuse is a powerful patent tool and it does
                  not require its Users to turn over extensive personal
                  information to us. In certain circumstances, we collect
                  certain information about Users, such as email addresses, for
                  the purposes of verifying access to Zuse. In these instances,
                  the collected information is only used for its intended
                  purpose, verification, and nothing else. When collecting
                  personal data, we will always tell you why we are collecting
                  the data and for what it is being used. Any other data
                  regarding Users that is collected as part of the
                  communications process between our computers and the Users’ is
                  only used to facilitate the communications process and is not
                  permanently stored.
                </p>

                <h4>You Control Your Personal Information</h4>
                <p>
                  With Zuse, you control your personal information and how it is
                  used. You can always update your information with us. If you
                  want, you can remove your personal information from Zuse. This
                  information is then deleted from our computers, with the
                  possible exception of data stored as part of archives. Contact
                  us at dpo@ipwe.com to update your information, to request that
                  we delete your information, or to ask us a question regarding
                  your information or our privacy policies. Outside of a legally
                  valid request from a government authority, we never share your
                  personal information with third parties. To the extent that we
                  use third parties to implement Zuse, these third parties are
                  not authorized to use this information for any other purpose
                  than to provide the Zuse service. If we experience a security
                  breach that puts your personal information at risk, we will
                  notify you of the breach. We do not sell User information to
                  third parties and we do not use this information for
                  advertising.
                </p>

                <h4>We Do Not Track Your Searches</h4>
                <p>
                  We do not track your searches or other activity on Zuse. Zuse
                  does not use cookies, pixel tags, or any other kind of
                  tracking technology. In certain Zuse services, Users can save
                  their own searches. Users can delete these searches whenever
                  they like and Zuse will not retain them after deletion. We do
                  not use these searches for any other purpose but saving them
                  for the User. It is a decision we made — we want Zuse Users to
                  be confident we are not tracking their searches. If you delete
                  saved information, that information is deleted from our
                  computers. We have no way to recover it.
                </p>

                <h4>Why Our Privacy Policies Matter</h4>
                <p>
                  Zuse is the world’s most advanced patent analytics platform.
                  Startups, corporations, universities, research institutes, and
                  individuals around the world use Zuse for critical analysis of
                  the patent landscape. These entities trust Zuse to provide
                  them with accurate and useful results. When these entities
                  execute their searches on Zuse, they are giving us a lot of
                  information about their business and research. Patent searches
                  reveal a lot of sensitive information about the searchers.
                  People who search for patents do so to learn about potentially
                  new technologies they plan to patent or maintain as trade
                  secrets, competitors’ intellectual property, as part of legal
                  analysis, to identify acquisition targets, or to perform other
                  strategic tasks. In other words, a patent search can divulge a
                  lot of important information about the searcher. We are aware
                  of this and that is why we do not track our Users’ activity on
                  Zuse. If we did, our Users would have no confidence that we
                  were not collecting sensitive data from them. Many other
                  providers of patent analytics tools do not feel the same way.
                  They track their customers’ use of their services and employ
                  the data they glean from their customers’ usage for their own
                  ends. This information becomes even more valuable when these
                  other services combine it with user personal information. We
                  understand that this information is valuable, but we also
                  believe that it belongs to our Users. That is why we do not
                  track User activity on Zuse and we keep to a minimum the
                  information we ask for from our Users.
                </p>
              </div>

              <div className="col-md-6">
                <h4>Electronic Communications</h4>
                <p>
                  When you visit <a
                    target="_blank"
                    href={'http://www.zuseip.com/'}
                  >
                    our
                  </a>{' '}
                  websites or send e-mails to us, you are communicating with us
                  electronically. You hereby consent to receive communications
                  from us electronically. You can reach us at any time by
                  sending an email to our data protection officer at{' '}
                  <a href={'mailto:dpo@ipwe.com'}>dpo@ipwe.com</a>. We will
                  communicate with you by e-mail or by posting notices on this
                  site. You hereby agree that all agreements, notices,
                  disclosures and other communications that we provide
                  electronically satisfy any legal requirement that such
                  communications be in writing. We do not use electronic
                  communications to advertise to Users of Zuse nor do we give
                  out Users’ contact information to third-party advertisers.
                </p>

                <h4>Intellectual Property</h4>
                <p>
                  All content provided by Zuse is owned by or licensed to IPwe
                  and protected by United States and international copyright
                  laws. The content may not be reproduced, transmitted or
                  distributed without the prior written consent of IPwe.
                </p>
                <p>
                  All trademarks, service marks, trade names and logos related
                  to Zuse are trademarks of IPwe.
                </p>

                <h4>Disclaimer of Warranties and Limitation of Liability</h4>
                <p>
                  Zuse and all related sites and services are provided by us on
                  an "as is" and "as available" basis. To the maximum extent
                  allowed by law, IPwe and its affiliates and subsidiaries make
                  no representations or warranties of any kind, express or
                  implied, as to the operations of these sites and services, or
                  the information, content or materials included thereon. All
                  Users of Zuse and the related sites and services hereby
                  expressly agree that their use of these sites and services is
                  at their sole risk.
                </p>

                <h4>European Union General Data Protection Regulation</h4>
                <p>
                  IPwe, Inc. (collectively “IPwe” or “we”) is a Delaware
                  corporation with a mailing address at 2633 McKinney Avenue,
                  suite 130-740, Dallax, TX 75204. Our Delaware Division of
                  Corporations file number is 6703755. Our servers are based in
                  the USA and by sending us their information, users of Zuse
                  (the “Users”) are consenting to their data being stored in the
                  USA. IPwe is the controller of any data stored on behalf of
                  the Users. We endeavor to collect as little data as possible
                  from the Users. As part of the login process, we may ask for
                  limited personal information (for example, name, email, etc.)
                  to verify Users. The data that we collect we store
                  indefinitely or until the User requests that we delete this
                  data. We only use this information for verification purposes
                  and for nothing else. As a convenience to our Users, we save
                  User searches. This information is saved only so that Users
                  can easily recall these searches for their own research. Users
                  can delete saved searches at any time.
                </p>
                <p>
                  Contact us at <a href={'mailto:dpo@ipwe.com'}>dpo@ipwe.com</a>{' '}
                  to update your information, to request that we delete your
                  information, or to ask us a question regarding your
                  information or our privacy policies.
                </p>
              </div>
            </div>
          </div>
        }
        h1="Terms of Service and Privacy Policy"
      />
    );
  }
}

export default TermsPage;
