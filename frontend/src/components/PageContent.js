import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const H1 = styled.h1`
  padding: 20px;
  text-align: center;
  font-size: 24px;
  color: #000;
`;

const Content = styled.div`
  padding: 50px 40px;
  background-color: #fff;
`;

const Button = styled.button`
  display: block;
  height: 37px;
  width: 115px;
  margin: 0 10px;
  color: #ffffff;
  border: 0;
  font-family: 'Roboto Slab', sans-serif;
  font-size: 15px;
  font-weight: bold;

  &:hover {
    background-color: #0e8268;
    cursor: pointer;
  }

  @media all and (min-width: 768px) {
    &:hover {
      background-color: #0e8268;
    }
  }
`;

const BackButton = Button.extend`
  width: 100%;
  height: auto;
  margin: 30px 0 0 0;
  padding: 15px 30px;
  background-color: #00a693;
  text-transform: uppercase;
  outline: none;

  &:hover {
    background-color: #0e8268;
  }

  @media all and (min-width: 768px) {
    width: auto;
    height: auto;
    padding: 15px 30px;
    background-color: #00a693;
    text-transform: uppercase;
    outline: none;
  }
`;

const ArrowLeft = styled.span`
  width: 15px;
  height: 15px;
  margin: -3px 5px 0 0;
  display: inline-block;
  vertical-align: middle;
  background: url(../images/icons/left-arrow.svg) no-repeat;
`;

class PageContent extends React.Component {
  static propTypes = {
    onReturn: PropTypes.func.isRequired,
    h1: PropTypes.string.isRequired,
    content: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]).isRequired
  };

  render() {
    return (
      <div className="container">
        <H1>{this.props.h1}</H1>
        <Content>
          {this.props.content}

          <BackButton onClick={this.props.onReturn}>
            <ArrowLeft />
            &nbsp;
            <span>Back</span>
          </BackButton>
        </Content>
      </div>
    );
  }
}

export default PageContent;
