import React from 'react';
import pure from 'recompose/pure';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const FooterWrapper = styled.div`
  padding: 16px 0;
  width: 100%;
  background: #e9f1f0;
  color: #b6cfcb;
  overflow: hidden;

  ${props =>
    props.isFixed
      ? `
    position: fixed;
    bottom: 0;
    z-index: 10;
  `
      : `
    position: static;
  `};
`;

const Links = styled.div`
  overflow: hidden;
  margin: auto;
  text-align: center;
  font-size: 12px;

  @media all and (min-width: 768px) {
    float: left;
    margin-left: 15px;
  }
`;

const StyledLink = styled(Link)`
  color: #87afa8;
`;

const SocialLinks = styled.div`
  display: none;
  float: right;
  margin-right: 30px;
  vertical-align: middle;

  @media all and (min-width: 768px) {
    display: block;
  }
`;

const SocialLink = styled.a`
  margin-left: 20px;

  & span::before {
    color: #7ba7a2;
    font-size: 18px;
  }
`;

const Copyright = styled.div`
  position: absolute;
  left: 50%;
  margin-left: -75px;
`;

function Footer({ isFixed }) {
  return (
    <FooterWrapper isFixed={isFixed}>
      <div className="container">
        <Links>
          <StyledLink to="/terms">
            Terms of Service and Privacy Policy
          </StyledLink>
        </Links>
        <Copyright>operated by IPwe, Inc.</Copyright>
        <SocialLinks>
          <SocialLink href="https://twitter.com/ipwe_">
            <span className="icon-twitter" />
          </SocialLink>
          <SocialLink href="https://www.linkedin.com/company/ipwe-com/">
            <span className="icon-linkedin" />
          </SocialLink>
        </SocialLinks>
      </div>
    </FooterWrapper>
  );
}

Footer.propTypes = {
  isFixed: PropTypes.bool
};

Footer.defaultProps = {
  isFixed: false
};

export default pure(Footer);
