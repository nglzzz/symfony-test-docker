import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types';
import AdvancedSearch from 'components/AdvancedSearch';
import GeneralSearch from 'components/GeneralSearch';

const SearchFormWrapper = styled.div`
  margin: auto;
  padding: 0 0 5px;
  overflow: hidden;
  background: #ffffff;
  
  &.main {
    background: transparent;

    @media all and (min-width: 768px) {
      background: rgba(255, 255, 255, .8);
      margin: 200px 15px 0;
      padding: 0 15px 15px 15px;
    }

    @media (min-width: 1200px) {
      width: 1030px;
      padding: 0 30px 20px 30px;
      margin: 257px auto auto;
    }
  }

  @media all and (min-width: 768px) {
    box-shadow: 0 0 7px 1px rgba(219,215,205,1);
    background-color: #fff;
  }

  @media (min-width: 1200px) {
    width: 100%;
    overflow: hidden;
    padding-bottom: 5px;
  }
`;

const Tabs = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 15px 0 13px;
  width: 100%;
  text-transform: uppercase;
  
  .main & {
    background: rgba(255, 255, 255, .8);

    @media all and (min-width: 768px) {
      background: transparent;
    }

    @media (min-width: 1200px) {
      width: 100%;
      margin-left: auto;
      margin-right: auto;
    }
  }

  @media all and (min-width: 768px) {
    justify-content: center;
    position: static;
    background: transparent;
  }

  @media (min-width: 1200px) {
    width: 1200px;
    margin: auto;
  }
`;

const Tab = styled.a`
  padding: 6px 0;
  border-bottom: 1px solid transparent;
  margin: 0 15px;
  
  && {
    color: #000000;
    text-decoration: none;
  }

  &.active {
    color: #00a693;
    border-color: #00a693;
  }
`;

function SearchForm({
  handleGeneralSearchChange,
  onSearch,
  onClear,
  toggleAdvancedSearch,
  toggleProgramType,

  generalSearchQuery,
  advancedSearchAttributes,
  isAdvancedSearchEnabled,
  paginationAttributes,
  selectedProgramTypes,

  isMain
}) {
  const toggleAdvanced = (event) => {
    event.preventDefault();

    toggleAdvancedSearch(!event.target.attributes.open);
  };

  const className = isMain ? 'main' : 'results';

  return (
    <SearchFormWrapper className={className}>
      <Tabs>
        <Tab href="#" onClick={toggleAdvanced} open className={!isAdvancedSearchEnabled && "active"}>
          Basic search
        </Tab>
        <Tab href="#" onClick={toggleAdvanced} className={isAdvancedSearchEnabled && "active"}>
          Advanced search
        </Tab>
        <Tab href="/offers">
          For sale or license
        </Tab>
      </Tabs>

      { isAdvancedSearchEnabled ? (
        <AdvancedSearch
          onSearch={onSearch}
          onClear={onClear}
          values={advancedSearchAttributes}
          paginationAttributes={paginationAttributes}
          selectedProgramTypes={selectedProgramTypes}
          toggleProgramType={toggleProgramType}
          isMain={isMain} />
      ) : (
        <GeneralSearch
          generalSearchQuery={generalSearchQuery}
          onSearch={onSearch}
          handleQueryChange={handleGeneralSearchChange}
          paginationAttributes={paginationAttributes}
          selectedProgramTypes={selectedProgramTypes}
          toggleProgramType={toggleProgramType}
          isMain={isMain} />
      ) }
    </SearchFormWrapper>
  );
}

SearchForm.propTypes = {
  handleGeneralSearchChange: PropTypes.func.isRequired,
  onSearch: PropTypes.func.isRequired,
  onClear: PropTypes.func,
  toggleAdvancedSearch: PropTypes.func.isRequired,
  toggleProgramType: PropTypes.func.isRequired,

  generalSearchQuery: PropTypes.string.isRequired,
  advancedSearchAttributes: PropTypes.shape({
    searchGrants: PropTypes.bool.isRequired,
    searchApplications: PropTypes.bool.isRequired,
    searchNPLs: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    inventor: PropTypes.string.isRequired,
    abstract: PropTypes.string.isRequired,
    specification: PropTypes.string.isRequired,
    claims: PropTypes.string.isRequired,
    currentAssignee: PropTypes.string.isRequired
  }).isRequired,
  selectedProgramTypes: PropTypes.arrayOf(PropTypes.string).isRequired,
  isAdvancedSearchEnabled: PropTypes.bool.isRequired,
  isMain: PropTypes.bool,
  paginationAttributes: PropTypes.shape({
    countAll: PropTypes.number.isRequired,
    onPage: PropTypes.number.isRequired,
    page: PropTypes.number.isRequired,
    pages: PropTypes.number.isRequired,
    perPage: PropTypes.number.isRequired
  }),
};

SearchForm.defaultProps = {
  isMain: false,
  onClear: () => {},
  paginationAttributes: null
};

export default SearchForm
