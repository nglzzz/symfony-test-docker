import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import DatePicker from 'components/common/DatePicker';
import Select from 'components/common/Select';
import StaticSelect from 'components/common/StaticSelect';
import SelectedTags from 'components/results/SelectedTags';
import $ from 'jquery';
import SelectWithCheckboxes from 'components/common/SelectWithCheckboxes';
import { OPTIONS } from 'constants/VerificationStatuses';

const FilterBlock = styled.div`
  width: 100%;
  background-color: #ffffff;
  margin: 1px auto 0;
  position: relative;
  padding: 6px 0 12px;
  box-shadow: 0 2px 5px 1px rgba(228, 235, 234, 1);

  @media all and (min-width: 768px) {
    margin-top: 15px;
  }

  @media all and (min-width: 1280px) {
    width: 1200px;
    border: 1px solid #d8e5e3;
  }

  .filter-wrapper-tablet-hide {
    left: -55%;

    @media all and (max-width: 576px) {
      left: -97%;
    }
  }
`;

const FilterTrigger = styled.div`
  padding-top: 9px;
  padding-left: 9px;

  @media all and (min-width: 768px) {
    padding-top: 17px;
  }
`;

const FilterLink = styled.button`
  padding: 3px;
  color: #00a693;
  background-color: #fff;
  border: none;
  cursor: pointer;
  outline: none;

  .icon-filter:before {
    font-size: 20px;
    position: relative;
    top: 3px;
  }

  &:focus {
    outline: none;
  }
`;

const FilterBean = styled.div`
  float: left;
  width: 100%;
  padding: 0 15px;

  @media all and (min-width: 768px) and (max-width: 1000px) {
    padding-left: 7px;
    padding-right: 7px;
  }

  &.doc-source {
    @media all and (min-width: 768px) and (max-width: 991px) {
      padding-left: 21px;
      padding-right: 0;
    }
    @media all and (min-width: 992px) and (max-width: 1200px) {
      padding-left: 32px;
      padding-right: 0;
    }
  }

  &.filter-switch {
    display: flex;
    justify-content: flex-end;
    padding-top: 8px;
    padding-bottom: 4px;

    .filter-switch-title {
      @media all and (min-width: 768px) {
        display: flex;
      }
      @media all and (max-width: 767px) {
        display: none;
      }
    }

    @media all and (min-width: 576px) {
      padding-bottom: 0;
    }

    @media all and (min-width: 768px) {
      flex-direction: column;
      padding-top: 0;
    }
  }

  @media all and (min-width: 1280px) {
    margin: 0;
  }

  .from {
    font-size: 12px;

    @media all and (min-width: 768px) {
      line-height: 38px;
      display: none;
    }
  }

  .to {
    font-size: 12px;

    @media all and (min-width: 768px) {
      line-height: 38px;
      min-width: 6%;
      margin-left: 1%;
      margin-right: 1%;
      display: block;
    }
  }

  &.no-bottom-margin {
    && {
      margin-bottom: 0;
    }
  }
`;

const FilterTitle = styled.h3`
  color: #2e2e2e;
  font: bold 13px 'Roboto', sans-serif;
  padding: 5px 0 2px 0;
  margin: 5px 0 3px 0;

  .filter-switch & {
    margin-right: 10px;
  }
  &.gpr-title {
    padding-left: 25px;
    background: url(../images/Global_Patent_Registry.svg) no-repeat left 2px;
    background-size: 20px;
  }
  @media all and (min-width: 768px) and (max-width: 1000px) {
    min-height: 30px;
    display: flex;
    align-items: center;
    padding: 0;
    margin: 10px 0 5px 0;
    &.gpr-title {
      padding-left: 0;
      background: none;
    }
  }
`;

const FilterVeil = styled.div`
  display: none;
  background-color: rgba(62, 78, 76, 0.4);
  position: absolute;
  top: 62px;
  left: 0;
  padding-left: 0;
  padding-right: 0;
  min-height: 450px;
  z-index: 120;
`;

const WrapperDesktop = styled.div`
  height: inherit;
  background-color: #ffffff;
`;

const WrapperMobile = styled.div`
  height: inherit;
  background-color: #ffffff;
  min-height: inherit;
  padding-left: 0;
  padding-right: 0;
  position: absolute;
  top: 0;
  left: -97%;
  transition-property: left;
  transition-duration: 1s;
  transition-delay: 0s;

  .filter-bean {
    float: none;
    margin-bottom: 15px;

    &.mob-select-wr {
      padding-right: 0;
      padding-left: 0;
      border-top: 1px solid #dddddd;
    }
  }

  .mob-datepicker-wr {
    margin-top: 30px;
    .filter-date {
      float: none;
    }
  }

  &.filter-wrapper-show {
    left: 0;
  }

  @media all and (min-width: 576px) {
    left: -50%;
  }

  @media all and (min-width: 768px) {
    display: flex;
  }

  @media all and (min-width: 1200px) {
    display: table-cell;
  }
`;

const DatePickerWrapper = styled.div`
  display: flex;
`;

const SelectWrapper = styled.form`
  position: relative;
  width: 100%;
`;

const SortingSelect = styled.select`
  -webkit-appearance: none;
  -moz-appearance: none;
  padding: 8px 20px 4px 0;
  background: #ffffff url(../images/icons/chevron-down-green.svg) right 0 top
    14px no-repeat;
  background-size: 13px;
  color: #00a693;
  border: none;
  outline: none;
`;

class SearchSettings extends React.Component {
  static propTypes = {
    toggleType: PropTypes.func.isRequired,
    addTerm: PropTypes.func.isRequired,
    onDateChange: PropTypes.func.isRequired,
    sort: PropTypes.func.isRequired,
    currentAssignees: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        count: PropTypes.number.isRequired
      })
    ).isRequired,
    sources: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        count: PropTypes.number.isRequired
      })
    ).isRequired,
    query: PropTypes.shape({
      generalSearchQuery: PropTypes.string.isRequired,
      advancedSearchAttributes: PropTypes.shape({
        searchGrants: PropTypes.bool.isRequired,
        searchApplications: PropTypes.bool.isRequired,
        searchNPLs: PropTypes.bool.isRequired,
        title: PropTypes.string.isRequired,
        inventor: PropTypes.string.isRequired,
        abstract: PropTypes.string.isRequired,
        specification: PropTypes.string.isRequired,
        claims: PropTypes.string.isRequired,
        currentAssignee: PropTypes.string.isRequired
      }).isRequired,
      isAdvancedSearchEnabled: PropTypes.bool.isRequired,
      sortingField: PropTypes.string,
      filteringAttributes: PropTypes.shape({
        verificationStatus: PropTypes.arrayOf(PropTypes.string).isRequired,
        currentAssignees: PropTypes.arrayOf(PropTypes.string).isRequired,
        priorityYears: PropTypes.shape({
          from: PropTypes.string,
          to: PropTypes.string
        }).isRequired,
        documentSource: PropTypes.arrayOf(PropTypes.string).isRequired
      }).isRequired
    }).isRequired,
    removeTerm: PropTypes.func.isRequired,
    selectedCurrentAssignees: PropTypes.arrayOf(PropTypes.string).isRequired,
    selectedSources: PropTypes.arrayOf(PropTypes.string).isRequired,
    selectedVerificationTypes: PropTypes.arrayOf(PropTypes.string).isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      filtersActive: false
    };

    this.addCurrentAssignee = this.addCurrentAssignee.bind(this);
    this.addSource = this.addSource.bind(this);
    this.changeFrom = this.changeFrom.bind(this);
    this.changeTo = this.changeTo.bind(this);
    this.sortByValue = this.sortByValue.bind(this);
    this.sortBySource = this.sortBySource.bind(this);
    this.sortByDate = this.sortByDate.bind(this);
    this.toggleFilters = this.toggleFilters.bind(this);
  }

  addCurrentAssignee = name => {
    this.props.addTerm('currentAssignees', name);
  };

  addSource = name => {
    this.props.addTerm('documentSource', name);
  };

  changeFrom = value => {
    this.props.onDateChange('from', value);
  };

  changeTo = value => {
    this.props.onDateChange('to', value);
  };

  sortByValue = event => {
    const value = event.target.value;

    this.props.sort(value === '' ? null : value);
  };

  sortBySource = () => {
    if (this.props.query.sorting === 'source_label') {
      this.props.sort(null);
    } else {
      this.props.sort('source_label');
    }
  };

  sortByDate = () => {
    if (this.props.query.sorting === 'priority_date') {
      this.props.sort(null);
    } else {
      this.props.sort('priority_date');
    }
  };

  toggleFilters = () => {
    const filterVeil = $(this.filterVeil);
    const filterPanel = $(this.filterPanel);

    if (this.state.filtersActive === false) {
      filterVeil.height(document.body.clientHeight).fadeIn(1, () => {
        this.setState({
          filtersActive: true
        });
      });

      filterPanel.addClass('filter-wrapper-show');
    } else {
      filterVeil.fadeOut(1, () => {
        this.setState({
          filtersActive: false
        });
      });

      filterPanel.removeClass('filter-wrapper-show');
    }
  };

  render() {
    const options = OPTIONS.map(item => {
      return {
        ...item,
        selected: this.props.query.filteringAttributes.verificationStatus.includes(
          item.id
        )
      };
    });

    return (
      <FilterBlock className="row">
        <FilterTrigger className="filter-trigger col-5 d-md-none">
          <FilterLink onClick={this.toggleFilters}>
            <span className="icon-filter" />&nbsp;Filters
          </FilterLink>
        </FilterTrigger>

        <FilterVeil
          innerRef={input => {
            this.filterVeil = input;
          }}
          className="col-12 d-md-none"
        >
          <WrapperMobile
            innerRef={input => {
              this.filterPanel = input;
            }}
            className="filter-wrapper-mobile col-11 col-sm-6"
          >
            <FilterBean className="filter-bean mob-select-wr no-bottom-margin">
              <SelectWithCheckboxes
                setCheckboxState={this.props.toggleType}
                descriptionOpened="GPR"
                placeholderClosed="GPR"
                options={options}
              />
            </FilterBean>
            <FilterBean className="filter-bean mob-select-wr">
              <SelectWrapper>
                <Select
                  handleAdd={this.addCurrentAssignee}
                  entityName="current assignee"
                  options={{
                    data: this.props.currentAssignees.map(currentAssignee => ({
                      id: currentAssignee.name,
                      text: currentAssignee.name
                    }))
                  }}
                />
              </SelectWrapper>
            </FilterBean>

            <FilterBean className="filter-bean mob-datepicker-wr">
              <FilterTitle>Priority date</FilterTitle>
              <span className="from">From</span>
              <DatePicker
                name={'from'}
                value={this.props.query.filteringAttributes.priorityYears.from}
                changeHandler={this.changeFrom}
              />
              <span className="to">to</span>
              <DatePicker
                name={'to'}
                value={this.props.query.filteringAttributes.priorityYears.to}
                changeHandler={this.changeTo}
              />
            </FilterBean>
            <FilterBean className="filter-bean mob-select-wr">
              <SelectWrapper>
                <Select
                  handleAdd={this.addSource}
                  entityName="document source"
                  options={{
                    data: this.props.sources.map(source => ({
                      id: source.name,
                      text: source.name
                    }))
                  }}
                />
              </SelectWrapper>
            </FilterBean>
            <SelectedTags
              selectedSources={this.props.selectedSources}
              selectedCurrentAssignees={this.props.selectedCurrentAssignees}
              selectedVerificationTypes={
                this.props.query.filteringAttributes.verificationStatus
              }
              removeTerm={this.props.removeTerm}
              wrapperClass="d-md-none"
            />
          </WrapperMobile>
        </FilterVeil>

        <WrapperDesktop className="filter-wrapper-desktop d-none d-md-block col-md-12">
          <FilterBean className="filter-bean col-sm-2 col-lg-3">
            <FilterTitle className="gpr-title">
              Global Patent Registry
            </FilterTitle>
            <SelectWithCheckboxes
              setCheckboxState={this.props.toggleType}
              descriptionOpened="Types"
              placeholderClosed="Types"
              options={options}
            />
          </FilterBean>

          <FilterBean className="filter-bean col-sm-2 col-lg-2">
            <FilterTitle>Current assignee</FilterTitle>
            <SelectWrapper>
              <Select
                handleAdd={this.addCurrentAssignee}
                entityName="current assignee"
                options={{
                  data: this.props.currentAssignees.map(currentAssignee => ({
                    id: currentAssignee.name,
                    text: currentAssignee.name
                  }))
                }}
              />
            </SelectWrapper>
          </FilterBean>

          <FilterBean className="filter-bean col-sm-4 col-lg-3">
            <FilterTitle>Priority date</FilterTitle>
            <DatePickerWrapper>
              <span className="from">From</span>
              <DatePicker
                name={'from'}
                value={this.props.query.filteringAttributes.priorityYears.from}
                changeHandler={this.changeFrom}
              />
              <span className="to">to</span>
              <DatePicker
                name={'to'}
                value={this.props.query.filteringAttributes.priorityYears.to}
                changeHandler={this.changeTo}
              />
            </DatePickerWrapper>
          </FilterBean>

          <FilterBean className="filter-bean doc-source col-sm-2 col-lg-2">
            <FilterTitle>Document source</FilterTitle>
            <SelectWrapper>
              <Select
                handleAdd={this.addSource}
                entityName="source"
                options={{
                  data: this.props.sources.map(source => ({
                    id: source.name,
                    text: source.name
                  }))
                }}
              />
            </SelectWrapper>
          </FilterBean>
          <FilterBean className="filter-bean col-sm-2 col-lg-2">
            <FilterTitle className="filter-switch-title">Sort By</FilterTitle>
            <SelectWrapper className="no-search">
              <StaticSelect
                handleSelect={this.props.sort}
                options={{
                  minimumResultsForSearch: -1,
                  data: [
                    {
                      id: '',
                      text: 'None'
                    },
                    {
                      id: 'priority_date',
                      text: 'Priority Date'
                    },
                    {
                      id: 'source_label_string',
                      text: 'Source'
                    },
                    {
                      id: 'qscore',
                      text: 'Qscore'
                    },
                    {
                      id: 'pgr_score',
                      text: 'Vscore'
                    }
                  ]
                }}
              />
            </SelectWrapper>
          </FilterBean>
        </WrapperDesktop>

        <FilterBean className="filter-bean filter-switch col-7 col-md-2">
          <SortingSelect
            className="d-md-none"
            value={
              this.props.query.sortingField ? this.props.query.sortingField : ''
            }
            onChange={this.sortByValue}
          >
            <option value="">Sort by ...</option>
            <option value="priority_date">Sort by Priority Date</option>
            <option value="source_label">Sort by Source</option>
          </SortingSelect>
        </FilterBean>
      </FilterBlock>
    );
  }
}

export default SearchSettings;
