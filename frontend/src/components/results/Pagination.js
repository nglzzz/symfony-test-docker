import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const PaginationWrapper = styled.div`
  display: flex;
  flex-direction: column-reverse;
  overflow: hidden;
  background: #fff;

  @media all and (min-width: 768px) {
    flex-direction: row;
    justify-content: space-between;
  }
`;

const PerPage = styled.div`
  float: left;
  margin: 15px 0 15px 15px;
`;

const Label = styled.label`
  font-weight: bold;
  margin-right: 5px;
`;

const Select = styled.select`
  width: 65%;
  padding-left: 15px;
  height: 37px;
  -webkit-appearance: none;
  -moz-appearance: none;
  border: 1px solid #c1c1c1;
  background: #fff url(../images/icons/chevron-down.svg) no-repeat right 12px top 14px;
  background-size: 13px;
  font: normal 13px 'Roboto', sans-serif;
  color: #383838;
  cursor: pointer;
  outline: none;
  
  &:active {
    background: #fff url(../images/icons/chevron-up.svg) no-repeat right 12px top 14px;
    background-size: 13px;
  }

  @media all and (min-width: 768px) {
    width: 114px;
  }
`;

const PaginationContainer = styled.div`
  float: right;
  margin: 15px 0;
  width: 100%;
  justify-content: space-between;
  
  @media all and (min-width: 768px) {
    width: auto;
    justify-content: flex-start;
  }
`;

const PaginationButtonWrapper = styled.div`
  width: 15%;
  display: flex;
  justify-content: center;

  @media all and (min-width: 768px) {
    width: auto;
  }
`;

const Pages = styled.div`
  float: left;
`;

const Page = styled.button`
  border: 0;
  background: none;
  cursor: pointer;
  float: left;
  height: 35px;
  text-align: center;
  line-height: 35px;
  display: block;
  color: #000;
  width: 9vw;
  border-left: 1px solid #e8e8e8;

  &:last-child {
    border-right: 1px solid #e8e8e8;
  }

  &.current {
    background-color: #19a384;
    color: #fff;
    border-color: #19a384;

    &:hover {
      color: #fff;
      background-color: #0e8268;
    }
  }
  
  @media all and (min-width: 768px) {
    width: 35px;
  }
`;

const PaginationButton = styled.button`
  border: 0;
  background: none;
  cursor: pointer;
  float: left;
  height: 35px;
  text-align: center;
  line-height: 35px;
  display: block;
  color: #000;
  padding: 0 10px;

  span::before {
    color: #000;
    font-size: 12px;
    margin: 0 5px;
  }

  &:hover {
    color: #0e8268;

    span::before {
      color: #0e8268;
    }
  }
`;

const PaginationLabel = styled.span`
`;

class Pagination extends React.Component {
  static propTypes = {
    attributes: PropTypes.shape({
      countAll: PropTypes.number.isRequired,
      onPage: PropTypes.number.isRequired,
      page: PropTypes.number.isRequired,
      pages: PropTypes.number.isRequired,
      perPage: PropTypes.number.isRequired
    }).isRequired,
    changeRowsPerPage: PropTypes.func.isRequired,
    setPage: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.setPage = this.setPage.bind(this);
  }

  setPage(event) {
    if (+event.target.value !== this.props.attributes.page) {
      this.props.setPage(+event.currentTarget.value);
    }
  }

  render() {
    const from = this.props.attributes.page - 3 > 0 ? this.props.attributes.page - 3 : 1;
    const to = this.props.attributes.page + 3 < this.props.attributes.pages ?
      this.props.attributes.page + 3 :
      this.props.attributes.pages;

    const pages = [];
    for (let i = from; i <= to; i += 1) {
      pages.push(i);
    }

    return (
      <PaginationWrapper className="container">
        <PerPage>
          <Label htmlFor="perPageSelect">Show per page: </Label>
          <Select
            id="perPageSelect"
            onChange={this.props.changeRowsPerPage}
            defaultValue={20}
            value={this.props.attributes.perPage}>
            <option value="10">10 rows</option>
            <option value="20">20 rows</option>
            <option value="50">50 rows</option>
            <option value="100">100 rows</option>
          </Select>
        </PerPage>
        <PaginationContainer className="pagination">
          <PaginationButtonWrapper>
            { this.props.attributes.page > 1 &&
              <PaginationButton
                onClick={this.setPage}
                value={this.props.attributes.page - 1}>
                <span className="icon-chevron-left" />
                <PaginationLabel className="d-none d-sm-inline">Previous</PaginationLabel>
              </PaginationButton> }
          </PaginationButtonWrapper>
          <Pages>
            { pages.map(page => (
              <Page key={page}
                    onClick={this.setPage}
                    value={page}
                    className={ this.props.attributes.page === page ? 'current' : '' }>
              { page }
              </Page>
            )) }
          </Pages>
          <PaginationButtonWrapper>
            { this.props.attributes.page < this.props.attributes.pages &&
              <PaginationButton onClick={this.setPage}
                                value={this.props.attributes.page + 1}>
                <PaginationLabel className="d-none d-sm-inline">Next</PaginationLabel>
                <span className="icon-chevron-right" />
              </PaginationButton> }
          </PaginationButtonWrapper>
        </PaginationContainer>
      </PaginationWrapper>
    )
  }

}

export default Pagination
