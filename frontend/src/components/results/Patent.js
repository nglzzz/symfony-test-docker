import React from 'react';
import PropTypes from 'prop-types';
import PatentScore from 'components/patent/PatentScore';
import PatentType from 'components/patent/PatentType';
import PatentNumberLink from 'components/patent/PatentNumberLink';
import PatentSourceLink from 'components/patent/PatentSourceLink';
import PatentVerification from 'components/patent/PatentVerification';
import PatentList from 'components/patent/PatentList';
import TableRow from 'components/results/TableRow';
import styled from 'styled-components';

const PatentWrapper = TableRow.extend`
  position: relative;
  margin-bottom: 9px;

  &:last-child {
    margin-bottom: 20px;
  }  
      
  @media all and (min-width: 768px) {
      min-height: 88px;
      display: grid;
      grid-template-columns: 5% 15% 22% 16% 12% 30%;
      grid-template-rows: auto auto;
  }
  
  @media all and (min-width: 1200px) {
    display: table-row;
  }
`;

const Type = styled.td`
  && {
    width: 41px;
    float: left;
    min-height: 44px;
    padding: 0.5rem 0.5rem 0.5rem 16px;

    @media all and (min-width: 768px) {
      width: 100%;
      float: none;
    }

    @media all and (min-width: 1200px) {
      width: 5%;
    }
  }
`;

const Number = styled.td`
  && {
    width: calc(100% - 41px);
    float: left;
    min-height: 44px;
    text-decoration: none;
    padding-top: 0.6rem;
    padding-right: 2.5rem;

    &:hover {
      text-decoration: none;
    }        
    
    .label-button {
      display: block;
      width: 68px;
      margin: 4px 4px 4px 0;
      padding: 1px 2px 0;
      text-transform: uppercase;
      text-align: center;
      color: #ffffff;
      line-height: 14px;
      font-size: 9px;
      font-weight: bold;
      border-radius: 3px;
      outline: none;
    }
    
    a.label-button.sale {
      background-color: #4f85e8;
      &:hover {
        color: blue;
      } 
    }
    
    a.label-button.license {
      background-color: #32cc6f;
      border: none;
      cursor: pointer;
      &:hover {
        color: green;
      } 
    }

    @media all and (min-width: 768px) {
      width: 100%;
      padding-right: 5px;
      float: none;
      a {
        display: block;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis; 
      }        
    }

    @media all and (min-width: 1200px) {
      width: 15%;
    }
  }
`;

const ApplicationNumber = styled.td`
  && {
    width: 100%;
    float: none;
    clear: both;
    min-height: 44px;
    padding-top: 0.6rem;

    @media all and (min-width: 768px) {
      width: 100%;
      grid-column-start: 3;
      grid-column-end: 5;
      float: none;
      a {
        display: block;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis; 
      }        
    }
    @media all and (min-width: 1200px) {
      width: 11%;
    }
  }
`;
const ApplNumberTitle = styled.span`
  && {
    font-weight: bold;
    padding-right: 8px;
  }
`;

const Verification = styled.td`
  && {
    text-align: center;
    @media all and (min-width: 768px) {
      grid-column-start: 5;
      grid-column-end: 7; 
      
      text-align: left;
      padding-left: 16px;
      border-top: none;
    }
    @media all and (min-width: 1200px) {
      width: 6%;
      position: static;
      text-align: center;
    }
  }
`;

const Score = styled.td`
  && {
    width: 50%;
    height: 44px;
    position: relative;
    padding-top: 0.6rem;

    @media all and (min-width: 768px) {
      width: auto;
      float: none;
    }
    
    @media all and (min-width: 1200px) {
      width: 6%;
    }
  }
`;

const QScore = Score.extend`
  float: left;
  @media all and (min-width: 768px) {
      grid-column-start: 1;
      grid-column-end: 3;
    }
`;

const VScore = Score.extend`
  && {
    float: right;
    @media all and (min-width: 768px) {
       float: none;
    }
    @media all and (min-width: 1200px) {
      position: static;
      left: 0;
    }
  }
`;

const Title = styled.td`
  && {
    display: none;

    @media all and (min-width: 768px) {
      display: none;
    }

    @media all and (min-width: 1200px) {
      display: table-cell;
      width: 25%;
    }
  }
`;

const Date = styled.td`
  && {
    clear: both;
    flex-direction: row;
    white-space: nowrap;

    @media all and (min-width: 768px) {
      width: 100%;
    }
    @media all and (min-width: 1200px) {
      width: 9%;
    }
  }
`;

const Source = styled.td`
  && {
    @media all and (min-width: 768px) {
      width: 100%;
    }
    @media all and (min-width: 1200px) {
      width: 5%;
    }
  }
`;

const Assignee = styled.td`
  && {
    @media all and (min-width: 768px) {
      width :100%;
    }

    @media all and (min-width: 1200px) {
      width: 12%;
    }
  }
`;

const ScoreTitle = styled.span`
  && {
    margin-right: 11px;
    color: #2e2e2e;
    font-weight: bold;
    line-height: 1.7;

    @media all and (min-width: 768px) {
      margin-right: 0.2rem;
    }
  }
`;

const DateTitle = styled.span`
  && {
    color: #2e2e2e;
    font-weight: bold;
    margin-right: 10px;
    
    @media all and (min-width: 768px) {
      display: block;
      padding-top: 6px;
      padding-bottom: 5px;
    }
    
    @media all and (min-width: 1200px) {
      display: none;
    }
  }
`;

const SourceTitle = styled.span`
  && {
    color: #2e2e2e;
    font-weight: bold;
    margin-right: 10px;

    @media all and (min-width: 768px) {
      display: block;
      padding-top: 6px;
      padding-bottom: 5px;
    }

    @media all and (min-width: 1200px) {
      display: none;
    }
  }
`;

const AssigneeTitle = styled.span`
  && {
    color: #2e2e2e;
    font-weight: bold;
    margin-right: 10px;

    @media all and (min-width: 768px) {
      display: block;
      padding-top: 6px;
      padding-bottom: 5px;
    }

    @media all and (min-width: 1200px) {
      display: none;
    }
  }
`;

function Patent({ data, verificationClickHandler }) {
  return (
    <PatentWrapper>
      <Type>
        <PatentType value={data.type} />
      </Type>
      <Number>
        <PatentNumberLink href={data.url} target="_blank">
          {data.number}
        </PatentNumberLink>
        { data.forSale && <a href={data.forSale.url} target="_blank" className="label-button sale" rel="noopener noreferrer">For sale</a>}
        { data.forLicense && <a href={data.forLicense.url} target="_blank" className="label-button license" rel="noopener noreferrer">For license</a>}
      </Number>
      <ApplicationNumber>
          <ApplNumberTitle className="d-xl-none">Appl. Number:</ApplNumberTitle>
          <PatentNumberLink>
            {data.applicationNumber}
          </PatentNumberLink>
      </ApplicationNumber>
      <Verification>
        <PatentVerification
          type={data.verificationStatus}
          clickHandler={verificationClickHandler}
        />
      </Verification>
      <QScore>
        <ScoreTitle className="d-xl-none">Q Score:</ScoreTitle>
        <PatentScore value={data.qScore} />
      </QScore>
      <VScore>
        <ScoreTitle className="d-xl-none">V Score:</ScoreTitle>
        <PatentScore value={data.vScore} />
      </VScore>
      <Title>{data.title}</Title>
      <Date>
        <DateTitle>Priority date:</DateTitle>
        {data.priorityDate}
      </Date>
      <Source>
        <SourceTitle>Source:</SourceTitle>
        <PatentSourceLink description={data.sourceDesc}>
          {data.source}
        </PatentSourceLink>
      </Source>
      <Assignee>
        <AssigneeTitle>Current assignee:</AssigneeTitle>
        <PatentList values={data.currentAssignee} />
      </Assignee>
    </PatentWrapper>
  );
}

Patent.propTypes = {
  data: PropTypes.shape({
    type: PropTypes.string.isRequired,
    applicationNumber: PropTypes.string.isRequired,
    forSale: PropTypes.shape({
      name: PropTypes.string,
      url: PropTypes.string,
    }),
    forLicense: PropTypes.shape({
      name: PropTypes.string,
      url: PropTypes.string,
    }),
    number: PropTypes.string.isRequired,
    qScore: PropTypes.number,
    vScore: PropTypes.number,
    title: PropTypes.string,
    priorityDate: PropTypes.string.isRequired,
    source: PropTypes.string.isRequired,
    sourceDesc: PropTypes.string.isRequired,
    currentAssignee: PropTypes.arrayOf(PropTypes.string).isRequired,
    verificationStatus: PropTypes.string.isRequired,
  }).isRequired,
  verificationClickHandler: PropTypes.func.isRequired
};

export default Patent;
