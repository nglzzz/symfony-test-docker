import styled from 'styled-components';

export default styled.tr`
  width: 100%;
  display: block;
  background-color: #fff;

  @media all and (min-width: 768px) {
    width: 100%;
    display: flex;
  }

  @media all and (min-width: 1200px) {
    display: table-row;
  }

  td {
    display: block;
    width: 100%;

    @media all and (min-width: 768px)  {
      width: auto;
      display: table-cell;
      padding: 0.5em;
      min-width: 0;
    }

    @media all and (min-width: 1200px) {
      border-bottom: 1px solid #e9ecef;
    }
  }
`;
