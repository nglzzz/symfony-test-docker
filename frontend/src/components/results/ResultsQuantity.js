import React from 'react'
import PropTypes from 'prop-types'

function ResultsQuantity({paginationAttributes}) {
  const fromPatent = (paginationAttributes.page - 1) * paginationAttributes.perPage + 1;
  const toPatent = (paginationAttributes.page - 1) * paginationAttributes.perPage + paginationAttributes.onPage;
  const countAll = paginationAttributes.countAll;

  const outputResults = fromPatent !== 0 && toPatent !== 0 && countAll !== 0;

  if (!outputResults) {
    return <div hidden />
  }

  return (
    <div className="results-quantity">{fromPatent} - {toPatent} of {countAll} results</div>
  )
}

ResultsQuantity.propTypes = {
  paginationAttributes: PropTypes.shape({
    countAll: PropTypes.number.isRequired,
    onPage: PropTypes.number.isRequired,
    page: PropTypes.number.isRequired,
    pages: PropTypes.number.isRequired,
    perPage: PropTypes.number.isRequired
  }).isRequired,
};


export default ResultsQuantity
