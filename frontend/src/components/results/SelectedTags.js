import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Param from 'components/common/Param';
import { GENERIC_STATUS, getVerificationStatusName } from 'constants/VerificationStatuses';

const Params = styled.div`
  padding: 0 15px;
  margin: 5px auto 0;

  @media all and (min-width: 1280px) {
    width: 1200px;
    padding: 0;
    margin-left: auto;
    margin-right: auto;
  }
`;

const ParamGroup = styled.div`
  display: block;

  @media all and (min-width: 768px) {
    display: inline;
  }
`;

function SelectedTags({ selectedCurrentAssignees, selectedSources, selectedVerificationTypes, removeTerm, wrapperClass }) {
  if (selectedCurrentAssignees.length === 0 && selectedSources.length === 0 && selectedVerificationTypes.length === 0) {
    return <div hidden />;
  }

  const removeCurrentAssignee = (name) => {
    removeTerm('currentAssignees', name);
  };

  const removeSource = (name) => {
    removeTerm('documentSource', name);
  };

  const removeVerificationStatus = (name) => {
    removeTerm('verificationStatus', name);
  };

  return (
    <Params className={wrapperClass}>
      <ParamGroup>
        {
          selectedCurrentAssignees.map((currentAssignee) => (
            <Param type="currentAssignee"
                   removeHandler={removeCurrentAssignee}
                   name={currentAssignee}
                   key={currentAssignee} />
          ))
        }
      </ParamGroup>
      <ParamGroup>
        {
          selectedSources.map((source) => (
            <Param type="source" removeHandler={removeSource} name={source} key={source} />
          ))
        }
      </ParamGroup>
      <ParamGroup>
        {
          selectedVerificationTypes
              .filter(status => status !== GENERIC_STATUS)
              .map((verificationStatus) => (
            <Param type="verificationStatus"
                   removeHandler={removeVerificationStatus}
                   name={verificationStatus}
                   title={getVerificationStatusName(verificationStatus)}
                   key={verificationStatus} />
          ))
        }
      </ParamGroup>
    </Params>
  );
}


SelectedTags.propTypes = {
  removeTerm: PropTypes.func.isRequired,
  selectedCurrentAssignees: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedSources: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedVerificationTypes: PropTypes.arrayOf(PropTypes.string).isRequired,
  wrapperClass: PropTypes.string
};

SelectedTags.defaultProps = {
  wrapperClass: ''
};

export default SelectedTags
