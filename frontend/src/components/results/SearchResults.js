import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Loader from 'react-loader';

import Patent from 'components/results/Patent';
import Pagination from 'components/results/Pagination';
import TableRow from 'components/results/TableRow';
import PatentVerificationPopup from 'components/patent/PatentVerificationPopup';

const flatMap = (arr, lambda) =>
  Array.prototype.concat.apply([], arr.map(lambda));

const SearchResultsContainer = styled.div`
  margin: 20px auto 0;

  @media all and (min-width: 1200px) {
    width: 1200px;
  }
`;

const ResultsTable = styled.table`
  width: 96%;
  margin-left: auto;
  margin-right: auto;
  font-size: 12px;
  border-top: 0;
  display: block;

  @media all and (min-width: 768px) {
    display: flex;
    flex-flow: column nowrap;
    overflow-x: visible;
  }

  @media all and (min-width: 1200px) {
    display: table;
    width: 100%;
  }
`;

const THead = styled.thead`
  display: none;
  background-color: #fff;

  @media all and (min-width: 1200px) {
    display: table-header-group;

    th {
      padding-left: 0.4rem;
    }
    th.info-label {
      position: relative;
      padding-right: 1rem;
      text-align: center;
    }
    th.info-label::after {
      content: 'i';
      text-align: center;
      font: bold 11px 'Roboto', sans-serif;
      position: absolute;
      top: 14px;
      right: 2px;
      width: 14px;
      height: 14px;
      padding-top: 1px;
      background-color: #d8e5e2;
      color: #828e8b;
      border-radius: 50%;
      cursor: pointer;
    }
    th.gpr-logo {
      padding-left: 27px;
      padding-right: 1.2rem;
    }
    th.gpr-logo::before {
      content: '';
      position: absolute;
      top: 13px;
      left: 8px;
      width: 23px;
      height: 15px;
      background: url(../images/Global_Patent_Registry.svg) no-repeat;
      background-size: 15px;
    }
  }
`;

const TBody = styled.tbody`
  width: 100%;
  display: block;

  @media all and (min-width: 768px) {
    flex-direction: column;
  }

  @media all and (min-width: 1200px) {
    display: table-row-group;
  }
`;

const LabelWrapper = styled.div`
  padding: 20px;
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  font-weight: 700;
`;

const LoaderWrapper = styled.div`
  height: 400px;
`;

const TitleMobile = TableRow.extend`
  width: 100%;
  color: #00a693;
  font-size: 13px;
  text-transform: uppercase;
  margin-bottom: 3px;
  position: relative;

  @media all and (min-width: 768px) {
    color: #00a693;
    text-transform: uppercase;
  }

  @media all and (min-width: 1200px) {
    display: none;
  }

  td {
    padding: 0;
    width: 100%;

    a {
      min-height: 2rem;
      display: block;
      width: 100%;
      padding: 0.75rem 1.5rem 0.75rem 0.75rem;

      &::after {
        content: '';
        width: 7px;
        height: 12px;
        background-image: url(../images/icons/chevron-right-green.svg);
        position: absolute;
        top: 50%;
        margin-top: -6px;
        right: 8px;
        cursor: pointer;
      }

      &:hover {
        color: #0e8268;

        &::after {
          background-image: url(../images/icons/chevron-right-dark-green.svg);
        }
      }
    }
  }
`;

const Tooltip = styled.span`
  visibility: hidden;

  position: absolute;
  z-index: 1000;
  top: -103px;
  left: 181px;
  transform: translate(-50%, 0);

  border: 1px solid #98c4bc;
  box-shadow: 2px 4px 10px 0 rgba(152, 196, 188, 0.75);
  width: 315px;

  background-color: #ffffff;
  color: #000000;
  font-weight: normal;
  text-align: left;
  padding: 15px;

  &::before {
    content: '';
    display: block;
    position: absolute;
    bottom: -13px;
    left: 10%;
    margin-left: -5px;
    width: 25px;
    height: 13px;
    background: url(../images/tt.png);
  }

  &.q-score {
    top: -160px;
  }
  &.v-score {
    top: -175px;
  }

  *:hover > & {
    visibility: visible;
  }

  .tooltip-link {
    display: inline-block;
    position: relative;
    margin-top: 5px;
    text-align: left;
    padding-right: 13px;
    &::after {
      content: '';
      position: absolute;
      top: 5px;
      right: 0;
      width: 5px;
      height: 8px;
      background-image: url(../images/icons/more-link-arrow.png);
    }
    &:hover {
      text-decoration: underline;
    }
  }

  &.low-tooltip {
    width: 160px;
    left: 119px;
    top: -67px;
  }
`;

const options = {
  lines: 13,
  length: 0,
  width: 20,
  radius: 59,
  scale: 0.45,
  corners: 1,
  color: '#0e8268',
  fadeColor: 'transparent',
  opacity: 0.2,
  rotate: 0,
  direction: 1,
  speed: 1.7,
  trail: 100,
  fps: 20,
  zIndex: 2e9,
  className: 'spinner',
  top: '50%',
  left: '50%',
  position: 'absolute'
};

const POPUP_VERIFICATION = 'POPUP_VERIFICATION';

class SearchResults extends React.Component {
  static propTypes = {
    changeRowsPerPage: PropTypes.func.isRequired,
    setPage: PropTypes.func.isRequired,
    selectPatent: PropTypes.func.isRequired,
    selectedPatent: PropTypes.shape({
      number: PropTypes.string
    }),
    patents: PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.string,
        number: PropTypes.string,
        qScore: PropTypes.number,
        vScore: PropTypes.number,
        title: PropTypes.string,
        priorityDate: PropTypes.string,
        currentAssignee: PropTypes.arrayOf(PropTypes.string),
        verificationStatus: PropTypes.string
      })
    ),

    paginationAttributes: PropTypes.shape({
      countAll: PropTypes.number.isRequired,
      onPage: PropTypes.number.isRequired,
      page: PropTypes.number.isRequired,
      pages: PropTypes.number.isRequired,
      perPage: PropTypes.number.isRequired
    }).isRequired,

    isLoading: PropTypes.bool.isRequired
  };

  static defaultProps = {
    patents: null,
    selectedPatent: null
  };

  render() {
    let {
      patents,
      paginationAttributes,
      changeRowsPerPage,
      setPage,
      isLoading
    } = this.props;

    if (isLoading) {
      return (
        <SearchResultsContainer className="container">
          <div className="row">
            <LoaderWrapper className="col">
              <Loader loaded={false} options={options} className="spinner" />
            </LoaderWrapper>
          </div>
        </SearchResultsContainer>
      );
    }

    if (patents === null || patents.length === 0) {
      return (
        <SearchResultsContainer className="container">
          <div className="row">
            <div className="col">
              <LabelWrapper className="d-flex justify-content-center">
                No results
              </LabelWrapper>
            </div>
          </div>
        </SearchResultsContainer>
      );
    }

    const displayPopup = (type, patent) => {
      this.setState({
        popupType: type,
      });

      this.props.selectPatent(patent);
    };

    return (
      <SearchResultsContainer>
        <ResultsTable className="table table-responsive">
          <THead>
            <tr>
              <th scope="col">Type</th>
              <th scope="col">Number</th>
              <th scope="col">Appl. Number</th>
              <th scope="col" className="info-label gpr-logo">
                GPR
                <Tooltip className="low-tooltip">
                  Global Patent Registry
                  <a
                    href="https://registry.ipwe.com/"
                    target={'_blank'}
                    className="tooltip-link"
                  >
                    More information
                  </a>
                </Tooltip>
              </th>
              <th scope="col" className="info-label">
                Q&nbsp;Score
                <Tooltip className="q-score">
                  The QScore is a single score that is designed to measure the
                  overall relative quality of a patent in a large collection by
                  taking into account various attributes, including the citation
                  network of the collection and a measure of value that the
                  patent owners assign to it.
                  <br />
                  <a
                    href="https://ipwe.com/zuse-analytics"
                    target={'_blank'}
                    className="tooltip-link"
                  >
                    More information
                  </a>
                </Tooltip>
              </th>
              <th scope="col" className="info-label">
                V&nbsp;Score
                <Tooltip className="v-score">
                  The VScore represents how close prior art matches the first
                  claim of the patent. A higher score means there is less
                  matching prior art in the collection related to the patent.
                  The score considers the first claim’s limitations, the text of
                  the art, the link structure of the citation network, and the
                  patent classification.
                  <br />
                  <a
                    href="https://ipwe.com/zuse-analytics"
                    target={'_blank'}
                    className="tooltip-link"
                  >
                    More information
                  </a>
                </Tooltip>
              </th>
              <th scope="col">Title</th>
              <th scope="col">Priority&nbsp;Date</th>
              <th scope="col">Source</th>
              <th scope="col">Current Assignee</th>
            </tr>
          </THead>
          <TBody>
            {flatMap(patents, patent => [
              <TitleMobile key={`${patent.id}-title`}>
                <td>
                  <a href={patent.url} target="_blank">
                    {patent.title}
                  </a>
                </td>
              </TitleMobile>,
              <Patent
                key={patent.id}
                data={patent}
                verificationClickHandler={() => displayPopup(POPUP_VERIFICATION, patent)}
              />
            ])}
          </TBody>
        </ResultsTable>
        <Pagination
          setPage={setPage}
          attributes={paginationAttributes}
          changeRowsPerPage={changeRowsPerPage}
        />
        {this.props.selectedPatent !== null && this.state.popupType === POPUP_VERIFICATION && (
          <PatentVerificationPopup
            closeHandler={() => this.props.selectPatent(null)}
          />
        )}
      </SearchResultsContainer>
    );
  }
}

export default SearchResults;
