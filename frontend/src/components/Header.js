import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import config from 'config';

const HeaderWrapper = styled.div`
  padding: 3px 0;
  height: 48px;
  background-color: #080607;

  &.show-menu {
    z-index: 200;
    display: table;
    padding: 0;
    width: 100%;
    height: 100vh;
    background: #00a693;
    vertical-align: middle;
    position: fixed;
  }

  @media all and (min-width: 768px) {
    height: 68px;
  }
`;

const HeaderContainer = styled.div`
  .logo-link {
    display: block;
    float: left;

    .show-menu & {
      display: none;
    }
  }
`;

const HeaderLogo = styled.img`
  height: 42px;

  @media all and (min-width: 768px) {
    height: 62px;
  }
`;

const MenuIcon = styled.span`
  float: right;
  margin-top: 12px;
  font-size: 16px;
  cursor: pointer;

  .show-menu & {
    display: none;
  }

  @media all and (min-width: 768px) {
    display: none;
  }
`;

const MenuIconCross = styled.span`
  display: none;
  float: right;
  margin-top: 16px;
  font-size: 16px;
  cursor: pointer;

  &::before {
    color: #fff;
  }

  .show-menu & {
    display: block;
  }
`;

const Nav = styled.div`
  display: none;
  float: right;
  margin-top: 17px;
  color: #fff;

  .show-menu & {
    display: table-cell;
    width: 100%;
    vertical-align: middle;
  }

  @media all and (min-width: 768px) {
    display: block;
    float: right;
    margin-top: 22px;
    color: #fff;
  }
`;

const NavLink = styled.a`
  display: block;
  float: left;
  margin-left: 30px;
  color: #8b9694;
  text-transform: uppercase;

  &:hover {
    color: #fff;
    text-decoration: none;
  }

  .show-menu & {
    display: block;
    float: none;
    margin: 0;
    width: 100%;
    color: #fff;
    text-align: center;
    font-weight: 700;
    font-size: 20px;
    font-family: 'Roboto Slab', sans-serif;

    &::after {
      display: block;
      margin: 25px auto;
      width: 46px;
      height: 1px;
      background: #77cbc1;
      content: '';
    }
  }

  @media all and (min-width: 768px) {
    display: block;
    float: left;
    margin-left: 30px;
    color: #8b9694;
    text-transform: uppercase;

    &:hover {
      color: #fff;
      text-decoration: none;
    }
  }
`;
const HelpLink = styled.a`
  background: #fff;
  border-radius: 50%;
  display: inline-block;
  width: 20px;
  height: 20px;
  text-align: center;
  color: #000;
  font-weight: bold;
  margin-left: 1em;
  float: left;
`;

const SocialLinks = styled.div`
  display: none;
  margin: auto;
  width: 120px;

  .show-menu & {
    display: block;
  }
`;

const SocialLink = styled.a`
  display: block;
  float: left;
  width: 40px;
  text-align: center;

  span::before {
    color: #fff;
    font-size: 18px;
  }
`;

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      active: false
    };

    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
  }

  open() {
    this.setState({
      active: true
    });
  }

  close() {
    this.setState({
      active: false
    });
  }

  render() {
    return (
      <HeaderWrapper className={this.state.active && 'show-menu'}>
        <HeaderContainer className="container">
          <Link className="logo-link" to="/">
            <HeaderLogo src="images/logo.svg" alt="ZUSE ANALYTICS" />
          </Link>
          <MenuIcon onClick={this.open} className="icon-menu" />
          <MenuIconCross onClick={this.close} className="icon-cross" />
          <Nav class="nav">
            <NavLink href={config.ipweLink}>IPWE</NavLink>
            <HelpLink target="_blank" href={config.wikiHelpLink}>
              ?
            </HelpLink>
          </Nav>
          <SocialLinks>
            <SocialLink href="https://twitter.com/ipwe_">
              <span className="icon-twitter" />
            </SocialLink>
            <SocialLink href="https://www.linkedin.com/company/ipwe-com/">
              <span className="icon-linkedin" />
            </SocialLink>
          </SocialLinks>
        </HeaderContainer>
      </HeaderWrapper>
    );
  }
}

export default Header;
