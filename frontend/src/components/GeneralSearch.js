import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import SearchTips from 'components/common/SearchTips';
import ResultsQuantity from 'components/results/ResultsQuantity';
import Checkbox from "components/common/Checkbox";
import {LICENSE_TYPE, ONLINE_SALE_TYPE} from "constants/ProgramTypes";

const GeneralSearchForm = styled.form`
  margin: 0 15px;
  position: relative;
  padding: 0 0 15px;
  overflow: hidden;

  .main & {
    background: transparent;
    margin-top: 100px;

    @media all and (min-width: 768px) {
      background: transparent;
      margin-top: 0;
      margin-left: 0;
      margin-right: 0;
    }

    @media (min-width: 1200px) {
      width: 100%;
      margin-left: auto;
      margin-right: auto;
    }
  }
  
  .search-input-container {
      width: 100%;
      @media all and (min-width: 768px) {
        width: calc(100% - 223px);
      }    
      @media (min-width: 1200px) {
        width: 980px;
      }
  }
  
  .types-wrap {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    @media all and (min-width: 648px) {
      flex-direction: row;
    }
  }

  @media all and (min-width: 768px) {
    margin: 0 15px;
    position: relative;
    overflow: hidden;
  }

  @media (min-width: 1200px) {
    width: 1200px;
    margin: auto;
  }

  .results-quantity {
    padding-top: 15px;
    font-size: 12px;
    clear: both;

    &::before,
    &::after {
      content: '';
      clear: both;
      display: block;
    }
  }
`;

const SearchButton = styled.button`
  display: block;
  position: absolute;
  top: 0;
  right: 0;
  height: 48px;
  width: 48px;
  background: #00a693 url(../images/icons/search.svg) 50% 50% no-repeat;
  background-size: 16px 16px;
  color: #ffffff;
  border: 0;
  font-family: 'Roboto Slab', sans-serif;
  font-size: 16px;
  text-transform: uppercase;
  font-weight: bold;

  &:hover {
    background-color: #0e8268;
    cursor: pointer;
  }

  @media all and (min-width: 768px) {
    height: 58px;
    width: 206px;
    top: 0;
    background: #00a693 url(../images/icons/search.svg) 20px 50% no-repeat;
    background-size: 16px 16px;
  }

  span {
    display: none;
    padding-left: 32px;

    @media all and (min-width: 768px) {
      display: inline;
    }
  }
`;

const SearchInput = styled.input`
  display: block;
  width: 100%;
  padding: 0 58px 0 15px;
  font-size: 18px;
  font-weight: 100;
  height: 48px;
  border: 1px solid #dad8ca;
  background-color: rgba(256, 256, 256, 0.7);

  .main & {
    @media (min-width: 1200px) {
      width: 745px;
    }
  }

  @media all and (min-width: 768px) {
    padding: 0 15px;
    font-size: 18px;
    height: 59px;
    width: 100%;
  }

  @media (min-width: 1200px) {
    width: 980px;
  }
`;

const TypesText = styled.div`
  margin-right: 10px;
  font-size: 13px;
  font-family: 'Roboto', sans-serif;
  font-weight: bold;
  line-height: 16px;
  color: #212529;
  @media all and (min-width: 768px) {
    margin-right: 20px;
  }
`;

const Checkboxes = styled.div`
  display: flex;
  justify-content: flex-start;
  margin: 13px 15px 15px 0;
  
  div {
    font-size: 13px;
    font-family: 'Roboto', sans-serif;
    line-height: 18px;
    color: #212529;
  }
  
  .checkbox {
    @media all and (max-width: 468px) {
      padding-right: 10px;
    }
  }
  
  @media all and (min-width: 768px) {
    margin: 13px 10px 10px 0;
  }
`;

class GeneralSearch extends React.Component {
  static propTypes = {
    handleQueryChange: PropTypes.func.isRequired,
    onSearch: PropTypes.func.isRequired,
    toggleProgramType: PropTypes.func.isRequired,
    selectedProgramTypes: PropTypes.arrayOf(PropTypes.string).isRequired,

    generalSearchQuery: PropTypes.string.isRequired,
    paginationAttributes: PropTypes.shape({
      countAll: PropTypes.number.isRequired,
      onPage: PropTypes.number.isRequired,
      page: PropTypes.number.isRequired,
      pages: PropTypes.number.isRequired,
      perPage: PropTypes.number.isRequired
    }),
    isMain: PropTypes.bool.isRequired
  };

  static defaultProps = {
    paginationAttributes: null
  };

  constructor(props) {
    super(props);

    this.search = this.search.bind(this);
  }

  componentDidMount() {
    this.searchInput.focus();
  }

  search = () => {
    this.props.onSearch(null, {
      programTypes: this.props.selectedProgramTypes,
    });
  };

  render() {
    let {
      handleQueryChange,
      generalSearchQuery,
      paginationAttributes,
      toggleProgramType,
      selectedProgramTypes,
      isMain
    } = this.props;

    const preventFormSubmit = event => {
      event.preventDefault();

      if (generalSearchQuery.trim() !== '') {
        this.search();
      }
    };

    const changeQuery = event => {
      handleQueryChange(event.target.value);
    };

    return (
      <GeneralSearchForm onSubmit={preventFormSubmit}>
        <div className="search-container">
          <div className="search-input-container">
            <SearchInput
              type="text"
              value={generalSearchQuery}
              innerRef={input => {
                this.searchInput = input;
              }}
              placeholder="Enter your search request"
              onChange={changeQuery}
            />
            <div className="types-wrap">
              <Checkboxes>
                <TypesText>Show types:</TypesText>
                <Checkbox
                  clickHandler={() => toggleProgramType(ONLINE_SALE_TYPE)}
                  name="showForSale"
                  state={selectedProgramTypes.includes(ONLINE_SALE_TYPE)}
                >
                  For sale
                </Checkbox>
                <Checkbox
                  clickHandler={() => toggleProgramType(LICENSE_TYPE)}
                  name="showForLicense"
                  state={selectedProgramTypes.includes(LICENSE_TYPE)}
                >
                  For license
                </Checkbox>
              </Checkboxes>
              {!isMain &&
              paginationAttributes && (
                <ResultsQuantity paginationAttributes={paginationAttributes} />
              )}
            </div>
          </div>
          <SearchButton>
            <span>Search Patent</span>
          </SearchButton>
        </div>
        <SearchTips />
      </GeneralSearchForm>
    );
  }
}

export default GeneralSearch;
