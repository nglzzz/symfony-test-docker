import React from 'react';
import styled from 'styled-components';

const CloseButtonWrapper = styled.button`
  padding: 11px 29px 11px 30px;
  font: bold 16px 'Roboto Slab', serif;
  color: #fff;
  background-color: #00a693;
  border: none;
  outline: none;
  cursor: pointer;

  &:focus {
    outline: 5px auto #00a693;
  }

  .icon-cross {
    padding-right: 6px;

    &::before {
      position: relative;
      top: -2px;
      color: #fff;
      font-size: 12px;
    }
  }
`;

function CloseButton(props) {
  return (
    <CloseButtonWrapper {...props}>
      <span className="icon-cross" />
      Close
    </CloseButtonWrapper>
  );
}

export default CloseButton;
