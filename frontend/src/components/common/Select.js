import React from 'react'
import PropTypes from 'prop-types'
import $ from 'jquery'
import 'jquery-ui-dist/jquery-ui'
import 'select2'

class Select extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const self = this;

    $(this.select)
      .select2({
        matcher: this.matcher,
        sorter: this.sorter
      })
      .on('change', (event) => {
        const target = event.target;
        const searchValue = target.value;

        target.value = 'none';

        setTimeout(() => {
          self.handleChange(searchValue);
        }, 0);
      })
      .on('select2:open', () => {
        $('.results-veil').show();
        $('body > .select2-container')
          .prepend(`
            <div class="select-header">
              Select ${this.props.entityName}
              <div class="close-mob-select">&nbsp;</div>
            </div>`
          );
        $('.close-mob-select').on('click', () => {
          $(this.select).select2('close');
        })
      })
      .on('select2:close', () => {
        $('.results-veil').hide();
      })
      .on('select2:closing', () => {
        $('.select-header').remove();
      });
  }

  componentWillReceiveProps(props) {
    const select = $(this.select);

    // Remove all options from latest render
    select.children(':not([value="none"])').remove();
    select.select2({
      ...props.options,
      matcher: this.matcher,
      sorter: this.sorter
    });
  }

  matcher = (params, data) => {
    if (typeof data.text === 'undefined' || data.id === 'none') {
      return null;
    }

    if ($.trim(params.term) === '') {
      return data;
    }

    if (data.text.toLowerCase().startsWith(params.term.toLowerCase())) {
      return {
        ...data,
        score: 2
      };
    }

    if (data.text.toLowerCase().includes(params.term.toLowerCase())) {
      return {
        ...data,
        score: 1
      };
    }

    return null;
  };

  sorter = (data) => data.sort((a, b) => {
    const scoreA = a.score ? a.score : 0;
    const scoreB = b.score ? b.score : 0;

    return scoreB - scoreA;
  });

  handleChange = (newValue) => {
    this.props.handleAdd(newValue);
  };

  render() {
    return (
      <select className="uppercase-select"
        ref={(select) => {this.select = select}}
        {...this.props.options}
      >
        <option value="none">Select {this.props.entityName}</option>
      </select>
    )
  }
}

Select.propTypes = {
  handleAdd: PropTypes.func.isRequired,
  options: PropTypes.shape(),
  entityName: PropTypes.string.isRequired
};

Select.defaultProps = {
  options: {}
};

export default Select
