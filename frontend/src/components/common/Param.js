import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const ParamWrapper = styled.div`
  border-radius: 2px;
  display: inline-block;
  margin-right: 2px;
  margin-top: 15px;
  padding: 10px 10px 10px 15px;
  text-transform: uppercase;

  span::before {
    font-size: 10px;
    margin-left: 5px;
  }

  button {
    background: transparent;
    border: 0;
    cursor: pointer;
    margin: 0;
    padding: 0;
    outline: none;
  }
  
  &:last-child {
    margin-right: 8px;
  }

  ${
    (props) => {
      if (props.type === 'currentAssignee') {
        return `
          background: #b6d3ce;
          color: #3a5550;
  
          span::before {
            color: #76948f;
          }
        `;
      } else if (props.type === 'source') {
        return `
          background: #bdc9d5;
          color: #405468;
  
          span::before {
            color: #6e8194;
          }
        `;
      } else if (props.type === 'verificationStatus') {
        return `
          background: #b4d7b2;
          color: #406855;
  
          span::before {
            color: #406854;
          }
        `;
      }
      
      return '';
    }
  }
`;

function Param(props) {
  const {name, title} = props;

  const removeHandler = (event) => {
    event.preventDefault();

    props.removeHandler(name);
  };

  return (
    <ParamWrapper type={props.type}>
      { title || name }
      <button onClick={removeHandler}><span className="icon-cross" /></button>
    </ParamWrapper>
  )
}

Param.propTypes = {
  removeHandler: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  title: PropTypes.string,

  type: PropTypes.string.isRequired
};

Param.defaultProps = {
  title: null
};

export default Param
