import React from 'react';
import PropTypes from 'prop-types';

import Header from 'components/Header';
import Footer from 'components/Footer';
import PageContent from 'components/PageContent';
import { withRouter } from 'react-router';

class StaticPage extends React.Component {
  static propTypes = {
    h1: PropTypes.string.isRequired,
    content: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]).isRequired
  };

  render() {
    return (
      <div>
        <Header />
        <PageContent
          {...this.props}
          onReturn={() => {
            this.props.history.goBack();
          }}
        />
        <Footer />
      </div>
    );
  }
}

export default withRouter(StaticPage);
