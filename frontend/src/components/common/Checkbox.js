import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const CheckboxWrapper = styled.span`
  padding-right: 20px;
  vertical-align: middle;
`;

const CheckboxInput = styled.input`
  visibility: hidden;
`;

const ArrowWrapper = styled.div`
  display: inline-block;
  position: relative;
  height: 16px;
  width: 16px;
  margin-right: 8px;
  box-sizing: border-box;
  ${props =>
    props.checked
      ? 'background-color: #00a693;'
      : 'border: 1px solid #c1c1c1;background-color: #ffffff;'};
`;

const LabelWrapper = styled.div`
  display: inline-block;
  color: #2e2e2e;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  font-weight: 400;
  vertical-align: top;
`;

const Svg = styled.img`
  position: absolute;
  top: 3px;
  left: 2px;
  width: 12px;
  height: 10px;
`;

function Checkbox({ clickHandler, state, children, name }) {
  return (
    <CheckboxWrapper className="checkbox">
      <CheckboxInput
        type="checkbox"
        onClick={clickHandler}
        value={state}
        id={name}
        hidden
      />
      <label htmlFor={name}>
        <ArrowWrapper checked={state}>
          {state && <Svg src="images/tick.svg" />}
        </ArrowWrapper>
        {children !== null && <LabelWrapper>{children}</LabelWrapper>}
      </label>
    </CheckboxWrapper>
  );
}

Checkbox.propTypes = {
  clickHandler: PropTypes.func.isRequired,
  state: PropTypes.bool.isRequired,
  children: PropTypes.node,
  name: PropTypes.string.isRequired
};

Checkbox.defaultProps = {
  children: null
};

export default Checkbox;
