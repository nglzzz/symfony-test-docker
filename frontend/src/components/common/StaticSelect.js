import React from 'react'
import PropTypes from 'prop-types'
import $ from 'jquery'
import 'jquery-ui-dist/jquery-ui'
import 'select2'

class StaticSelect extends React.Component {
    constructor(props) {
        super(props);

        this.handleSelect = this.handleSelect.bind(this);
    }

    componentDidMount() {
        const self = this;

        $(this.select)
            .select2({
                ...this.props.options
            })
            .on('change', (event) => {
                const target = event.target;
                const searchValue = target.value;

                setTimeout(() => {
                    self.handleSelect(searchValue);
                }, 0);
            });
    }

    handleSelect = (newValue) => {
        this.props.handleSelect(newValue);
    };

    render() {
        return (
            <select ref={(select) => {this.select = select}}
            />
        )
    }
}

StaticSelect.propTypes = {
    handleSelect: PropTypes.func.isRequired,
    options: PropTypes.shape()
};

StaticSelect.defaultProps = {
    options: {}
};

export default StaticSelect
