import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Checkbox from './Checkbox';
import onClickOutside from 'react-onclickoutside';

const Wrapper = styled.div`
  position: relative;

  &.opened {
    box-shadow: 0 2px 7px rgba(0, 166, 147, 0.1);
    background-color: #ffffff;
  }
`;

const SelectButton = styled.button`
  border: none;
  height: 45px;
  background: #fff;

  @media all and (min-width: 768px) {
    border: 1px solid #c1c1c1;
    height: 37px;
  }

  position: relative;
  width: 100%;

  font-family: 'Roboto', sans-serif;
  font-size: 13px;

  border-radius: 0;
  padding: 9px;
  text-align: left;
  padding-left: 20px;

  &:focus {
    outline: none;
  }

  &::after {
    content: '';
    position: absolute;
    top: 15px;
    right: 10px;
    border: none;
    height: 7px;
    margin-left: -10px;
    width: 12px;
  }

  .closed > & {
    color: #888888;

    &::after {
      background-image: url(/images/icons/chevron-down.svg);
    }
  }

  .opened > & {
    color: #2e2e2e;
    border: 1px solid #98c4bf;
    border-bottom: none;

    &::after {
      background-image: url(/images/icons/chevron-up.svg);
    }
  }
`;

const Options = styled.ul`
  list-style: none;
  margin: 0;
  padding: 5px;
  background-color: #fff;

  position: absolute;
  z-index: 5;
  right: 0;
  left: 0;
  border-bottom: 1px solid #98c4bf;
  border-left: 1px solid #98c4bf;
  border-right: 1px solid #98c4bf;

  display: none;

  .opened & {
    display: block;
  }
`;

const Option = styled.li`
  color: #2e2e2e;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  padding-left: 15px;
`;

class SelectWithCheckboxes extends React.Component {
  static propTypes = {
    setCheckboxState: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        selected: PropTypes.bool.isRequired
      })
    ).isRequired,
    placeholderClosed: PropTypes.string,
    descriptionOpened: PropTypes.string,
    defaultOpened: PropTypes.bool
  };

  static defaultProps = {
    placeholderClosed: 'Open',
    descriptionOpened: 'Close',
    defaultOpened: false
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpen: props.defaultOpened
    };

    this.toggleSelect = this.toggleSelect.bind(this);
    this.setCheckboxState = this.setCheckboxState.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  toggleSelect() {
    this.setState(state => ({
      isOpen: !state.isOpen
    }));
  }

  handleClickOutside = () => {
    if (this.state.isOpen) {
      this.toggleSelect();
    }
  };

  setCheckboxState = option => event => {
    const field = event.target.id;

    this.props.setCheckboxState(field, !option.selected);
  };

  render() {
    return (
      <Wrapper className={this.state.isOpen ? 'opened' : 'closed'}>
        <SelectButton onClick={this.toggleSelect}>
          {this.state.isOpen
            ? this.props.descriptionOpened
            : this.props.placeholderClosed}
        </SelectButton>
        <Options>
          {this.props.options.map((option) => (
            <Option key={option.id}>
              <Checkbox
                clickHandler={this.setCheckboxState(option)}
                name={option.id}
                state={option.selected}
              >
                {option.component}
              </Checkbox>
            </Option>
          ))}
        </Options>
      </Wrapper>
    );
  }
}

export default onClickOutside(SelectWithCheckboxes);
