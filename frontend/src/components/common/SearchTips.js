import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import config from 'config';
import CloseButton from 'components/common/CloseButton';

const SearchTipsWrapper = styled.div`
  display: block;
  padding: 15px 0 0;

  ${props => (props.shouldBeCentered ? 'text-align:center;' : '')};
`;

const SearchTipsButton = styled.a`
  display: inline-block;
  padding: 2px 0 0 25px;
  background: url(../images/icons/tip.png) no-repeat left top;
  font-size: 13px;
  outline: none;

  &:hover {
    text-decoration: underline;
    color: #0e8268;
  }
`;

const SearchTipsModal = styled.div``;

const ModalDialog = styled.div`
  margin-top: 206px;
  max-width: 494px;
  box-shadow: 0 0 5px 0 rgba(50, 50, 50, 0.75);
`;

const ModalContent = styled.div`
  border-radius: 0;
  border: none;
`;

const ModalHeader = styled.div`
  padding: 31px 28px 0;
  border-bottom: none;
`;

const TipsTitle = styled.h5`
  font: bold 15px 'Roboto Slab', serif;
  text-transform: uppercase;
  color: #00a693;
  padding-left: 27px;
  background: url(../images/icons/tip.png) no-repeat left -2px;
  letter-spacing: 1.6px;
`;

const HeaderCloseButton = styled.button`
  && {
    background: url(../images/icons/close-select.png) no-repeat center center;
    margin: -35px -23px 0 auto;
    opacity: 1;
    outline: none;
    cursor: pointer;
  }
`;

const ModalBody = styled.ul`
  list-style-image: url(../images/icons/list-arrow.png);
  padding: 17px 47px 0;

  li {
    text-align: left;
    padding-left: 8px;
    margin-bottom: 11px;
    color: #383838;
    font-size: 13px;

    .example {
      margin-left: 30px;
    }
  }
`;

const ModalFooter = styled.div`
  justify-content: flex-start;
  padding: 2px 35px 35px;
  border-top: none;
`;

function SearchTips({ shouldBeCentered }) {
  return (
    <SearchTipsWrapper shouldBeCentered={shouldBeCentered}>
      <SearchTipsButton href="#" data-toggle="modal" data-target="#tipsModal">
        Search tips
      </SearchTipsButton>
      <SearchTipsModal className="modal fade" id="tipsModal">
        <ModalDialog role="document" className="modal-dialog">
          <ModalContent className="modal-content">
            <ModalHeader className="modal-header">
              <TipsTitle>Search tips</TipsTitle>
              <HeaderCloseButton
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                &nbsp;
              </HeaderCloseButton>
            </ModalHeader>
            <ModalBody className="modal-body">
              {config.searchTips.map((line, index) => (
                <li key={index}>
                  <div>{line.text}</div>
                  <div className="example">Example: {line.example}</div>
                </li>
              ))}
            </ModalBody>
            <ModalFooter className="modal-footer">
              <CloseButton data-dismiss="modal" />
            </ModalFooter>
          </ModalContent>
        </ModalDialog>
      </SearchTipsModal>
    </SearchTipsWrapper>
  );
}

SearchTips.propTypes = {
  shouldBeCentered: PropTypes.bool
};

SearchTips.defaultProps = {
  shouldBeCentered: false
};

export default SearchTips;
