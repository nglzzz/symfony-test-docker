import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import $ from 'jquery'
import 'jquery-ui-dist/jquery-ui'
import '@fengyuanchen/datepicker/dist/datepicker.min'
import '@fengyuanchen/datepicker/dist/datepicker.min.css'
import 'assets/css/datepicker.css'

const FilterDate = styled.input`
  background: url(../images/icons/calendar.svg) no-repeat right 5px top 9px;
  background-size: 16px;
  border: 1px solid #c1c1c1;
  color: #888888;
  cursor: pointer;
  font: normal 13px "Roboto", sans-serif;
  height: 37px;
  padding-left: 5px;
  float: left;
  width: 100%;

  &:focus {
    border-color: #98c4bf;
    color: #000000;
    outline: none;
    box-shadow: 0 2px 2px 2px rgba(237,249,247,1);
  }

  @media all and (min-width: 768px) {
    width: 120px;
  }

  ${props => props.error && 'border-color: #A63400;'};
`

class DatePicker extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      value: props.value,
      error: false,
    }

    this.onBlur = this.onBlur.bind(this)
    this.changeState = this.changeState.bind(this)
    this.changeHandler = this.changeHandler.bind(this)
  }

  componentDidMount() {
    const self = this;

    const resultsVeil = $('.results-veil');

    $(this.input).datepicker({
      autoHide: true,
      format: 'yyyy-mm-dd',
      pick: () => {
        self.changeHandler($(self.input).datepicker('getDate', true))
      },
      template: `
        <div class="datepicker-container">
          <div class="datepicker-header d-sm-none">
            <span>Choose Date</span>
            <div class="close-datepicker">&nbsp;</div>
          </div>
          <div class="datepicker-panel" data-view="years picker">
            <ul>
              <li data-view="years prev">&lsaquo;</li>
              <li data-view="years current"></li>
              <li data-view="years next">&rsaquo;</li>
            </ul>
            <ul data-view="years"></ul>
          </div>
          <div class="datepicker-panel" data-view="months picker">
            <ul>
              <li data-view="year prev">&lsaquo;</li>
              <li data-view="year current"></li>
              <li data-view="year next">&rsaquo;</li>
            </ul>
            <ul data-view="months"></ul>
          </div>
          <div class="datepicker-panel" data-view="days picker">
            <ul>
              <li data-view="month prev">&lsaquo;</li>
              <li data-view="month current"></li>
              <li data-view="month next">&rsaquo;</li>
            </ul>
            <ul data-view="week"></ul>
            <ul data-view="days"></ul>
          </div>
          <div class="datepicker-footer">
            <div class="datepick-cancel"><span class="icon-cross">&nbsp;</span>Clear</div>
          </div>
        </div>`,
      show() {
        $('.datepicker-container .datepick-cancel').on('click', () => {
          $(self.input).datepicker('reset').datepicker('hide');
          self.changeHandler(null)
        });
        $('.datepicker-container .close-datepicker').on('click', () => {
          $(self.input).datepicker('hide');
        });
        resultsVeil.show();
      },
      hide() {
        $('.datepicker-container .datepick-cancel').off('click');
        resultsVeil.hide();
      },
    })
  }

  componentWillReceiveProps(props) {
    this.setState({
      value: props.value,
      error: false,
    })
  }

  onBlur = event => {
    if (event.target.value.match(/^\d{4}-\d{2}-\d{2}$/) || event.target.value === '') {
      this.changeHandler(event.target.value)
    } else {
      this.setState({
        error: true,
      })
    }
  }

  changeState = event => {
    this.setState({
      error: false,
    })
    this.setState({
      value: event.target.value,
    })
  }

  changeHandler = value => {
    this.setState({
      error: false,
    })

    this.props.changeHandler(value !== '' ? value : null)
  }

  render() {
    const value = this.state.value ? this.state.value : ''

    return (
      <FilterDate
        className="filter-date"
        id={this.props.name}
        error={this.state.error}
        innerRef={input => {
          this.input = input
        }}
        type="text"
        value={value}
        onBlur={this.onBlur}
        onChange={this.changeState}
      />
    )
  }
}

DatePicker.propTypes = {
  value: PropTypes.string,
  changeHandler: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
}

DatePicker.defaultProps = {
  value: '',
}

export default DatePicker
