import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Input = styled.input`
  padding: 8px 16px;
  color: #888888;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  font-weight: 400;
  border: 1px solid #c1c1c1;
  background-color: #ffffff;
  margin-bottom: 15px;
  width: 100%;
`;

function SearchInputField({changeHandler, state, children, name}) {
  return (
    <Input onChange={changeHandler} placeholder={children} id={name} value={state} />
  )
}

SearchInputField.propTypes = {
  changeHandler: PropTypes.func.isRequired,
  state: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

export default SearchInputField
