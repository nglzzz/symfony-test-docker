import styled from 'styled-components';

export const Tooltip = styled.div`
  visibility: hidden;

  *:hover > & {
    visibility: visible;
  }

  position: absolute;
  top: ${props =>
    (props.additionalTopOffset ? props.additionalTopOffset : 0) - 45}px;
  left: 50%;
  transform: translate(-50%, 0);
  z-index: 10;

  background-color: ${props =>
    props.backgroundColor ? props.backgroundColor : 'inherit'};
  color: ${props => (props.color ? props.color : '#ffffff')};
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  ${props => (props.bold ? 'font-weight: 700;' : '')} padding: 10px 15px;
  border-color: ${props =>
    props.backgroundColor ? props.backgroundColor : 'inherit'};
  white-space: nowrap;

  &:after {
    content: '';
    display: block;
    position: absolute;

    bottom: -5px;
    left: 50%;
    margin-left: -5px;

    width: 0;
    height: 0;

    border-left: 5px solid transparent;
    border-right: 5px solid transparent;

    border-top: 5px solid;
    border-top-color: ${props =>
      props.backgroundColor ? props.backgroundColor : 'inherit'};
  }

  @media all and (max-width: 1300px) {
    visibility: hidden;
    width: fit-content;
    top: -7px;
    left: 34px;
    transform: none;

    &:after {
      display: none;
    }
    &:before {
      content: '';
      display: block;
      position: absolute;

      bottom: 15px;
      left: 0%;
      margin-left: -5px;

      width: 0;
      height: 0;

      border-top: 5px solid transparent;
      border-bottom: 5px solid transparent;

      border-right: 5px solid;
      border-right-color: ${props =>
        props.backgroundColor ? props.backgroundColor : 'inherit'};
    }
  }

  &.verificationTooltip {
    &:before {
      left: 103%;

      border-top: 5px solid transparent;
      border-bottom: 5px solid transparent;
      border-right: 5px solid transparent;

      border-left: 5px solid;
      border-left-color: ${props =>
        props.backgroundColor ? props.backgroundColor : 'inherit'};
    }
    @media all and (min-width: 768px) {
      &:before {
        left: 0;
        margin-left: -10px;

        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        border-left: 5px solid transparent;

        border-right: 5px solid;
        border-right-color: ${props =>
          props.backgroundColor ? props.backgroundColor : 'inherit'};
      }
    }
    @media all and (min-width: 1300px) {
      left: 50%;
      top: -48px;
    }
  }
`;
