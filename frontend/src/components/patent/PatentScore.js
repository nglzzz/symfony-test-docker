import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const ScoreWrapper = styled.div`
  display: inline-block;
  border-radius: 2px;
  width: 39px;
  padding-top: 1px;
  padding-bottom: 1px;
  height: 21px;

  ${ props => props.red ? 'background-color: #e65b5b;' : '' }
  ${ props => props.yellow ? 'background-color: #e8b22f;' : '' }
  ${ props => props.green ? 'background-color: #00a693;' : '' }
  ${ props => props.gray ? 'background-color: #b9c0bf;' : '' }
  
  color: #ffffff;
  font-family: "Roboto", sans-serif;
  font-size: 13px;
  font-weight: 800;
  text-align: center;
`;

function PatentScore({value}) {
  if (value === null) {
    return <ScoreWrapper gray>&ndash;</ScoreWrapper>
  }

  if (value > 66) {
    return <ScoreWrapper green>{ value }</ScoreWrapper>
  } else if (value > 33) {
    return <ScoreWrapper yellow>{ value }</ScoreWrapper>
  }

  return <ScoreWrapper red>{value}</ScoreWrapper>
}

PatentScore.propTypes = {
  value: PropTypes.number,
};

PatentScore.defaultProps = {
  value: null
};

export default PatentScore
