import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const SourceText = styled.span`
  color: #00a693;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  font-weight: 400;
  text-transform: uppercase;
`;

const Tooltip = styled.span`
  visibility: hidden;
  
  position: absolute;
  z-index: 1000;
  top: -80px;
  left: 80px;
  transform: translate(-50%, 0);

  border: 1px solid #98c4bc;
  box-shadow: 2px 4px 10px 0 rgba(152, 196, 188, 0.75);
  width: 200px;

  background-color: #ffffff;
  color: #000000;
  padding: 15px;

  &::before {
    content: '';
    display: block;
    position: absolute;
    bottom: -13px;
    left: 10%;
    margin-left: -5px;
    width: 25px;
    height: 13px;
    background: url(../images/tt.png);
  }

  *:hover > & {
    visibility: visible;
  }
  
  @media all and (min-width: 768px) {
    left: 84px;
    min-height: 70px;
  }
`;

const LinkWrapper = styled.div`
  position: relative;
  background-color: transparent;
  display: inline-block;
`;

function PatentSourceLink({children, description}) {
  return <LinkWrapper>
    <SourceText>{children}</SourceText>
    <Tooltip>{description}</Tooltip>
  </LinkWrapper>
}

PatentSourceLink.propTypes = {
  children: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};

export default PatentSourceLink
