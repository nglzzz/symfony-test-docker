import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Tooltip } from 'components/common/Tooltip';
import { getVerificationStatusName } from 'constants/VerificationStatuses';

const VerificationWrap = styled.div`
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: row-reverse;
  justify-content: flex-end;
  align-items: center;

  @media all and (min-width: 768px) {
    width: 100%;
    flex-direction: row-reverse;
    justify-content: flex-start;
  }
  @media all and (min-width: 1200px) {
    width: 100%;
    justify-content: center;
  }
`;

const LinkLabel = styled.span`
  padding-right: 11px;
  color: #00a693;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  text-decoration: underline;


  @media all and (min-width: 768px) {
      display: inline;
    }
  
  @media all and (min-width: 1200px) {
      display: none;
    }
  }
`;

const GlobalLabel = styled.span`
  display: block;
  position: relative;
  margin-top: 2px;
  padding-left: 28px;
  color: #2e2e2e;
  margin-right: 9px;
  font-weight: bold;
  text-decoration: none;
  &::before {
    position: absolute;
    top: 0;
    left: 0;
    content: '';
    width: 23px;
    height: 15px;
    background: url(../images/gpr-logo.png) no-repeat 0 0;
  }
  @media all and (min-width: 768px) {
    display: none;
  }
`;

const VerificationMark = styled.button`

  display: inline-block;
  border-radius: 50%;
  border-width: 2px;
  border-style: solid;
  width: 24px;
  height: 24px;
  cursor: pointer;

  background: 3px 6px no-repeat;
  background-size: 14px 10px;

  @media all and (min-width: 768px) {
    position: static;
  }

  &:focus {
    outline: none;
  }

  ${props =>
    props.ipwe
      ? 'background-color: #eee9e0; border-color: #e5dac8; background-image: url(../images/icons/tick-gold.svg);'
      : ''} ${props =>
      props.owner
        ? 'background-color: #d9eae8; border-color: #b0dfda; background-image: url(../images/icons/tick-green.svg);'
        : ''} ${props =>
      props.patentOffice
        ? 'background-color: #e1eaf9; border-color: #cdd9ec; background-image: url(../images/icons/tick-blue.svg);'
        : ''};
`;

const Dash = styled.div`
  display: none;

  @media all and (min-width: 1200px) {
    display: inline-block;
    width: 10px;
    height: 2px;
    background-color: #9b9b9b;
  }
`;

function PatentVerification({ type, verificationDetails, clickHandler }) {
  if (type === 'IPWE_VERIFIED') {
    return (
      <VerificationWrap>
        <VerificationMark ipwe onClick={clickHandler}>
          <Tooltip
            color="#c6a776"
            backgroundColor="#eee9e0"
            additionalTopOffset={-2}
            className="verificationTooltip"
          >
            {getVerificationStatusName(type)}
          </Tooltip>
        </VerificationMark>
        <LinkLabel>IPWE&nbsp;verified</LinkLabel>
        <GlobalLabel>GPR:</GlobalLabel>
      </VerificationWrap>
    );
  } else if (type === 'OWNER_VERIFIED') {
    return (
      <VerificationWrap>
        <VerificationMark owner onClick={clickHandler}>
          <Tooltip
            color="#00a693"
            backgroundColor="#d9eae8"
            additionalTopOffset={-2}
            className="verificationTooltip"
          >
            {getVerificationStatusName(type)}
          </Tooltip>
        </VerificationMark>
        <LinkLabel>Owner&nbsp;verified</LinkLabel>
        <GlobalLabel>GPR:</GlobalLabel>
      </VerificationWrap>
    );
  } else if (type === 'NATIONAL_OFFICE_VERIFIED') {
    return (
      <VerificationWrap>
        <VerificationMark patentOffice onClick={clickHandler}>
          <Tooltip
            color="#7196cd"
            backgroundColor="#e1eaf9"
            additionalTopOffset={-2}
            className="verificationTooltip"
          >
            {getVerificationStatusName(type)}
          </Tooltip>
        </VerificationMark>
        <LinkLabel>Patent&nbsp;Office verified</LinkLabel>
        <GlobalLabel>GPR:</GlobalLabel>
      </VerificationWrap>
    );
  } else {
    return <Dash />;
  }
}

PatentVerification.propTypes = {
  type: PropTypes.string.isRequired,
  verificationDetails: PropTypes.arrayOf([]),
  clickHandler: PropTypes.func.isRequired
};

export default PatentVerification;
