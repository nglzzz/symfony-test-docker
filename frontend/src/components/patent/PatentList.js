import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import PatentText from 'components/patent/PatentText';

const ListWrapper = styled.ul`
  list-style: none;
  padding-left: 0;
  margin-bottom: 0;
`;

const ListItem = styled.li`
  border-bottom: 1px solid #e8e8e8;
  padding-top: 6px;
  padding-bottom: 6px;

  &:last-child {
    border-bottom: none;
  }
`;

function PatentList({values}) {
  return <ListWrapper>
    { values.map(value => <ListItem key={value}><PatentText>{value}</PatentText></ListItem>) }
  </ListWrapper>
}

PatentList.propTypes = {
  values: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default PatentList
