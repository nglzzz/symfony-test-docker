import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Tooltip } from 'components/common/Tooltip';

const Type = styled.div`
  position: relative;
  border-radius: 50%;
  width: 24px;
  height: 24px;
  padding: 3px 7px;

  background-color: ${props => props.color};
  border-color: ${props => props.color};
`;

const TypeText = styled.div`
  color: #ffffff;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  font-weight: 700;
  text-transform: uppercase;
`;

function PatentType({ value }) {
  if (value === 'A') {
    return (
      <Type color="#d3b688">
        <Tooltip bold>Application</Tooltip>
        <TypeText>A</TypeText>
      </Type>
    );
  } else if (value === 'G') {
    return (
      <Type color="#00a693">
        <Tooltip bold>Grant</Tooltip>
        <TypeText>G</TypeText>
      </Type>
    );
  } else if (value === 'N') {
    return (
      <Type color="#b9c0bf">
        <Tooltip bold>Non Patent Document</Tooltip>
        <TypeText>N</TypeText>
      </Type>
    );
  }
}

PatentType.propTypes = {
  value: PropTypes.string.isRequired
};

export default PatentType;
