import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Text = styled.span`
  color: #383838;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  font-weight: 400;
`;

function PatentText({children}) {
  return <Text>{children}</Text>
}

PatentText.propTypes = {
  children: PropTypes.string.isRequired,
};

export default PatentText
