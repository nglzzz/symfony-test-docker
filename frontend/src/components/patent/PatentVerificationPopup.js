import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import CloseButton from 'components/common/CloseButton';
import { withRouter } from 'react-router';
import * as SearchActions from 'actions/SearchActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Loader from 'react-loader';
import $ from 'jquery';

const WindowWrapper = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background: rgb(0, 0, 0, 0.2);
  z-index: 20;

  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
`;
const Window = styled.div`
  min-width: 300px;
  position: relative;
  z-index: 25;

  background: #fff;
  padding: 20px;
  padding-bottom: 0;
  overflow: hidden;

  @media all and (max-width: 480px) {
    max-height: 100%;
  }

  @media all and (min-width: 768px) {
    width: 640px;
    padding: 40px;
    padding-bottom: 0;
  }

  @media all and (min-width: 1200px) {
    width: 720px;
  }
`;

const Header = styled.h1`
  margin-bottom: 30px;
  color: #00a693;
  font-family: 'Roboto Slab', sans-serif;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
  letter-spacing: 0.8px;
`;
const DataType = styled.p`
  color: #383838;
  font-weight: bold;
  border-bottom: 1px solid #dddddd;
`;
const VerificationsWrapper = styled.div`
  overflow-x: hidden;
  overflow-y: auto;
  max-height: calc(75vh - 130px);

  @media all and (max-width: 480px) {
    max-height: calc(100vh - 130px);
  }
`;
const VerificationsList = styled.ul`
  margin-top: 5px;
  overflow: visible;
  list-style: none;
  padding-left: 0;
  max-height: 60vh;
  margin-bottom: 30px;

  @media all and (min-width: 768px) {
    margin-bottom: 15px;
    margin-top: 30px;
  }

  @media all and (min-width: 1200px) {
    max-height: 420px;
  }

  li:not(:last-child)::after {
    content: '';
    display: block;
    height: 1px;
    background-color: #e8e8e8;
    margin-top: 15px;
    margin-bottom: 15px;
    width: 100%;
  }
`;
const VerificationItem = styled.li`
  &.hidden {
    display: none;
  }
`;

const DescriptionLine = styled.p`
  display: flex;
  margin-bottom: 20px;
`;

const Bold = styled.span`
  display: block;
  width: 25%;
  font-size: 12px;
  font-weight: bold;
  @media all and (max-width: 560px) {
    width: 40%;
  }
`;
const Value = styled.span`
  font-size: 12px;
  width: 75%;
  @media all and (max-width: 560px) {
    width: 60%;
  }
`;

const ShowMore = styled.span`
  position: relative;
  color: #00a693;
  font-size: 12px;
  padding-right: 16px;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
  &::after {
    content: '';
    right: 0;
    top: 5px;
    position: absolute;
    width: 8px;
    height: 5px;
    background: url(/images/icons/chevron-down-green.svg);
  }
`;
const HideMore = ShowMore.extend`
  display: none;
  &:after {
    transform: rotate(180deg);
  }
`;

const CrossButton = styled.button`
  position: absolute;
  top: 14px;
  right: 16px;

  width: 16px;
  height: 16px;
  background-image: url(/images/icons/close-select.png);
  cursor: pointer;

  border: none;
`;

const CloseButtonWrapper = styled.div`
  margin-top: 15px;
  margin-bottom: 20px;

  @media all and (min-width: 768px) {
    margin-top: 20px;
    margin-bottom: 25px;
  }

  @media all and (min-width: 1200px) {
  }
`;

const Veil = styled.div`
  position: fixed;
  z-index: 7;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: rgba(62, 78, 76, 0.4);
`;

const ErrorWrapper = styled.div`
  padding-top: 20px;
  padding-bottom: 20px;
`;

const SpinnerContainer = styled.div`
  padding: 20px;
`;

const SpinnerRow = styled.div``;

const SpinnerColumn = styled.div`
  height: 100px;
`;

const options = {
  lines: 8,
  length: 0,
  width: 20,
  radius: 30,
  scale: 0.45,
  corners: 1,
  color: '#0e8268',
  fadeColor: 'transparent',
  opacity: 0.2,
  rotate: 0,
  direction: 1,
  speed: 1.7,
  trail: 100,
  fps: 20,
  zIndex: 2e9,
  className: 'spinner',
  top: '50%',
  left: '50%',
  position: 'absolute'
};

class PatentVerificationPopup extends React.Component {
  static propTypes = {
    loadVerificationDetails: PropTypes.func.isRequired,
    closeHandler: PropTypes.func.isRequired,
    verifications: PropTypes.arrayOf(
      PropTypes.shape({
        event_type: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        transaction_id: PropTypes.string.isRequired,
        event_attributes: PropTypes.shape({
          label: PropTypes.string.isRequired,
          value: PropTypes.string.isRequired
        })
      })
    ),
    isLoading: PropTypes.bool.isRequired
  };

  static defaultProps = {
    details: []
  };

  componentDidMount() {
    this.props.loadVerificationDetails();
  }

  showDetails = event => {
    event.target.style.display = 'none';
    let li = $(event.target).closest('li'),
      type = li.data('type');

    li
      .parent()
      .find('[data-type="' + type + '"]')
      .show()
      .find('.hide-details')
      .show();
  };

  hideDetails = event => {
    event.target.style.display = 'none';
    let li = $(event.target).closest('li'),
      type = li.data('type');

    li
      .parent()
      .find('.hidden')
      .hide();
    li
      .parent()
      .find('[data-type="' + type + '"]')
      .find('.show-details')
      .show();
  };

  render() {
    let { closeHandler, isLoading, verifications, error } = this.props;

    let content = null;

    if (isLoading) {
      content = (
        <SpinnerContainer className="container">
          <SpinnerRow className="row">
            <SpinnerColumn className="col">
              <Loader loaded={false} options={options} className="spinner" />
            </SpinnerColumn>
          </SpinnerRow>
        </SpinnerContainer>
      );
    } else {
      if (error) {
        content = (
          <ErrorWrapper>
            <Bold>Error:</Bold> {error.message}
          </ErrorWrapper>
        );
      } else {
        if (verifications.length) {
          let counter = {
            GLOBAL_REGISTRY: 0,
            PATENT_OFFICE: 0,
            ORIGINAL_PUBLICATION_DATA: 0
          };
          verifications.map(verification => {
            counter[verification.event_type]++;
          });
          content = (
            <VerificationsWrapper className="verification-wrapper">
              {['GLOBAL_REGISTRY', 'PATENT_OFFICE', 'ORIGINAL_PUBLICATION_DATA']
                .filter(
                  type =>
                    verifications.filter(
                      verification => verification.event_type === type
                    ).length > 0
                )
                .map(type => (
                  <div>
                    <DataType>
                      {type === 'GLOBAL_REGISTRY'
                        ? 'Owner Verified'
                        : type === 'PATENT_OFFICE'
                          ? 'National Office Assignment'
                          : 'Original Publication Data'}
                    </DataType>
                    <VerificationsList className="verification-list">
                      {verifications
                        .filter(
                          verification => verification.event_type === type
                        )
                        .sort(
                          (verificationA, verificationB) =>
                            Date.parse(verificationB.date) -
                            Date.parse(verificationA.date)
                        )
                        .map((detailsItem, index) => (
                          <VerificationItem
                            data-type={detailsItem.event_type}
                            className={index > 0 ? 'hidden' : ''}
                          >
                            <DescriptionLine>
                              <Bold>Date:</Bold>
                              <Value>{detailsItem.date}</Value>
                            </DescriptionLine>
                            <DescriptionLine>
                              <Bold>Verification type:</Bold>{' '}
                              <Value>
                                {detailsItem.event_type.replace(/_/g, ' ')}
                              </Value>
                            </DescriptionLine>
                            <DescriptionLine>
                              <Bold>Description:</Bold>{' '}
                              <Value>
                                {detailsItem.description
                                  ? detailsItem.description
                                  : 'No description'}
                              </Value>
                            </DescriptionLine>
                            {(detailsItem.event_attributes || []).map(
                              attribute => (
                                <DescriptionLine className="details-item">
                                  <Bold>{attribute.label}: </Bold>
                                  <Value>
                                    {(attribute.value || '')
                                      .split(';')
                                      .map(item => {
                                        return (
                                          <div>
                                            {item}
                                            <br />
                                          </div>
                                        );
                                      })}
                                  </Value>
                                </DescriptionLine>
                              )
                            )}
                            {index === 0 &&
                            counter[detailsItem.event_type] > 1 ? (
                              <ShowMore
                                onClick={this.showDetails}
                                className="show-details"
                              >
                                Show More
                              </ShowMore>
                            ) : (
                              ''
                            )}
                            {index + 1 === counter[detailsItem.event_type] ? (
                              <HideMore
                                onClick={this.hideDetails}
                                className="hide-details"
                              >
                                Hide
                              </HideMore>
                            ) : (
                              ''
                            )}
                          </VerificationItem>
                        ))}
                    </VerificationsList>
                  </div>
                ))}
            </VerificationsWrapper>
          );
        } else {
          content = <p>No information reported</p>;
        }
      }
    }

    return (
      <WindowWrapper>
        <Window>
          <Header>Verification Details</Header>
          {content}
          <CloseButtonWrapper>
            <CloseButton onClick={closeHandler} />
          </CloseButtonWrapper>
          <CrossButton onClick={closeHandler} />
        </Window>
      </WindowWrapper>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(SearchActions, dispatch);
}

function mapStateToProps(state) {
  return {
    verifications: state.patent.verifications,
    isLoading: state.patent.inProgress,
    error: state.patent.error
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withRouter(PatentVerificationPopup)
);
