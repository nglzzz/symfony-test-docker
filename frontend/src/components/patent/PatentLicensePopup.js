import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import CloseButton from 'components/common/CloseButton';

const WindowWrapper = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 20;

  display: flex;
  justify-content: center;
  align-items: center;
`;

const Window = styled.div`
  min-width: 300px;
  position: relative;
  z-index: 25;

  background: #fff;
  padding: 20px 20px 0;
  overflow: hidden;
  
  a {
    display: block;
  }

  @media all and (max-width: 480px) {
    max-height: 100%;
  }

  @media all and (min-width: 768px) {
    width: 640px;
    padding: 40px;
    padding-bottom: 0;
  }

  @media all and (min-width: 1200px) {
    width: 720px;
  }
`;

const Header = styled.h1`
  margin-bottom: 30px;
  color: #00a693;
  font-family: 'Roboto Slab', sans-serif;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
  letter-spacing: 0.8px;
`;

const CrossButton = styled.button`
  position: absolute;
  top: 14px;
  right: 16px;

  width: 16px;
  height: 16px;
  background-image: url(/images/icons/close-select.png);
  cursor: pointer;

  border: none;
`;

const CloseButtonWrapper = styled.div`
  margin-top: 15px;
  margin-bottom: 20px;

  @media all and (min-width: 768px) {
    margin-top: 20px;
    margin-bottom: 25px;
  }
`;

class PatentLicensePopup extends React.Component {
  static propTypes = {
    closeHandler: PropTypes.func.isRequired,
    patent: PropTypes.shape({
      forLicense: PropTypes.arrayOf(PropTypes.shape({
        url: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
      })).isRequired
    }).isRequired
  };

  render() {
    let { closeHandler, patent } = this.props;

    return (
      <WindowWrapper>
        <Window>
          <Header>Available license programs</Header>
          {patent.forLicense.map((program, index) => (<a key={index} href={program.url} target="_blank" rel="noopener noreferrer">{program.name}</a>))}
          <CloseButtonWrapper>
            <CloseButton onClick={closeHandler} />
          </CloseButtonWrapper>
          <CrossButton onClick={closeHandler} />
        </Window>
      </WindowWrapper>
    );
  }
}

export default PatentLicensePopup;
