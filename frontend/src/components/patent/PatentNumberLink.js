import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Link = styled.a`
  color: #00a693;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  text-decoration: underline;
  font-weight: 400;
  text-transform: uppercase;
  margin-bottom: 6px;
  &:hover {
    text-decoration: none;
  }
`;

const NoLink = styled.span`
  color: #212529;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  font-weight: 400;
  text-transform: uppercase;
`;


function PatentNumberLink({href, children}) {
  if (href === null) {
    return <NoLink>{children}</NoLink>
  }

  return <Link href={href} target="_blank" rel="noopener noreferrer">{children}</Link>
}

PatentNumberLink.propTypes = {
  children: PropTypes.string.isRequired,
  href: PropTypes.string
};

PatentNumberLink.defaultProps = {
  href: null
};

export default PatentNumberLink
