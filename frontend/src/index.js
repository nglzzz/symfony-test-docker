import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import 'styles/global-styles'
import configureStore from 'store'
import { Router } from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'
import App from 'containers/App';
import {loadPatents, setQuery} from 'actions/SearchActions';
import config from 'config';

window.inspectletInit(config.inspectSiteId);

const store = configureStore();
const history = createHistory();
history.listen((location, action) => {
  if (location.pathname === '/results' && action === 'POP') {
    store.dispatch(setQuery(location.state));
    store.dispatch(loadPatents());
  }
});

render(
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);
