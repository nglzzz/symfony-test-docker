import { createStore, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import rootReducer from 'reducers'
import logger from 'redux-logger'

export default function configureStore(initialState = {}) {
  const middlewares = [ReduxThunk, logger];
  const enhancers = [
    applyMiddleware(...middlewares),
  ];
  const composeEnhancers = composeWithDevTools({});
  const store = createStore(rootReducer, initialState, composeEnhancers(...enhancers));
  if (module.hot) {
    module.hot.accept('../reducers', () => {
      /* eslint-disable global-require */
      const nextReducer = require('../reducers').default;
      store.replaceReducer(nextReducer)
    })
  }

  return store
}
