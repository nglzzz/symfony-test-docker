#!/bin/bash

BUNDLE_NAME=`find /var/frontend/static/js -name 'main*.js'`

escape () {
    echo $1 | sed -e 's/\//\\\//g'
}

SERVICES_URL_PARAMETER='${SERVICES_URL}'
IPWE_LINK_PARAMETER='${IPWE_LINK}'
INSPECT_SITE_ID_PARAMETER='${INSPECT_SITE_ID}'

SERVICES_URL=${SERVICES_URL:-https://api.zuseip.com}
IPWE_LINK=${IPWE_LINK:-https://ipwe.com}
INSPECT_SITE_ID=${INSPECT_SITE_ID:-0}

sed -i \
    -e "s/$SERVICES_URL_PARAMETER/`escape $SERVICES_URL`/g" \
    -e "s/$IPWE_LINK_PARAMETER/`escape $IPWE_LINK`/g" \
    -e "s/$INSPECT_SITE_ID_PARAMETER/`escape $INSPECT_SITE_ID`/g" $BUNDLE_NAME

cp /var/nginx/* /etc/nginx/conf.d/

if [[ $SYMFONY_ENV = 'dev' ]]; then
    BACKEND_ENTRY='app_dev'
else
    BACKEND_ENTRY='app'
fi

sed -i -e "s/\${BACKEND_ENTRY}/$BACKEND_ENTRY/g" /etc/nginx/conf.d/backend.https.conf
