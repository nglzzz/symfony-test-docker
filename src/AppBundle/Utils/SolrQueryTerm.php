<?php

namespace AppBundle\Utils;

class SolrQueryTerm implements SolrQueryNode
{
    /**
     * Solr engine's default boost value
     */
    public const DEFAULT_BOOST = 1;

    /**
     * @var string
     */
    private $field;

    /**
     * @var string
     */
    private $value;

    /**
     * @var SolrQueryExpression|null
     */
    private $parent;

    /**
     * @var int
     */
    private $boost;

    /**
     * SolrQueryTerm constructor.
     *
     * @param SolrQueryExpression $parent
     * @param string              $field
     * @param string              $value
     * @param int                 $boost
     */
    public function __construct(
        SolrQueryExpression $parent,
        string $field,
        string $value,
        int $boost = self::DEFAULT_BOOST
    ) {
        $this->field = $field;
        $this->value = $value;
        $this->parent = $parent;
        $this->boost = $boost;
    }

    /**
     * @return string
     */
    public function getAsQuery(): string
    {
        if ($this->boost !== self::DEFAULT_BOOST) {
            return sprintf('%s:%s^%d', $this->field, $this->value, $this->boost);
        }

        return sprintf('%s:%s', $this->field, $this->value);
    }

    /**
     * @param SolrQueryExpression|null $parent
     */
    public function setParent(?SolrQueryExpression $parent): void
    {
        $this->parent = $parent;
    }
}
