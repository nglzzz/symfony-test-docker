<?php

namespace AppBundle\Utils;

use AppBundle\Exception\SolrQueryBuilderException;
use Solarium\QueryType\Select\Query\FilterQuery;
use Solarium\QueryType\Select\Query\Query;

class SolrQueryExpression implements SolrQueryNode
{
    public const OR = ' OR ';
    public const AND = ' AND ';

    /**
     * @var string
     */
    private $type;

    /**
     * @var SolrQueryNode[]
     */
    private $nodes = [];

    /**
     * @var SolrQueryExpression|null
     */
    private $parent;

    /**
     * @var Query|FilterQuery
     */
    private $query;

    /**
     * SolrQueryExpression constructor.
     *
     * @param Query|FilterQuery        $query
     * @param SolrQueryExpression|null $parent
     * @param string                   $type
     */
    private function __construct($query, ?self $parent, string $type)
    {
        $this->type = $type;
        $this->parent = $parent;
        $this->query = $query;
    }

    /**
     * SolrQueryExpression constructor.
     *
     * @param Query $query
     *
     * @return self
     */
    public static function createQueryExpression(Query $query): self
    {
        return new self($query, null, self::AND);
    }

    /**
     * SolrQueryExpression constructor.
     *
     * @param FilterQuery $filterQuery
     *
     * @return self
     */
    public static function createFilterExpression(FilterQuery $filterQuery): self
    {
        return new self($filterQuery, null, self::AND);
    }

    /**
     * @param string $field
     * @param string $value
     * @param int    $boost
     *
     * @return self
     */
    public function addTerm(string $field, string $value, int $boost = SolrQueryTerm::DEFAULT_BOOST): self
    {
        if ('' !== $value) {
            $this->nodes[] = new SolrQueryTerm($this, $field, $value, $boost);
        }

        return $this;
    }

    /**
     * @param string $field
     * @param int    $value
     *
     * @return self
     */
    public function addNumber(string $field, int $value): self
    {
        $this->nodes[] = new SolrQueryNumber($this, $field, $value);

        return $this;
    }

    /**
     * @param string $field
     * @param string $from
     * @param string $to
     *
     * @return self
     */
    public function addDateRange(string $field, string $from, string $to): self
    {
        $this->nodes[] = new SolrQueryDateRange($this, $field, $from, $to);

        return $this;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function addExpression(string $type): self
    {
        $expression = new self($this->query, $this, $type);

        $this->nodes[] = $expression;

        return $expression;
    }

    /**
     * @return self
     */
    public function endExpression(): self
    {
        return $this->parent ?? $this;
    }

    /**
     * @param self|null $parent
     */
    public function setParent(?self $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return string
     */
    public function getAsQuery(): string
    {
        $length = \count($this->nodes);

        if (1 === $length) {
            $this->nodes[0]->setParent(null);

            return $this->nodes[0]->getAsQuery();
        }

        $queryAsString = implode($this->type, array_map(function (SolrQueryNode $node): string {
            return $node->getAsQuery();
        }, $this->nodes));

        if (null === $this->parent) {
            if (0 === $length) {
                return '*:*';
            }

            return $queryAsString;
        }

        if (0 === $length) {
            throw new SolrQueryBuilderException('Child expression has no childs');
        }

        return sprintf('(%s)', $queryAsString);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return 0 === \count($this->nodes);
    }

    /**
     * Build query.
     */
    public function buildQuery(): void
    {
        $this->query->setQuery($this->getAsQuery());
    }
}
