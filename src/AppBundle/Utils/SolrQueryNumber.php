<?php

namespace AppBundle\Utils;

class SolrQueryNumber implements SolrQueryNode
{
    /**
     * @var string
     */
    private $field;

    /**
     * @var int
     */
    private $value;

    /**
     * @var SolrQueryExpression|null
     */
    private $parent;

    /**
     * SolrQueryTerm constructor.
     *
     * @param SolrQueryExpression $parent
     * @param string              $field
     * @param int                 $value
     */
    public function __construct(SolrQueryExpression $parent, string $field, int $value)
    {
        $this->field = $field;
        $this->value = $value;
        $this->parent = $parent;
    }

    /**
     * @return string
     */
    public function getAsQuery(): string
    {
        return sprintf('%s:%d', $this->field, $this->value);
    }

    /**
     * @param SolrQueryExpression|null $parent
     */
    public function setParent(?SolrQueryExpression $parent): void
    {
        $this->parent = $parent;
    }
}
