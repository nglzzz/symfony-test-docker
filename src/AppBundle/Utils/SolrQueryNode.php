<?php

namespace AppBundle\Utils;

interface SolrQueryNode
{
    public function getAsQuery(): string;

    public function setParent(?SolrQueryExpression $parent): void;
}
