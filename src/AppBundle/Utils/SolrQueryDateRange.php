<?php

namespace AppBundle\Utils;

class SolrQueryDateRange implements SolrQueryNode
{
    /**
     * @var string
     */
    private $field;

    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    /**
     * @var SolrQueryExpression|null
     */
    private $parent;

    /**
     * SolrQueryTerm constructor.
     *
     * @param SolrQueryExpression $parent
     * @param string              $field
     * @param string              $from
     * @param string              $to
     */
    public function __construct(SolrQueryExpression $parent, string $field, string $from, string $to)
    {
        $this->field = $field;
        $this->parent = $parent;
        $this->from = $from;
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getAsQuery(): string
    {
        return sprintf('%s:[%s TO %s]', $this->field, $this->from, $this->to);
    }

    /**
     * @param SolrQueryExpression|null $parent
     */
    public function setParent(?SolrQueryExpression $parent): void
    {
        $this->parent = $parent;
    }
}
