<?php

namespace AppBundle\Controller;

use AppBundle\Exception\Service\UnexpectedRequestValueException;
use AppBundle\Exception\Service\UnexpectedServiceResponseExceptions;
use AppBundle\Service\SearchService;
use AppBundle\Service\VerificationsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    /**
     * @var SearchService
     */
    private $searchService;

    /**
     * @var VerificationsService
     */
    private $verificationsService;

    /**
     * SearchController constructor.
     *
     * @param SearchService $searchService
     * @param VerificationsService $verificationsService
     */
    public function __construct(SearchService $searchService, VerificationsService $verificationsService)
    {
        $this->searchService = $searchService;
        $this->verificationsService = $verificationsService;
    }

    /**
     * @Route("/search", name="search", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchAction(Request $request): JsonResponse
    {
        $query = json_decode($request->query->get('query'), true);

        if (JSON_ERROR_NONE === json_last_error()) {
            try {
                $searchResults = $this->searchService->search($query);

                return new JsonResponse(
                    $searchResults,
                    Response::HTTP_OK,
                    [
                        'Access-Control-Allow-Origin' => '*',
                    ]
                );
            } catch (UnexpectedRequestValueException $e) {
                return new JsonResponse(
                    [
                        'status' => 'error',
                        'message' => $e->getMessage(),
                    ],
                    Response::HTTP_BAD_REQUEST
                );
            }
        }

        return new JsonResponse(
            [
                'status' => 'error',
                'message' => 'Bad request',
            ],
            Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * @Route("/verifications", name="verifications", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function verificationsAction(Request $request): JsonResponse
    {
        $patentNumber = $request->query->get('patentNumber');

        if (null !== $patentNumber) {
            try {
                $verifications = $this->verificationsService->requestVerifications($patentNumber);

                return new JsonResponse(
                    $verifications,
                    Response::HTTP_OK,
                    [
                        'Access-Control-Allow-Origin' => '*',
                    ]
                );
            } catch (UnexpectedServiceResponseExceptions $e) {
                return new JsonResponse(
                    [
                        'status' => 'error',
                        'message' => 'Internal server error: Unexpected service response',
                    ],
                    Response::HTTP_INTERNAL_SERVER_ERROR
                );
            }
        }

        return new JsonResponse(
            [
                'status' => 'error',
                'message' => 'Bad request',
            ],
            Response::HTTP_BAD_REQUEST
        );
    }
}
