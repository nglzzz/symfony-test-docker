<?php

namespace AppBundle\Service;

use AppBundle\Exception\Service\UnexpectedServiceResponseExceptions;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class VerificationsService
{
    private const VERIFICATION_TYPE_IPWE_VERIFIED = 'IPWE_VERIFIED';
    private const VERIFICATION_TYPE_NOT_REPORTED = 'NOT_REPORTED';
    private const VERIFICATION_TYPE_OWNER_VERIFIED = 'OWNER_VERIFIED';
    private const VERIFICATION_TYPE_NATIONAL_OFFICE_VERIFIED = 'NATIONAL_OFFICE_VERIFIED';

    private const VERIFICATION_STATUSES = [
        self::VERIFICATION_TYPE_IPWE_VERIFIED,
        self::VERIFICATION_TYPE_NOT_REPORTED,
        self::VERIFICATION_TYPE_OWNER_VERIFIED,
        self::VERIFICATION_TYPE_NATIONAL_OFFICE_VERIFIED,
    ];

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Client
     */
    private $guzzleClient;

    /**
     * SearchService constructor.
     *
     * @param LoggerInterface $logger
     * @param string $servicesBaseUrl
     */
    public function __construct(LoggerInterface $logger, string $servicesBaseUrl)
    {
        $this->logger = $logger;
        $this->guzzleClient = new Client([
            'base_uri' => $servicesBaseUrl,
            'http_errors' => false,
        ]);
    }

    /**
     * @param string $patentNumber
     *
     * @return array
     *
     * @throws UnexpectedServiceResponseExceptions
     */
    public function requestVerifications(string $patentNumber): array
    {
        $response = $this->guzzleClient->get(sprintf('patents/%s/verification.json', $patentNumber));

        if ($response->getStatusCode() === Response::HTTP_OK) {
            $contents = $response->getBody()->getContents();
            $parsedResponse = json_decode($contents, true);

            if (JSON_ERROR_NONE === json_last_error()) {
                return $parsedResponse;
            }
        }

        throw new UnexpectedServiceResponseExceptions(
            sprintf('Verification service returned unexpected data')
        );
    }

    /**
     * @return array
     */
    public static function getVerificationStatuses(): array
    {
        return self::VERIFICATION_STATUSES;
    }

    /**
     * @param string $status
     *
     * @return bool
     */
    public static function isValidVerificationStatus(string $status): bool
    {
        return in_array($status, self::getVerificationStatuses());
    }

    /**
     * @param array $verificationStatuses
     *
     * @return bool
     */
    public static function areAllVerificationStatusesChecked(array $verificationStatuses): bool
    {
        return \count($verificationStatuses) === \count(self::getVerificationStatuses())
            && !array_diff($verificationStatuses, self::getVerificationStatuses());
    }
}
