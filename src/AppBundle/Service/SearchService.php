<?php

namespace AppBundle\Service;

use AppBundle\Exception\Service\UnexpectedRequestValueException;
use Psr\Log\LoggerInterface;
use Solarium\Client;
use Solarium\Core\Client\Request;
use Solarium\QueryType\Select\Query\Query;
use Solarium\QueryType\Select\Result\DocumentInterface;
use Solarium\QueryType\Select\Result\Result;
use AppBundle\Utils\SolrQueryExpression;

/**
 * Class SearchService.
 */
class SearchService
{
    private const APPLICATION_TYPE = 0;
    private const GRANT_TYPE = 1;
    private const NPL_TYPE = 2;

    private const DEFAULT_PER_PAGE = 20;

    /**
     * Used to increase field's significance
     */
    private const BOOST_PATENT_NUMBER = 10;

    private const DEFAULT_SORT_ORDER = [
        'priority_date' => 'desc',
        'source_label_string' => 'asc',
        'qscore' => 'desc',
        'pgr_score' => 'asc',
    ];

    private const ONLINE_SALE_TYPE = 'TYPE_ONLINE_SALE';
    private const LICENSE_TYPE = 'TYPE_LICENSE';

    private const AVAILABLE_PROGRAM_TYPES = [
        self::ONLINE_SALE_TYPE,
        self::LICENSE_TYPE,
    ];

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Client
     */
    private $solrClient;

    /**
     * SearchService constructor.
     *
     * @param LoggerInterface $logger
     * @param Client          $solrClient
     */
    public function __construct(LoggerInterface $logger, Client $solrClient)
    {
        $this->solrClient = $solrClient;

        $this->logger = $logger;
    }

    /**
     * @param array $parameters
     *
     * @return array
     *
     * @throws UnexpectedRequestValueException
     */
    public function search(array $parameters): array
    {
        $page = $parameters['paginationAttributes']['page'] ?? 1;
        $perPage = $parameters['paginationAttributes']['perPage'] ?? self::DEFAULT_PER_PAGE;
        $start = $perPage * ($page - 1);

        $advancedSearchEnabled = $parameters['isAdvancedSearchEnabled'] ?? false;

        $query = $this->solrClient->createSelect();
        $query->setHandler('search');

        $query->setStart($start);
        $query->setRows($perPage);

        if ($parameters['sortingField'] ?? null !== null) {
            if (!isset(self::DEFAULT_SORT_ORDER[$parameters['sortingField']])) {
                throw new UnexpectedRequestValueException(sprintf(
                    'Invalid sorting field: %s',
                    $parameters['sortingField']
                ));
            }

            $query->addSort(
                $parameters['sortingField'],
                self::DEFAULT_SORT_ORDER[$parameters['sortingField']] ?? 'desc'
            );
        }

        /**
         * @var string[]
         */
        $programTypes = $parameters['filteringAttributes']['programTypes'] ?? [];
        $this->filterByProgramType($query, $programTypes);

        /**
         * @var string[]
         */
        $currentAssignees = $parameters['filteringAttributes']['currentAssignees'] ?? [];
        $this->filterByCurrentAssignee($query, $currentAssignees);

        /**
         * @var string[]
         */
        $sources = $parameters['filteringAttributes']['documentSource'] ?? [];
        $this->filterBySources($query, $sources);

        /**
         * @var string[]
         */
        $priorityYears = $parameters['filteringAttributes']['priorityYears'] ?? [];
        $this->filterByPriorityDates($query, $priorityYears);

        /**
         * @var string[]
         */
        $verificationStatuses = $parameters['filteringAttributes']['verificationStatus'] ?? [];
        $this->filterByVerificationStatuses($query, $verificationStatuses);

        $queryExpression = SolrQueryExpression::createQueryExpression($query);

        if ($advancedSearchEnabled) {
            /**
             * @var string[]
             */
            $advancedSearchParameters = $parameters['advancedSearchAttributes'] ?? [];

            $title = $this->convertRequest($advancedSearchParameters['title'] ?? '');
            $abstract = $this->convertRequest($advancedSearchParameters['abstract'] ?? '');
            $inventor = $this->convertRequest($advancedSearchParameters['inventor'] ?? '');
            $specification = $this->convertRequest($advancedSearchParameters['specification'] ?? '');
            $claims = $this->convertRequest($advancedSearchParameters['claims'] ?? '');
            $currentAssignee = $this->convertRequest($advancedSearchParameters['currentAssignee'] ?? '');

            $queryExpression
                ->addExpression(SolrQueryExpression::AND)
                    ->addTerm('title', $title)
                    ->addTerm('abstract', $abstract)
                    ->addTerm('authors', $inventor)
                    ->addTerm('paragraph_text', $specification)
                    ->addTerm('claim_text', $claims)
                    ->addTerm('current_assignee', $currentAssignee)
                ->endExpression()
                ->buildQuery();

            $this->filterByPublicationTypes($query, $advancedSearchParameters);
        } else {
            $sourceQuery = $parameters['generalSearchQuery'] ?? '*';

            $isPatentNumber = $this->isPatentNumber($sourceQuery);

            $searchFields = [
                'label' => $isPatentNumber ? self::BOOST_PATENT_NUMBER : 1,
                'application_number' => $isPatentNumber ? self::BOOST_PATENT_NUMBER : 1,
                'authors' => 10, // Values selected by Dan on WIKI page
                'title' => 10,
                'current_assignee' => 10,
                'original_assignee' => 10,
                'paragraph_text' => 1,
                'abstract' => 5,
                'claims_text' => 2,
            ];

            $query->addParam('qf', \join(' ', \array_map(function ($key, $value) {
                return $value > 1 ? \sprintf('%s^%s', $key, $value) : $key;
            }, \array_keys($searchFields), $searchFields)));


            if ($isPatentNumber) {
                $query->setQuery($this->unifyPatentNumbers($sourceQuery));
            } else {
                $query->setQuery($sourceQuery);
            }

            $query->addParam('indent', 'on');
            $query->addParam('tie', '0.1');
            $query->addParam('mm', '100%');
            $query->addParam('defType', 'edismax');
        }

        $query->addParam('facet', 'true');
        $query->addParam('facet.field', 'current_assignee_string,source_label_string');

        $request = $this->solrClient->createRequest($query);

        $this->fixFilterQueryParameter($request);

        $this->logger->info('Sending request', [
            'request_uri' => $request->getUri(),
        ]);

        $response = $this->solrClient->executeRequest($request);

        /**
         * @var Result $results
         */
        $results = $this->solrClient->createResult($query, $response);

        return $this->parseResults($results, $perPage, $page);
    }

    /**
     * @param Query $query
     * @param array $programTypes
     */
    private function filterByProgramType(Query $query, array $programTypes): void
    {
        $filteredProgramTypes = \array_intersect(self::AVAILABLE_PROGRAM_TYPES, $programTypes);

        if (!$filteredProgramTypes) {
            return;
        }

        $programTypesFilter = $query->createFilterQuery('active_offer_types');
        $programTypesExpression = SolrQueryExpression::createFilterExpression($programTypesFilter)
            ->addExpression(SolrQueryExpression::OR);

        foreach ($filteredProgramTypes as $programType) {
            $programTypesExpression->addTerm('active_offer_types', \sprintf('"%s"', $programType));
        }

        $programTypesExpression->endExpression()->buildQuery();
    }

    /**
     * @param Query $query
     * @param array $currentAssignees
     */
    private function filterByCurrentAssignee(Query $query, array $currentAssignees): void
    {
        if (\count($currentAssignees) > 0) {
            $currentAssigneesFilter = $query->createFilterQuery('current_assignee');
            $currentAssigneesExpression = SolrQueryExpression::createFilterExpression($currentAssigneesFilter)
                ->addExpression(SolrQueryExpression::OR);

            foreach ($currentAssignees as $currentAssignee) {
                $currentAssigneesExpression->addTerm('current_assignee', sprintf('"%s"', $currentAssignee));
            }

            $currentAssigneesExpression->endExpression()->buildQuery();
        }
    }

    /**
     * @param Query $query
     * @param array $sources
     */
    private function filterBySources(Query $query, array $sources): void
    {
        if (\count($sources) > 0) {
            $sourcesFilter = $query->createFilterQuery('sources');
            $sourcesExpression = SolrQueryExpression::createFilterExpression($sourcesFilter)
                ->addExpression(SolrQueryExpression::OR);

            foreach ($sources as $source) {
                $sourcesExpression->addTerm('source_label_string', sprintf('"%s"', $source));
            }

            $sourcesExpression->endExpression()->buildQuery();
        }
    }

    /**
     * @param Query $query
     * @param array $priorityDates
     */
    private function filterByPriorityDates(Query $query, array $priorityDates): void
    {
        if (isset($priorityDates['from']) || isset($priorityDates['to'])) {
            $priorityDateFilter = $query->createFilterQuery('priority_date');
            $dateExpression = SolrQueryExpression::createFilterExpression($priorityDateFilter);

            $from = isset($priorityDates['from']) ? $this->toSolrDateFormat($priorityDates['from']) : '*';
            $to = isset($priorityDates['to']) ? $this->toSolrDateFormat($priorityDates['to']) : 'NOW';

            $dateExpression->addDateRange('priority_date', $from, $to);

            $dateExpression->buildQuery();
        }
    }

    /**
     * @param Query $query
     * @param array $verificationStatuses
     */
    private function filterByVerificationStatuses(Query $query, array $verificationStatuses): void
    {
        if (!$verificationStatuses ||
            VerificationsService::areAllVerificationStatusesChecked($verificationStatuses)) {
            return;
        }

        $verificationStatusFilter = $query->createFilterQuery('verification_status');
        $verificationStatusExpression = SolrQueryExpression::createFilterExpression($verificationStatusFilter)
            ->addExpression(SolrQueryExpression::OR);

        foreach ($verificationStatuses as $verificationStatus) {
            if (VerificationsService::isValidVerificationStatus($verificationStatus)) {
                $verificationStatusExpression->addTerm('verification_status', sprintf('"%s"', $verificationStatus));
            }
        }

        $verificationStatusExpression->endExpression()->buildQuery();
    }

    /**
     * @param Query $query
     * @param array $publicationTypes
     */
    private function filterByPublicationTypes(Query $query, array $publicationTypes): void
    {
        $searchNPLs = $publicationTypes['searchNPLs'] ?? false;
        $searchApplications = $publicationTypes['searchApplications'] ?? false;
        $searchGrants = $publicationTypes['searchGrants'] ?? false;

        if (!($searchNPLs === $searchApplications && $searchApplications === $searchGrants)) {
            $typeFilter = $query->createFilterQuery('type');

            $publicationTypeExpression = SolrQueryExpression::createFilterExpression($typeFilter)
                ->addExpression(SolrQueryExpression::OR);

            if ($searchNPLs) {
                $publicationTypeExpression->addNumber('publication_type', self::NPL_TYPE);
            }
            if ($searchApplications) {
                $publicationTypeExpression->addNumber('publication_type', self::APPLICATION_TYPE);
            }
            if ($searchGrants) {
                $publicationTypeExpression->addNumber('publication_type', self::GRANT_TYPE);
            }

            $publicationTypeExpression->endExpression()->buildQuery();
        }
    }

    /**
     * @param Result $results
     * @param int    $perPage
     * @param int    $page
     *
     * @return array
     */
    private function parseResults(Result $results, int $perPage, int $page): array
    {
        $onPage = $results->count();
        $countAll = $results->getNumFound();
        $pages = floor($countAll / $perPage) + (0 === $countAll % $perPage ? 0 : 1);

        $facetData = $results->getData()['facet_counts']['facet_fields'];



        return [
            'paginationAttributes' => compact('countAll', 'page', 'pages', 'onPage'),
            'patents' => \array_map(function (DocumentInterface $patent): array {
                $licensePrograms = \array_map(
                    function (array $offer): array {
                        return [
                            'name' => 'For license',
                            'url' => $offer['url']
                        ];
                    },
                    \array_values(\array_filter($patent['offers'], function (array $offer): bool {
                        return $offer['trans_type'] === self::LICENSE_TYPE;
                    }))
                );

                $salePrograms = \array_map(
                    function (array $offer): array {
                        return [
                            'name' => 'For sale',
                            'url' => $offer['url']
                        ];
                    },
                    \array_values(\array_filter($patent['offers'], function (array $offer): bool {
                        return $offer['trans_type'] === self::ONLINE_SALE_TYPE;
                    }))
                );

                return [
                    'id' => $patent['id'],
                    'number' => $patent['label'],
                    'applicationNumber' => $patent['application_number'],
                    'url' => $patent['url'],
                    'title' => $patent['title'],
                    'type' => $patent['pub_type'],
                    'forLicense' => $licensePrograms[0] ?? null,
                    'forSale' => $salePrograms[0] ?? null,
                    'qScore' => $patent['qscore'],
                    'vScore' => $patent['vscore'],
                    'sourceId' => $patent['source_id'],
                    'source' => $patent['source_label_string'],
                    'sourceDesc' => $patent['source_description'],
                    'priorityDate' => (new \DateTime($patent['priority_date'] ?? '1970-01-01'))->format('Y-m-d'),
                    'currentAssignee' => $patent['current_assignee_string'] ?? ['Unassigned'],
                    'classifications' => $patent['classifications'] ?? [],
                    'verificationStatus' => $patent['verification_status'],
                ];
            }, $results->getDocuments()),
            'currentAssignees' => $this->fixFacetListFormat($facetData['current_assignee_string'] ?? []),
            'sources' => $this->fixFacetListFormat($facetData['source_label_string'] ?? []),
        ];
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function fixFacetListFormat(array $data): array
    {
        $length = \count($data);
        $fixedFacets = [];

        for ($i = 0; $i < $length - 1; $i += 2) {
            if ($data[$i + 1] > 0) {
                $fixedFacets[] = [
                    'name' => $data[$i],
                    'count' => $data[$i + 1],
                ];
            }
        }

        return $fixedFacets;
    }

    /**
     * Convert user request to Solr syntax
     * Uppercase AND and OR, add parenthesis
     *
     * @param string $value
     *
     * @return string
     */
    private function convertRequest(string $value): string
    {
        if (empty($value)) {
            return $value;
        }

        $valueWithParenthesis = sprintf('(%s)', $value);
        $uppercaseAnd = str_ireplace(' and ', ' AND ', $valueWithParenthesis);

        return str_ireplace(' or ', ' OR ', $uppercaseAnd);
    }

    /**
     * @param Request $request
     */
    private function fixFilterQueryParameter(Request $request): void
    {
        $filterQuery = $request->getParam('fq');
        if ($filterQuery !== null) {
            $request->removeParam('fq');
            $request->addParam('fq[]', $filterQuery);
        }
    }

    /**
     * @param string $date
     *
     * @return string
     */
    private function toSolrDateFormat(string $date): string
    {
        return (new \DateTime($date))->format('Y-m-d\TH:i:s.v\Z');
    }

    /**
     * @param string $string
     *
     * @return bool
     */
    private function isPatentNumber(string $string): bool
    {
        return (bool) \preg_match('/^(?:(\w{0,2})-?)?((?:\d{2}\/)?[\d,]{5,})(?:-?(\w{0,2}))?$/', $string);
    }

    /**
     * @param string $patentNumber
     *
     * @return string
     */
    private function unifyPatentNumbers(string $patentNumber): string
    {
        $str = \str_replace('/', '', $patentNumber);
        $str = \str_replace(',', '', $str);

        // US7000000 => US-7000000
        if (\preg_match('/[a-z]{2}\d{1,9}/i', $str)) {
            $str = \preg_replace('/[a-z]{2}/i', '$0-', $str, 1);
        }

        // US-7000000B1 => US-7000000-B1
        if (\preg_match('/[a-z]{2}-\d{1,9}[a-z]{1,2}/i', $str)) {
            $str = \preg_replace('/[a-z]{2}-\d{1,9}/i', '$0-', $str, 1);
        }

        return $str;
    }
}
