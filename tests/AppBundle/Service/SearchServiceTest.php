<?php

namespace Tests\AppBundle\Service;

use AppBundle\Exception\Service\UnexpectedRequestValueException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use AppBundle\Service\SearchService;
use Psr\Log\LoggerInterface;
use Solarium\Client;
use Solarium\Core\Client\Response;
use Solarium\QueryType\Select\Result\Document;
use Solarium\QueryType\Select\Result\Result;

class SearchServiceTest extends TestCase
{
    private const FILTER_FROM = '2017-01-01';
    private const FILTER_TO = '2017-02-02';

    /**
     * @var SearchService
     */
    private $searchService;

    /**
     * @var LoggerInterface|MockObject
     */
    private $logger;

    /**
     * @var Result|MockObject
     */
    private $searchResult;

    /**
     * @var Response|MockObject
     */
    private $searchResponse;

    public function setUp()
    {
        parent::setUp();

        $this->logger = $this->createMock(LoggerInterface::class);

        $client = $this->getMockBuilder(Client::class)
            ->setMethods([
                'executeRequest',
                'createResult',
            ])
            ->getMock();

        $this->searchResult = $this->createMock(Result::class);
        $this->searchResponse = $this->createMock(Response::class);

        $client->method('executeRequest')->willReturn($this->searchResponse);
        $client->method('createResult')->willReturn($this->searchResult);

        $this->searchService = new SearchService($this->logger, $client);
    }

    public function testEmptySearch()
    {
        $this->searchResult->expects($this->once())->method('count')->willReturn(0);
        $this->searchResult->expects($this->once())->method('getNumFound')->willReturn(0);
        $this->searchResult->expects($this->once())->method('getData')->willReturn([
            'facet_counts' => [
                'facet_fields' => [
                    'source_label_string' => [],
                    'current_assignee_string' => [],
                ],
            ],
        ]);
        $this->searchResult->expects($this->once())->method('getDocuments')->willReturn([]);

        $this->searchService->search([]);
    }

    public function testSearchResults()
    {
        $this->searchResult->expects($this->once())->method('count')->willReturn(5);
        $this->searchResult->expects($this->once())->method('getNumFound')->willReturn(5);
        $this->searchResult->expects($this->once())->method('getData')->willReturn([
            'facet_counts' => [
                'facet_fields' => [
                    'source_label_string' => [
                        'x', 1,
                        'y', 4,
                        'z', 0,
                    ],
                    'current_assignee_string' => [
                        'x', 1,
                        'y', 4,
                        'z', 0,
                    ],
                ],
            ],
        ]);
        $this->searchResult->expects($this->once())->method('getDocuments')->willReturn([
            new Document([
                'id' => 1,
                'label' => 'label',
                'application_number' => 'application_number',
                'url' => 'url',
                'title' => 'title',
                'pub_type' => 'pub_type',
                'qscore' => 'qscore',
                'vscore' => 'vscore',
                'source_id' => 'source_id',
                'offers' => [
                    [
                        'url' => 'https://google.com/',
                        'trans_type' => 'TYPE_ONLINE_SALE',
                    ]
                ],
                'source_label_string' => 'source_label_string',
                'source_description' => 'source_description',
                'priority_date' => '2017-01-01',
                'current_assignee_string' => 'current_assignee_string',
                'classifications' => 'classifications',
                'verification_status' => 'verification_status',
            ]),
            new Document([
                'id' => 2,
                'label' => 'label',
                'application_number' => 'application_number',
                'url' => 'url',
                'title' => 'title',
                'pub_type' => 'pub_type',
                'qscore' => 'qscore',
                'vscore' => 'vscore',
                'source_id' => 'source_id',
                'offers' => [
                    [
                        'url' => 'https://google.com/',
                        'trans_type' => 'TYPE_LICENSE',
                    ]
                ],
                'source_label_string' => 'source_label_string',
                'source_description' => 'source_description',
                'priority_date' => '2017-01-01',
                'current_assignee_string' => 'current_assignee_string',
                'classifications' => 'classifications',
                'verification_status' => 'verification_status',
            ]),
        ]);

        $this->logger->method('info')->willReturnCallback(function ($message, $context = []) {
            if ($message === 'Sending request') {
                self::assertContains('&q=%2A&', $context['request_uri']);
            }
        });

        $results = $this->searchService->search([]);

        self::assertEquals([
            [
                'name' => 'x',
                'count' => '1',
            ],
            [
                'name' => 'y',
                'count' => '4',
            ],
        ], $results['currentAssignees']);

        self::assertEquals([
            [
                'name' => 'x',
                'count' => '1',
            ],
            [
                'name' => 'y',
                'count' => '4',
            ],
        ], $results['sources']);

        self::assertEquals([
            'countAll' => 5,
            'page' => 1,
            'pages' => 1,
            'onPage' => 5,
        ], $results['paginationAttributes']);

        self::assertEquals([
            [
                'id' => 1,
                'number' => 'label',
                'applicationNumber' => 'application_number',
                'url' => 'url',
                'title' => 'title',
                'type' => 'pub_type',
                'qScore' => 'qscore',
                'vScore' => 'vscore',
                'sourceId' => 'source_id',
                'source' => 'source_label_string',
                'sourceDesc' => 'source_description',
                'priorityDate' => '2017-01-01',
                'currentAssignee' => 'current_assignee_string',
                'classifications' => 'classifications',
                'verificationStatus' => 'verification_status',
                'forLicense' => null,
                'forSale' => [
                    'url' => 'https://google.com/',
                    'name' => 'For sale',
                ],
            ],
            [
                'id' => 2,
                'number' => 'label',
                'applicationNumber' => 'application_number',
                'url' => 'url',
                'title' => 'title',
                'type' => 'pub_type',
                'qScore' => 'qscore',
                'vScore' => 'vscore',
                'sourceId' => 'source_id',
                'source' => 'source_label_string',
                'sourceDesc' => 'source_description',
                'priorityDate' => '2017-01-01',
                'currentAssignee' => 'current_assignee_string',
                'classifications' => 'classifications',
                'verificationStatus' => 'verification_status',
                'forLicense' => [
                    'url' => 'https://google.com/',
                    'name' => 'For license',
                ],
                'forSale' => null,
            ]
        ], $results['patents']);
    }

    public function testQueryBuildSort()
    {
        $this->searchResult->expects($this->exactly(2))->method('count')->willReturn(0);
        $this->searchResult->expects($this->exactly(2))->method('getNumFound')->willReturn(0);
        $this->searchResult->expects($this->exactly(2))->method('getData')->willReturn([
            'facet_counts' => [
                'facet_fields' => [
                    'source_label_string' => [],
                    'current_assignee_string' => [],
                ],
            ],
        ]);
        $this->searchResult->expects($this->exactly(2))->method('getDocuments')->willReturn([]);

        $this->logger->method('info')->willReturnCallback(function ($message, $context = []) {
            if ($message === 'Sending request') {
                self::assertContains('sort', $context['request_uri']);
                if (false !== strpos($context['request_uri'], 'sort=priority_date')) {
                    self::assertContains('sort=priority_date+desc', $context['request_uri']);
                }
                if (false !== strpos($context['request_uri'], 'sort=source_label_string')) {
                    self::assertContains('sort=source_label_string+asc', $context['request_uri']);
                }
            }
        });

        $this->searchService->search([
            'sortingField' => 'priority_date',
        ]);

        $this->searchService->search([
            'sortingField' => 'source_label_string',
        ]);

        try {
            $this->searchService->search([
                'sortingField' => 'bad_sort_field',
            ]);
        } catch (UnexpectedRequestValueException $e) {
            self::assertEquals('Invalid sorting field: bad_sort_field', $e->getMessage());
        }
    }

    public function testQueryBuildDate()
    {
        $this->searchResult->expects($this->exactly(4))->method('count')->willReturn(0);
        $this->searchResult->expects($this->exactly(4))->method('getNumFound')->willReturn(0);
        $this->searchResult->expects($this->exactly(4))->method('getData')->willReturn([
            'facet_counts' => [
                'facet_fields' => [
                    'source_label_string' => [],
                    'current_assignee_string' => [],
                ],
            ],
        ]);
        $this->searchResult->expects($this->exactly(4))->method('getDocuments')->willReturn([]);

        $call = 0;

        $this->logger->method('info')->willReturnCallback(function ($message, $context = []) use (&$call) {
            if ($message === 'Sending request') {
                $call++;
                switch ($call) {
                    case 1:
                        self::assertNotContains('fq', $context['request_uri']);
                        break;
                    case 2:
                        self::assertContains(
                            sprintf('fq%%5B%%5D=priority_date%%3A%%5B%sT00%%3A00%%3A00.000Z+TO+%sT00%%3A00%%3A00.000Z%%5D', self::FILTER_FROM, self::FILTER_TO),
                            $context['request_uri']
                        );
                        break;
                    case 3:
                        self::assertContains(
                            sprintf('fq%%5B%%5D=priority_date%%3A%%5B%%2A+TO+%sT00%%3A00%%3A00.000Z%%5D', self::FILTER_TO),
                            $context['request_uri']
                        );
                        break;
                    case 4:
                        self::assertContains(
                            sprintf('fq%%5B%%5D=priority_date%%3A%%5B%sT00%%3A00%%3A00.000Z+TO+NOW%%5D', self::FILTER_FROM),
                            $context['request_uri']
                        );
                }
            }
        });

        $this->searchService->search([
            'filteringAttributes' => [
                'priorityYears' => [

                ],
            ],
        ]);

        $this->searchService->search([
            'filteringAttributes' => [
                'priorityYears' => [
                    'from' => self::FILTER_FROM,
                    'to' => self::FILTER_TO,
                ],
            ],
        ]);

        $this->searchService->search([
            'filteringAttributes' => [
                'priorityYears' => [
                    'to' => self::FILTER_TO,
                ],
            ],
        ]);

        $this->searchService->search([
            'filteringAttributes' => [
                'priorityYears' => [
                    'from' => self::FILTER_FROM,
                ],
            ],
        ]);
    }

    public function testQueryBuildCurrentAssignees()
    {
        $this->searchResult->expects($this->exactly(3))->method('count')->willReturn(0);
        $this->searchResult->expects($this->exactly(3))->method('getNumFound')->willReturn(0);
        $this->searchResult->expects($this->exactly(3))->method('getData')->willReturn([
            'facet_counts' => [
                'facet_fields' => [
                    'source_label_string' => [],
                    'current_assignee_string' => [],
                ],
            ],
        ]);
        $this->searchResult->expects($this->exactly(3))->method('getDocuments')->willReturn([]);

        $call = 0;

        $this->logger->method('info')->willReturnCallback(function ($message, $context = []) use (&$call) {
            if ($message === 'Sending request') {
                $call++;
                switch ($call) {
                    case 1:
                        self::assertNotContains('fq%5B%5D', $context['request_uri']);
                        break;
                    case 2:
                        self::assertContains('fq%5B%5D=current_assignee%3A%22x%22', $context['request_uri']);
                        break;
                    case 3:
                        self::assertContains('fq%5B%5D=current_assignee%3A%22x%22+OR+current_assignee%3A%22y%22', $context['request_uri']);
                }
            }
        });

        $this->searchService->search([
            'filteringAttributes' => [
                'currentAssignees' => [],
            ],
        ]);

        $this->searchService->search([
            'filteringAttributes' => [
                'currentAssignees' => [
                    'x'
                ],
            ],
        ]);

        $this->searchService->search([
            'filteringAttributes' => [
                'currentAssignees' => [
                    'x', 'y'
                ],
            ],
        ]);
    }

    public function testQueryBuildVerificationStatus()
    {
        $this->searchResult->expects($this->exactly(3))->method('count')->willReturn(0);
        $this->searchResult->expects($this->exactly(3))->method('getNumFound')->willReturn(0);
        $this->searchResult->expects($this->exactly(3))->method('getData')->willReturn([
            'facet_counts' => [
                'facet_fields' => [
                    'source_label_string' => [],
                    'verification_status' => [],
                ],
            ],
        ]);
        $this->searchResult->expects($this->exactly(3))->method('getDocuments')->willReturn([]);

        $methodCallNumber = 0;

        $this->logger->method('info')->willReturnCallback(function ($message, $context = []) use (&$methodCallNumber) {
            if ($message === 'Sending request') {
                $methodCallNumber++;
                switch ($methodCallNumber) {
                    case 1:
                        self::assertNotContains('fq%5B%5D', $context['request_uri']);
                        break;
                    case 2:
                        self::assertContains('fq%5B%5D=verification_status%3A%22IPWE_VERIFIED%22', $context['request_uri']);
                        break;
                    case 3:
                        self::assertContains('fq%5B%5D=verification_status%3A%22IPWE_VERIFIED%22+OR+verification_status%3A%22OWNER_VERIFIED%22', $context['request_uri']);
                }
            }
        });

        $this->searchService->search([
            'filteringAttributes' => [
                'verificationStatus' => [],
            ],
        ]);

        $this->searchService->search([
            'filteringAttributes' => [
                'verificationStatus' => [
                    'IPWE_VERIFIED'
                ],
            ],
        ]);

        $this->searchService->search([
            'filteringAttributes' => [
                'verificationStatus' => [
                    'IPWE_VERIFIED', 'OWNER_VERIFIED'
                ],
            ],
        ]);
    }

    public function testQueryBuildSources()
    {
        $this->searchResult->expects($this->exactly(3))->method('count')->willReturn(0);
        $this->searchResult->expects($this->exactly(3))->method('getNumFound')->willReturn(0);
        $this->searchResult->expects($this->exactly(3))->method('getData')->willReturn([
            'facet_counts' => [
                'facet_fields' => [
                    'source_label_string' => [],
                    'current_assignee_string' => [],
                ],
            ],
        ]);
        $this->searchResult->expects($this->exactly(3))->method('getDocuments')->willReturn([]);

        $call = 0;

        $this->logger->method('info')->willReturnCallback(function ($message, $context = []) use (&$call) {
            if ($message === 'Sending request') {
                $call++;
                switch ($call) {
                    case 1:
                        self::assertNotContains('fq%5B%5D', $context['request_uri']);
                        break;
                    case 2:
                        self::assertContains('fq%5B%5D=source_label_string%3A%22x%22', $context['request_uri']);
                        break;
                    case 3:
                        self::assertContains('fq%5B%5D=source_label_string%3A%22x%22+OR+source_label_string%3A%22y%22', $context['request_uri']);
                }
            }
        });

        $this->searchService->search([
            'filteringAttributes' => [
                'documentSource' => [],
            ],
        ]);

        $this->searchService->search([
            'filteringAttributes' => [
                'documentSource' => [
                    'x'
                ],
            ],
        ]);

        $this->searchService->search([
            'filteringAttributes' => [
                'documentSource' => [
                    'x', 'y'
                ],
            ],
        ]);
    }

    public function testQueryBuildType()
    {
        $this->searchResult->expects($this->exactly(5))->method('count')->willReturn(0);
        $this->searchResult->expects($this->exactly(5))->method('getNumFound')->willReturn(0);
        $this->searchResult->expects($this->exactly(5))->method('getData')->willReturn([
            'facet_counts' => [
                'facet_fields' => [
                    'source_label_string' => [],
                    'current_assignee_string' => [],
                ],
            ],
        ]);
        $this->searchResult->expects($this->exactly(5))->method('getDocuments')->willReturn([]);

        $call = 0;

        $this->logger->method('info')->willReturnCallback(function ($message, $context = []) use (&$call) {
            if ($message === 'Sending request') {
                $call++;
                switch ($call) {
                    case 1:
                        self::assertNotContains('fq%5B%5D', $context['request_uri']);
                        break;
                    case 2:
                        self::assertNotContains('fq%5B%5D', $context['request_uri']);
                        break;
                    case 3:
                        self::assertNotContains('fq%5B%5D', $context['request_uri']);
                        break;
                    case 4:
                        self::assertContains(
                            'fq%5B%5D=publication_type%3A2+OR+publication_type%3A1',
                            $context['request_uri']
                        );
                        break;
                    case 5:
                        self::assertContains(
                            'fq%5B%5D=publication_type%3A0',
                            $context['request_uri']
                        );
                }
            }
        });

        $this->searchService->search([
            'isAdvancedSearchEnabled' => false,
            'advancedSearchAttributes' => [
                'searchNPLs' => true,
                'searchApplications' => false,
                'searchGrants' => true,
            ],
        ]);

        $this->searchService->search([
            'isAdvancedSearchEnabled' => true,
            'advancedSearchAttributes' => [
                'searchNPLs' => true,
                'searchApplications' => true,
                'searchGrants' => true,
            ],
        ]);

        $this->searchService->search([
            'isAdvancedSearchEnabled' => true,
            'advancedSearchAttributes' => [
                'searchNPLs' => false,
                'searchApplications' => false,
                'searchGrants' => false,
            ],
        ]);

        $this->searchService->search([
            'isAdvancedSearchEnabled' => true,
            'advancedSearchAttributes' => [
                'searchNPLs' => true,
                'searchApplications' => false,
                'searchGrants' => true,
            ],
        ]);

        $this->searchService->search([
            'isAdvancedSearchEnabled' => true,
            'advancedSearchAttributes' => [
                'searchNPLs' => false,
                'searchApplications' => true,
                'searchGrants' => false,
            ],
        ]);
    }

    public function testQueryBuildGeneralSearch()
    {
        $this->searchResult->expects($this->once())->method('count')->willReturn(0);
        $this->searchResult->expects($this->once())->method('getNumFound')->willReturn(0);
        $this->searchResult->expects($this->once())->method('getData')->willReturn([
            'facet_counts' => [
                'facet_fields' => [
                    'source_label_string' => [],
                    'current_assignee_string' => [],
                ],
            ],
        ]);
        $this->searchResult->expects($this->once())->method('getDocuments')->willReturn([]);

        $this->logger->method('info')->willReturnCallback(function ($message, $context = []) {
            if ($message === 'Sending request') {
                self::assertContains('qf=label+application_number+authors%5E10+title%5E10+current_assignee%5E10+original_assignee%5E10+paragraph_text+abstract%5E5+claims_text%5E2', $context['request_uri']);
                self::assertContains('q=string', $context['request_uri']);
            }
        });

        $this->searchService->search([
            'generalSearchQuery' => 'string',
        ]);
    }

    public function testQueryBuildPatentNumberSearch()
    {
        $this->searchResult->expects($this->once())->method('count')->willReturn(0);
        $this->searchResult->expects($this->once())->method('getNumFound')->willReturn(0);
        $this->searchResult->expects($this->once())->method('getData')->willReturn([
            'facet_counts' => [
                'facet_fields' => [
                    'source_label_string' => [],
                    'current_assignee_string' => [],
                ],
            ],
        ]);
        $this->searchResult->expects($this->once())->method('getDocuments')->willReturn([]);

        $this->logger->method('info')->willReturnCallback(function ($message, $context = []) {
            if ($message === 'Sending request') {
                self::assertContains('qf=label%5E10+application_number%5E10+authors%5E10+title%5E10+current_assignee%5E10+original_assignee%5E10+paragraph_text+abstract%5E5+claims_text%5E2', $context['request_uri']);
                self::assertContains('q=123456789', $context['request_uri']);
            }
        });

        $this->searchService->search([
            'generalSearchQuery' => '123456789',
        ]);
    }

    public function testQueryBuildAdvancedSearch()
    {
        $this->searchResult->expects($this->once())->method('count')->willReturn(0);
        $this->searchResult->expects($this->once())->method('getNumFound')->willReturn(0);
        $this->searchResult->expects($this->once())->method('getData')->willReturn([
            'facet_counts' => [
                'facet_fields' => [
                    'source_label_string' => [],
                    'current_assignee_string' => [],
                ],
            ],
        ]);
        $this->searchResult->expects($this->once())->method('getDocuments')->willReturn([]);

        $this->logger->method('info')->willReturnCallback(function ($message, $context = []) {
            if ($message === 'Sending request') {
                self::assertContains('q=title%3A%28title%29+AND+current_assignee%3A%28assignee%29', $context['request_uri']);
            }
        });

        $this->searchService->search([
            'isAdvancedSearchEnabled' => true,
            'advancedSearchAttributes' => [
                'title' => 'title',
                'currentAssignee' => 'assignee',
                'inventor' => '',
            ],
        ]);
    }

    /**
     * @throws \ReflectionException
     */
    public function testUnifyPatentNumbers()
    {
        $reflection = new \ReflectionClass($this->searchService);
        $reflectionMethodUnifyPatentNumbers = $reflection->getMethod('unifyPatentNumbers');
        $reflectionMethodUnifyPatentNumbers->setAccessible(true);

        $unifiedPatentNumbers = $reflectionMethodUnifyPatentNumbers->invokeArgs($this->searchService, ['US7000000B1']);
        self::assertEquals('US-7000000-B1', $unifiedPatentNumbers);
    }
}
