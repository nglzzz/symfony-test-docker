<?php

namespace Tests\AppBundle\Service;

use AppBundle\Exception\Service\UnexpectedServiceResponseExceptions;
use AppBundle\Service\VerificationsService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response as ResponseHttp;

class VerificationsServiceTest  extends TestCase
{
    private const SERVICES_BASE_URL = 'http://test.com:80/v1';
    private const PATENT_NUMBER = 'BY-123456789-E1';
    private const TEST_JSON = '{"foo": "bar"}';


    /**
     * @var VerificationsService
     */
    private $verificationsService;

    /**
     * @var LoggerInterface|MockObject
     */
    private $logger;

    /**
     * @throws \ReflectionException
     */
    public function setUp()
    {
        parent::setUp();

        $this->logger = $this->createMock(LoggerInterface::class);

        $guzzleMockHandler = new MockHandler([
            new Response(ResponseHttp::HTTP_OK,
                [],
                self::TEST_JSON
            ),
            new Response(ResponseHttp::HTTP_NO_CONTENT,
                [],
                self::TEST_JSON
            ),
        ]);

        $guzzleHandler = HandlerStack::create($guzzleMockHandler);
        $guzzleClient = new Client(['handler' => $guzzleHandler]);

        /** @var VerificationsService $verificationsService */
        $verificationsService = new VerificationsService($this->logger, self::SERVICES_BASE_URL);

        $reflection = new \ReflectionClass(VerificationsService::class);
        $reflectionPropertyGuzzleClient = $reflection->getProperty('guzzleClient');
        $reflectionPropertyGuzzleClient->setAccessible(true);
        $reflectionPropertyGuzzleClient->setValue($verificationsService, $guzzleClient);

        $this->verificationsService = $verificationsService;
    }

    public function testRequestVerifications()
    {
        $response = $this->verificationsService->requestVerifications(self::PATENT_NUMBER);

        self::assertArrayHasKey('foo', $response);

        try {
            $this->verificationsService->requestVerifications(self::PATENT_NUMBER);
        } catch (UnexpectedServiceResponseExceptions $e) {
            self::assertEquals('Verification service returned unexpected data', $e->getMessage());
        }
    }
}
