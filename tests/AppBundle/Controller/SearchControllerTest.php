<?php

namespace Tests\AppBundle\Controller;

use RomaricDrigon\MetaYaml\Loader\YamlLoader;
use RomaricDrigon\MetaYaml\MetaYaml;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SearchControllerTest extends WebTestCase
{
    public function testSearchEndpoint()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/search?query=%7B%22isAdvancedSearchEnabled%22%3Afalse%2C%22generalSearchQuery%22%3A%22pizza%22%2C%22advancedSearchAttributes%22%3A%7B%22searchNPLs%22%3Atrue%2C%22searchApplications%22%3Atrue%2C%22searchGrants%22%3Atrue%2C%22inventor%22%3A%22%22%2C%22title%22%3A%22%22%2C%22abstract%22%3A%22%22%2C%22specification%22%3A%22%22%2C%22claims%22%3A%22%22%2C%22currentAssignee%22%3A%22%22%7D%2C%22sorting%22%3Anull%2C%22filtering%22%3A%7B%22currentAssignees%22%3A%5B%5D%2C%22priorityYears%22%3A%7B%22from%22%3Anull%2C%22to%22%3Anull%7D%2C%22documentSource%22%3A%5B%5D%7D%2C%22paginationAttributes%22%3A%7B%22perPage%22%3A20%2C%22page%22%3A1%7D%7D'
        );

        $response = $client->getResponse();
        $content = $response->getContent();

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $responseData = json_decode($content, true);

        self::assertEquals(JSON_ERROR_NONE, json_last_error());

        $loader = new YamlLoader();

        $schemaValidator = new MetaYaml($loader->loadFromFile('app/Resources/schema/response.yaml'));

        self::assertTrue($schemaValidator->validate($responseData));
    }

    public function testSearchBadSortField()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/search?query=%7B%22sortingField%22%3A%22bad_sort_field%22%7D'
        );

        self::assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    public function testSearchEndpointBadRequest()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/search?query=%7B%22isAdvancedSearchEnabled%22%3Afalse%2C%22generalSearchQuery%22%3A%22pizza%22%2C%22advancedSearchAttributes%22%3A%7B%22searchNPLs%22%3Atrue%2C%22searchApplications%22%3Atrue%2C%22searchGrants%22%3Atrue%2C%22inventor%22%3A%22%22%2C%22title%22%3A%22%22%2C%22abstract%22%3A%22%22%2C%22specification%22%3A%22%22%2C%22claims%22%3A%22%22%2C%22currentAssignee%22%3A%22%22%7D%2C%22sorting%22%3Anull%2C%22filtering%22%3A%7B%22classifications%22%3A%5B%5D%2C%22priorityYears%22%3A%7B%22from%22%3Anull%2C%22to%22%3Anull%7D%2C%22documentSource%22%3A%5B%5D%7D%2C%22paginationAttributes%22%3A%7B%22perPage%22%3A20%2C%22page%22%3A1%7D'
        );

        self::assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    public function testVerificationsEndpoint()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/verifications?patentNumber=BY-1231234234-E'
        );

        $response = $client->getResponse();
        $content = $response->getContent();

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());

        json_decode($content, true);

        self::assertEquals(JSON_ERROR_NONE, json_last_error());
    }

    public function testVerificationsEndpointForNoNumber()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/verifications'
        );

        $response = $client->getResponse();
        $content = $response->getContent();

        self::assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        $responseData = json_decode($content, true);

        self::assertEquals(JSON_ERROR_NONE, json_last_error());

        self::assertEquals('Bad request', $responseData['message'] ?? '');
    }

    public function testVerificationsEndpointForEmptyNumber()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/verifications?patentNumber='
        );

        $response = $client->getResponse();
        $content = $response->getContent();

        self::assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());

        $responseData = json_decode($content, true);

        self::assertEquals(JSON_ERROR_NONE, json_last_error());

        self::assertEquals('Internal server error: Unexpected service response', $responseData['message'] ?? '');
    }
}
