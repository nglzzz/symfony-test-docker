<?php

namespace Tests\AppBundle\Utils;

use AppBundle\Utils\SolrQueryExpression;
use AppBundle\Utils\SolrQueryTerm;
use PHPUnit\Framework\TestCase;
use Solarium\Client;

class SolrQueryTermTest extends TestCase
{
    private const FIELD_NAME = 'field';
    private const FIELD_VALUE = 'value';
    private const NON_DEFAULT_BOOST = SolrQueryTerm::DEFAULT_BOOST + 1;

    public function testQueryGeneration()
    {
        $client = new Client();

        $query = $client->createSelect();

        $expression = SolrQueryExpression::createQueryExpression($query);

        $queryString = $expression
            ->addTerm(self::FIELD_NAME, self::FIELD_VALUE)
            ->getAsQuery();

        self::assertEquals(sprintf('%s:%s', self::FIELD_NAME, self::FIELD_VALUE), $queryString);
    }

    public function testQueryGenerationNonDefaultBoost()
    {
        $client = new Client();

        $query = $client->createSelect();

        $expression = SolrQueryExpression::createQueryExpression($query);

        $queryString = $expression
            ->addTerm(self::FIELD_NAME, self::FIELD_VALUE, self::NON_DEFAULT_BOOST)
            ->getAsQuery();

        self::assertEquals(sprintf('%s:%s^%d', self::FIELD_NAME, self::FIELD_VALUE, self::NON_DEFAULT_BOOST), $queryString);
    }
}
