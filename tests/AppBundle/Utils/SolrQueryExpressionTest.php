<?php

namespace Tests\AppBundle\Utils;

use AppBundle\Exception\SolrQueryBuilderException;
use function GuzzleHttp\Psr7\parse_query;
use PHPUnit\Framework\TestCase;
use AppBundle\Utils\SolrQueryExpression;
use Solarium\Client;

class SolrQueryExpressionTest extends TestCase
{
    private const FIELD_NAME = 'field';
    private const FIELD_VALUE = 'value';

    public function testCreateSelectQueryExpression()
    {
        $client = new Client();

        $query = $client->createSelect();

        $expression = SolrQueryExpression::createQueryExpression($query);

        self::assertEquals('*:*', $expression->getAsQuery());
    }

    public function testCreateFilterQueryExpression()
    {
        $client = new Client();

        $query = $client->createSelect();

        $expression = SolrQueryExpression::createFilterExpression($query->createFilterQuery());

        self::assertEquals('*:*', $expression->getAsQuery());
    }

    public function testAndExpression()
    {
        $client = new Client();
        $query = $client->createSelect();

        $expression = SolrQueryExpression::createQueryExpression($query);

        $queryString = $expression
            ->addExpression(SolrQueryExpression::AND)
                ->addTerm(self::FIELD_NAME, self::FIELD_VALUE)
                ->addTerm(self::FIELD_NAME, self::FIELD_VALUE)
            ->endExpression()
            ->getAsQuery();

        self::assertEquals(
            sprintf('%s:%s AND %s:%s', self::FIELD_NAME, self::FIELD_VALUE, self::FIELD_NAME, self::FIELD_VALUE),
            $queryString
        );
    }

    public function testOrExpression()
    {
        $client = new Client();
        $query = $client->createSelect();

        $expression = SolrQueryExpression::createQueryExpression($query);

        $queryString = $expression
            ->addExpression(SolrQueryExpression::OR)
                ->addTerm(self::FIELD_NAME, self::FIELD_VALUE)
                ->addTerm(self::FIELD_NAME, self::FIELD_VALUE)
            ->endExpression()
            ->getAsQuery();

        self::assertEquals(
            sprintf('%s:%s OR %s:%s', self::FIELD_NAME, self::FIELD_VALUE, self::FIELD_NAME, self::FIELD_VALUE),
            $queryString
        );
    }

    public function testSubExpressions()
    {
        $client = new Client();
        $query = $client->createSelect();

        $expression = SolrQueryExpression::createQueryExpression($query);

        $queryString = $expression
            ->addExpression(SolrQueryExpression::OR)
                ->addTerm(self::FIELD_NAME, self::FIELD_VALUE)
                ->addTerm(self::FIELD_NAME, self::FIELD_VALUE)
            ->endExpression()
            ->addExpression(SolrQueryExpression::AND)
                ->addTerm(self::FIELD_NAME, self::FIELD_VALUE)
                ->addTerm(self::FIELD_NAME, self::FIELD_VALUE)
            ->endExpression()
            ->getAsQuery();

        $term = sprintf('%s:%s', self::FIELD_NAME, self::FIELD_VALUE);

        self::assertEquals(
            sprintf('(%s OR %s) AND (%s AND %s)', $term, $term, $term, $term),
            $queryString
        );
    }

    public function testCharEscape()
    {
        $client = new Client();
        $query = $client->createSelect();

        $expression = SolrQueryExpression::createQueryExpression($query);

        $expression
            ->addTerm(self::FIELD_NAME, '"&*')
            ->buildQuery();

        $request = $client->createRequest($query);

        $parsedQuery = parse_url($request->getUri(), PHP_URL_QUERY);

        self::assertContains(sprintf('q=%s%%3A%%22%%26%%2A', self::FIELD_NAME), $parsedQuery);

        $parsed = parse_query($parsedQuery);

        self::assertEquals(
            sprintf('%s:"&*', self::FIELD_NAME),
            $parsed['q']
        );
    }

    public function testEmptyExpression()
    {
        $this->expectException(SolrQueryBuilderException::class);

        $client = new Client();
        $query = $client->createSelect();

        self::assertTrue(SolrQueryExpression::createQueryExpression($query)->isEmpty());

        SolrQueryExpression::createQueryExpression($query)
            ->addExpression(SolrQueryExpression::AND)
            ->endExpression()
            ->addTerm(self::FIELD_NAME, self::FIELD_VALUE)
            ->getAsQuery();
    }
}
