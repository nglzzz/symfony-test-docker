<?php

namespace Tests\AppBundle\Utils;

use AppBundle\Exception\SolrQueryBuilderException;
use AppBundle\Utils\SolrQueryExpression;
use PHPUnit\Framework\TestCase;
use Solarium\Client;

class SolrQueryNumberTest extends TestCase
{
    private const FIELD_NAME = 'field';
    private const FIELD_VALUE = 1;
    private const INVALID_FIELD_VALUE = 'string';

    public function testQueryGeneration()
    {
        $client = new Client();

        $query = $client->createSelect();

        $expression = SolrQueryExpression::createQueryExpression($query);

        $queryString = $expression
            ->addNumber(self::FIELD_NAME, self::FIELD_VALUE)
            ->getAsQuery();

        self::assertEquals(sprintf('%s:%s', self::FIELD_NAME, self::FIELD_VALUE), $queryString);
    }

    public function testInvalidValue()
    {
        $this->expectException(\TypeError::class);

        $client = new Client();

        $query = $client->createSelect();

        $expression = SolrQueryExpression::createQueryExpression($query);

        $expression
            ->addNumber(self::FIELD_NAME, self::INVALID_FIELD_VALUE)
            ->getAsQuery();
    }
}
