<?php

namespace Tests\AppBundle\Utils;

use AppBundle\Utils\SolrQueryExpression;
use PHPUnit\Framework\TestCase;
use Solarium\Client;

class SolrQueryDateRangeTest extends TestCase
{
    private const DATE_FIELD_NAME = 'date';
    private const DATE_FIELD_FROM = '2017-10-10';
    private const DATE_FIELD_TO = '2017-12-12';

    public function testQueryGeneration()
    {
        $client = new Client();

        $query = $client->createSelect();

        $expression = SolrQueryExpression::createQueryExpression($query);

        $queryString = $expression
            ->addDateRange(self::DATE_FIELD_NAME, self::DATE_FIELD_FROM, self::DATE_FIELD_TO)
            ->getAsQuery();

        self::assertEquals(sprintf('%s:[%s TO %s]', self::DATE_FIELD_NAME, self::DATE_FIELD_FROM, self::DATE_FIELD_TO), $queryString);
    }
}
