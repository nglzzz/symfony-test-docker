<?php

$finder = PhpCsFixer\Finder::create()->in('src');

return PhpCsFixer\Config::create()
    ->setRules([
        '@Symfony' => true,
        '@PSR1' => true,
        '@PSR2' => true,
    ])
    ->setFinder($finder);